package auth

import helpers.TestRuntimeContext
import org.specs2.specification.Scope
import models.user.User
import models.auth._
import play.api.test.PlaySpecification
import scala.concurrent.duration._
import scala.concurrent.Await

/**
 * Test for AuthServiceModule
 */
object AuthServiceModuleTest extends PlaySpecification{

  trait TestContext extends Scope {
    val context = new TestRuntimeContext
    val testUserObj = User("dan","dan@queuenetwork.com","bogus",NormalUser,"Firstname","Lastname", displayName = "fakeusername")
    val testUser = Await.result(context.userService.create(testUserObj),30 seconds)

    val newOrgAdminUser = User("newuser","test@foo.com","bogus",OrganizationAdmin,"Another","User", displayName = "fakeusername")
    val newNormalUser = User("foo","foo@foo.com","bogus",NormalUser,"Another","User", displayName = "fakeusername")
    val normalAuth = Some(context.authObjects.AuthToken(1,"foo",NormalUser))
    val orgAdminAuth = Some(context.authObjects.AuthToken(2,"orgAdmin",OrganizationAdmin))
  }

  "AuthServiceModule" should {
    "allow you to generate a token to reset a password" in new TestContext {
      val token = context.authService.generateResetToken(testUser.id)
      val claims = context.tokenService.decode(token)

      claims.isSuccess must beTrue
      claims.get.getSubject must be equalTo testUser.id.toString
      // reset should be the getTime() of the date the token was generated
      claims.get.getCustomClaim("reset").toString.length must be equalTo 13
    }
    "allow a user to reset their password" in new TestContext {
      val NEW_PASSWORD = "new password"
      // basic sanity check to make sure the base user's encrypted password matches the graph object on
      context.authService.checkPassword(testUserObj.password,testUser.obj.password) must beTrue
      // generate a reset token
      val token = context.authService.generateResetToken(testUser.id)
      // set a new password using the token
      val newUser = Await.result(context.authService.resetPassword(token,NEW_PASSWORD),30 seconds)
      // confirm that the new password matches the encrypted graph user's
      context.authService.checkPassword(NEW_PASSWORD,newUser.obj.password) must beTrue
    }
    "allow a user to change their password" in new TestContext {
      val NEW_PASSWORD = "new password"
      // set a new password using the token
      val newUser =  Await.result(context.authService.changePassword(testUser.id,NEW_PASSWORD),30 seconds)
      // get the new password from the graph user
      context.authService.checkPassword(NEW_PASSWORD,newUser.obj.password) must beTrue
    }

    "allow Access levels to be compared" in {
      AnonymousAccess < NormalUser &&
      NormalUser < Administrator &&
      Administrator < OrganizationAdmin &&
      OrganizationAdmin < PlatformAdmin &&
      PlatformAdmin < GodMode must beTrue
    }
    "allow you to authenticate by username" in new TestContext {
      val auth = Await.result(context.authService.authenticate("dan","bogus"),30 seconds)
      auth.username must be equalTo "dan"
    }
    "allow you to authenticate by email" in new TestContext {
      val auth = Await.result(context.authService.authenticate("dan@queuenetwork.com","bogus"),30 seconds)
      auth.username must be equalTo "dan"
    }
    "throw an Exception on invalid credentials" in new TestContext {
      Await.result(context.authService.authenticate("dan", "incorrect"), 30 seconds) must throwA[AuthenticationException]
      Await.result(context.authService.authenticate("incorrect", "bogus"), 30 seconds) must throwA[AuthenticationException]
    }
    "allow a user to register another user" in new TestContext {
      val user = Await.result(context.authService.signup(orgAdminAuth,newOrgAdminUser),30 seconds)
      user.obj.username must be equalTo newOrgAdminUser.username
    }
    "deny a user from creating a user with greater access" in new TestContext {
      Await.result(context.authService.signup(normalAuth,newOrgAdminUser),30 seconds) must throwA[AuthorizationException]
    }
    "allow an anonymous user to register" in new TestContext {
      val user = Await.result(context.authService.signup(None,newNormalUser),30 seconds)
      user.obj.username must be equalTo newNormalUser.username
    }
    "deny an anonymous user from setting greater access" in new TestContext {
      Await.result(context.authService.signup(None,newOrgAdminUser),30 seconds) must throwA[AuthorizationException]
    }
    "quick signup with just an email address" in new TestContext {
      var email = "fake@email.com"
      var token = Await.result(context.authService.register(None,email),30 seconds)
      var decoded = context.tokenService.decode(token,validate = true)
      decoded must beSuccessfulTry
      decoded.map{token =>
        token.getSubject must be equalTo email
      }
    }
    "complete quick signup with token" in new TestContext {
      todo
    }
    "decode a token" in new TestContext {
      val subject = "subject"
      val valid = context.tokenService.encode(subject,Map(),100)
      val invalid = context.tokenService.encode(subject,Map(),-100)

      val validResult = Await.result(context.authService.validate(valid), 30 seconds)
      val invalidResult = Await.result(context.authService.validate(invalid), 30 seconds) must throwA[ExpiredTokenException]

      validResult.getSubject must be equalTo "subject"

    }
  }
}
