package auth

import helpers.{TestConstantsModuleImpl, TestRuntimeContext}
import models.auth.{InvalidJWTIssuerException, TestTokenHelperModuleImpl, TokenModuleImpl, NormalUser}
import models.ProductionConstantsModuleImpl
import play.api.test.PlaySpecification
import org.specs2.specification.Scope

/**
 * Test classes to models.Token
 */
object TokenModuleSpec extends PlaySpecification{
  trait TestContext extends Scope {
    val context = new TestRuntimeContext
  }
  val subject = "test"
  val claims = Map("foo"->"bar")

  "TokenModule" should {
    "allow you to encode/decode a token" in new TestContext{
      //val token = tokenService.encode(subject,claims)
      val (token, refreshToken) = context.tokenService.newTokenWithRefresh(subject,claims)

      val decoded = context.tokenService.decode(token).get
      decoded.getSubject must be equalTo "test"
      decoded.getCustomClaim("foo").toString must be equalTo "bar"
    }

    "fail when a token is expired" in new TestContext {
      val token = context.tokenService.encode(subject,claims,-100000L) // negative expiration time
      val decoded = context.tokenService.decode(token)

      decoded.isFailure must beTrue
      decoded.failed.get.getMessage must be equalTo "token has expired"

    }
    "fails when an invalid token is passed in" in new TestContext {
      val decoded = context.tokenService.decode("BAD FORMAT")

      decoded.isFailure must beTrue
      decoded.failed.get.getMessage must be equalTo "Invalid serialized plain/JWS/JWE object: Missing part delimiters"
    }
    "allow you to refresh an expired token" in new TestContext {
      val (token,refreshToken) = context.tokenService.newTokenWithRefresh(subject,claims,-100000L) // negative expiration time

      val newToken = context.tokenService.refresh(token,refreshToken)
      newToken.isSuccess must beTrue

      val newTokenDecoded = context.tokenService.decode(newToken.get)
      newTokenDecoded.isSuccess must beTrue
      newTokenDecoded.get.getCustomClaim("foo") must be equalTo "bar"
    }
    "allow you to retrieve a user from a token" in new TestContext {
      val customClaims = Map("username" -> "foo","access"->NormalUser.toString)
      val (token, refreshToken) = context.tokenService.newTokenWithRefresh("100", customClaims)

      val authToken = context.authService.getUserByToken(token).get

      authToken.id.asInstanceOf[Long].toString must be equalTo "100"
      authToken.access must be equalTo NormalUser
    }

    "reject tokens without the proper jwtIssuer " in {
      val prod = new Object with TokenModuleImpl with TestTokenHelperModuleImpl with ProductionConstantsModuleImpl
      val test = new Object with TokenModuleImpl with TestTokenHelperModuleImpl with TestConstantsModuleImpl

      val prodToken = prod.tokenService.encode(subject,claims) // create a token using production "jwtIssuer"
      val testToken = test.tokenService.encode(subject,claims) // create a token using test "jwtIssuer"

      val result1 = prod.tokenService.decode(testToken)
      val result2 = test.tokenService.decode(prodToken)
      val result3 = prod.tokenService.decode(prodToken)
      val result4 = test.tokenService.decode(testToken)

      result1.isFailure must beTrue
      result1.failed.get match {
        case e : InvalidJWTIssuerException => success
        case _ => failure("not the expected exception")
      }

      result2.isFailure must beTrue
      result2.failed.get match {
        case e : InvalidJWTIssuerException => success
        case _ => failure("not the expected exception")
      }

      result3.isSuccess must beTrue
      result4.isSuccess must beTrue

    }
  }

}
