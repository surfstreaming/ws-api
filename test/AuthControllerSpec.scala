import com.tinkerpop.blueprints.GraphFactory
import com.tinkerpop.gremlin.scala.ScalaGraph
import helpers.TestRuntimeContext
import org.specs2.mutable._

import play.api.GlobalSettings
import play.api.test._
import play.api.test.Helpers._
import play.api.libs.json.Json
import play.api.libs.iteratee.Enumerator
import org.specs2.specification.Scope
import models.RuntimeContext


/**
 * Specs2 tests
 */
class AuthControllerSpec extends Specification {

  val test = FakeApplication(withGlobal = Some(new GlobalSettings() {
    val foo = "bar"

    def onStart(app: App) {
      println("Hello world!")
    }
  }))

  trait TestContext extends Scope

  "Foo" should {
    "stop making noise" in {
      1 must be equalTo 1
    }
  }
}
/*
    "AuthController" should {

      "login should return JSON"  in new WithApplication(test)   {
        val json = Json.obj(
          "username" -> "someuser",
          "password" -> "password"
        )
        val request = FakeRequest("POST","/").withJsonBody(json).withHeaders((CONTENT_TYPE, "application/json"))
        val requestBody = Enumerator(Json.stringify(json).getBytes) andThen Enumerator.eof
        val result = requestBody |>>> controllers.AuthController.login(request)

        status(result) must equalTo(OK)
        contentType(result) must beSome("application/json")
        charset(result) must beSome("utf-8")
      }

      "login should be able to handle WithCors" in new WithApplication(test)  {
        val json = Json.obj(
          "username" -> "someuser",
          "password" -> "password"
        )
        //val request = FakeRequest("POST","/").withJsonBody(json)
        val request = FakeRequest("POST","/").withJsonBody(json).withHeaders((CONTENT_TYPE, "application/json"))
        val requestBody = Enumerator(Json.stringify(json).getBytes) andThen Enumerator.eof
        val result = requestBody |>>> controllers.AuthController.login(request)
        //val result : Future[SimpleResult] = controllers.AuthController.testfoo(request).run

        contentAsString(result) must contain("\"status\":\"ok\"")
      }
      "login should validate JSON" in new WithApplication(test)  {
        val json = Json.obj(
          "nouser" -> "someuser",
          "password" -> "password"
        )
        //val request = FakeRequest("POST","/auth/login").withBody(Json.stringify(json)).withHeaders((CONTENT_TYPE, "application/json"))
        //val result = controllers.AuthController.login(request).run
        val request = FakeRequest("POST","/").withJsonBody(json).withHeaders((CONTENT_TYPE, "application/json"))
        val requestBody = Enumerator(Json.stringify(json).getBytes) andThen Enumerator.eof
        val result = requestBody |>>> controllers.AuthController.login(request)

        contentAsString(result) must contain("\"status\":\"error\"")
        contentAsString(result) must contain("\"obj.username\":[\"error.path.missing\"]")
      }

      "login should give an error for a bad password" in new WithApplication(test)  {
        val json = Json.obj(
          "username" -> "dan",
          "password" -> "wrong"
        )
        val request = FakeRequest("POST","/").withJsonBody(json).withHeaders((CONTENT_TYPE, "application/json"))
        val requestBody = Enumerator(Json.stringify(json).getBytes) andThen Enumerator.eof
        val result = requestBody |>>> controllers.AuthController.login(request)

        contentAsString(result) must contain("\"status\":\"error\"")
        contentAsString(result) must contain("\"message\":\"incorrect username or password\"")
      }

      "successful login should return a token" in new WithApplication(test){
        val json = Json.obj(
          "username" -> "dan",
          "password" -> "password"
        )
        val request = FakeRequest("POST","/").withJsonBody(json).withHeaders((CONTENT_TYPE, "application/json"))
        val requestBody = Enumerator(Json.stringify(json).getBytes) andThen Enumerator.eof
        val result = requestBody |>>> controllers.AuthController.login(request)

        contentAsString(result) must contain("\"status\":\"ok\"")
        contentAsString(result) must contain("token")
      }

    }
*/
