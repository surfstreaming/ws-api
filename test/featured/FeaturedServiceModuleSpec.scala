package featured

import play.api.test.PlaySpecification
import helpers.TestRuntimeContext
import org.specs2.specification.Scope

/**
 * Test for FeaturedServiceModule
 */
object FeaturedServiceModuleSpec extends PlaySpecification {

  trait TestContext extends Scope {
    val context = new TestRuntimeContext("fixtures/data.json")
    //val anonUser = Await.result(context.graph.get[User](4 ),5 seconds) // id 4 == user: anonymous
  }

  "fixtures test" should {
    "be able to extract FeaturedContent queue" in new TestContext {
/*
      val result = anonUser.vertex.out("created_queue")
        .has("queueName","Featured Content").as("x")
        .out("next_queue_item").as("item")
        .propertyMap().as("itemProps")
        .back("item")
        .loop("x",
          {y =>  y.getLoops < 10},
          emit => true
        )
        .out("queue_item_content")
        .as("content")
        .propertyMap()
        .as("contentProps")
        .select("item","itemProps","content","contentProps")
        .map { row =>
          val item = row.getColumn("item") match {
            case v : Vertex => v
            case _ => throw new Exception("item was expected to be a Vertex, but was not" )
          }
          // TODO this unconversion is unsafe, but should be OK as long as the return value of propertyMap() remains the same
          val itemProps = row.getColumn("itemProps").asInstanceOf[Map[String,Any]]
          val content = row.getColumn("content") match {
            case v : Vertex => v
            case _ => throw new Exception("item was expected to be a Vertex, but was "  )
          }
          // TODO this unconversion is unsafe, but should be OK as long as the return value of propertyMap() remains the same
          val contentProps = row.getColumn("contentProps").asInstanceOf[Map[String,Any]]

          (item,itemProps,content,contentProps)
        }
        .toList()


      //result.map(_.getId.toString).toScalaList() must be equalTo List("1")
      //result.map(_.toString) must be equalTo List("foo")
      result.size must be equalTo 3
      //result must be equalTo "wrong"
*/
    }
  }
}
