package queues

import com.surf.graph.{ObjectNotFoundException, GraphObjectUniqueConstraintException}
import helpers.TestRuntimeContext
import models.ObjectNotFoundApiException
import models.auth.NormalUser
import models.queues.{UserFavoriteQueueEdge, Queue}
import org.specs2.specification.Scope
import play.api.test.PlaySpecification

import scala.concurrent.Await
import scala.concurrent.duration._

/**
 * Specifications for queue favorites
 */
object FavoritesSpec extends PlaySpecification {
  trait TestContext extends Scope {
    val context = new TestRuntimeContext("fixtures/data.json")

    val testQueue = Queue(code = "foocode", name = "My First Queue", description = None, access = "private")
    val friendQueue = Queue(code = "friendsQueue", name = "Queue for Friends", description = None, access = "friends")
    val publicQueue = Queue(code = "publicQueue", name = "Public Queue", description = None, access = "public")
    val dupeQueue = Queue(code = "foocode", name = "My Duplicate Queue", description = None, access = "private")



    val userJoe = context.ids.get("user-joe").get
    val queueTestPrivate = context.ids.get("queue-test-private").get
    val queueTestPublic = context.ids.get("queue-test-public").get
  }


  "Queue Favorites" should {
    "allow a user to add a favorite queue" in new TestContext {
      val result = Await.result(context.queueService.edgeCreate(userJoe,userJoe,UserFavoriteQueueEdge(),queueTestPrivate),30 seconds)

      result.v1.id must be equalTo userJoe
      result.v2.id must be equalTo queueTestPrivate
      result.edge.label must be equalTo "favorite_queue"
    }
    "allow a user to retrieve a favorite segment" in new TestContext {
      todo
      /*
      val result = Await.result(context.queueService.edgeGet[UserFavoriteQueueEdge](Some(userJoe),userJoe,"user-joe|favorite_queue|queue-test-public"),30 seconds)


      result.v1.id must be equalTo userJoe
      result.v2.id must be equalTo queueTestPublic
      result.edge.label must be equalTo "favorite_queue"
        */
    }
    "should throw an error if adding favorite but already exists" in new TestContext {
      //val request = UserFavoriteQueueEdgeRequest("queue-test-public")
      val edge =
      Await.result(context.queueService.edgeCreate(userJoe,userJoe,UserFavoriteQueueEdge(),queueTestPublic),30 seconds) must throwA[GraphObjectUniqueConstraintException]
    }

    "allow a user to delete a favorite queue" in new TestContext {
      todo
      /*
      val result = Await.result(context.queueService.edgeGet[UserFavoriteQueueEdge](Some(userJoe),userJoe,"user-joe|favorite_queue|queue-test-public"),30 seconds)
      val deleted = Await.result(context.queueService.edgeDelete[UserFavoriteQueueEdge](userJoe,userJoe,result.edge.id),30 seconds)

      val last = Await.result(context.queueService.edgeGet[UserFavoriteQueueEdge](Some(userJoe),userJoe,"user-joe|favorite_queue|queue-test-public"),30 seconds) must throwA[ObjectNotFoundException]
      */
    }

    "allow a user to retrieve their favorite queues" in new TestContext {
      val result = Await.result(context.queueService.edgeList[UserFavoriteQueueEdge](Some(userJoe),userJoe),30 seconds)

      result.map(_.id) must containAllOf(Seq(queueTestPublic))
    }
    "allow favorite to be retrieved as a component of a queue" in new TestContext {
      todo
    }
    "get number of favorites for a queue" in new TestContext {
      val queue = Await.result(context.queueService.get(Some(userJoe),userJoe,queueTestPublic),30 seconds)
      val result = Await.result(context.queueService.favoriteCount(queue),30 seconds)

      result must be equalTo 3L
    }
    "do not fail if there are no likes/favorites" in new TestContext {
      val queue = Await.result(context.queueService.get(Some(userJoe),userJoe,queueTestPrivate),30 seconds)
      val favorites = Await.result(context.queueService.favoriteCount(queue),30 seconds)
      val likes = Await.result(context.queueService.likeCount(queue),30 seconds)

      favorites must be equalTo 0L
      likes must be equalTo 0L
    }
  }
}