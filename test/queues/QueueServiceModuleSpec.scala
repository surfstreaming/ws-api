package queues

import com.surf.graph.ObjectNotFoundException
import play.api.test.PlaySpecification
import helpers.TestRuntimeContext
import org.specs2.specification.Scope
import scala.concurrent.Await
import scala.concurrent.duration._
import models.auth.{AuthorizationException, NormalUser, GodMode}
import models.queues.{QueueItem, Queue}

/**
 * Test Queue modules
 */
object QueueServiceModuleSpec extends PlaySpecification {
  trait TestContext extends Scope {
    val context = new TestRuntimeContext("fixtures/data.json")

    val testQueue = Queue(code = "foocode",name = "My First Queue",description = None, access = "private")
    val friendQueue = Queue(code = "friendsQueue",name = "Queue for Friends",description = None, access = "friends")
    val publicQueue = Queue(code = "publicQueue",name = "Public Queue",description = None, access = "public")
    val dupeQueue = Queue(code = "foocode",name =  "My Duplicate Queue",description = None, access = "private")

    val userAdmin = context.ids.get("user-admin").get
    val userJoe = context.ids.get("user-joe").get
    val userHank = context.ids.get("user-hank").get
    val movieHobbit = context.ids.get("movie-hobbit").get
    val movieXmen = context.ids.get("movie-xmen").get

    val queueTestPrivate = context.ids.get("queue-test-private").get
    val queueTestPublic = context.ids.get("queue-test-public").get
    val queueTestFriend = context.ids.get("queue-test-friend").get

  }

  "QueueServiceModule" should {
    "allow you to retrieve all queues for a user" in new TestContext {
      val result = Await.result(context.queueService.getAllQueues(userAdmin),30 seconds)
      result.size must be equalTo 1
    }
    "retrieve queues with token" in new TestContext {
      val result = Await.result(context.queueService.getQueues(Some(userAdmin),userAdmin),30 seconds)
      result.size must be equalTo 1
    }
    "allow you to create a queue" in new TestContext {
      val queue = Await.result(context.queueService.create(userJoe,userJoe,testQueue),30 seconds)
      queue.obj.code must be equalTo "foocode"
      val roundTrip = Await.result(context.graph.get[Queue](queue.id), 30 seconds)
      roundTrip.obj.code must be equalTo queue.obj.code
    }
    "owner can fetch their queue" in new TestContext {
      val queue = Await.result(context.queueService.get(Some(userJoe),userJoe,queueTestPrivate),30 seconds)
      queue.obj.code must be equalTo "queue_test_private"
    }
    "throw error when when creating queue that isn't yours" in new TestContext {
      Await.result(context.queueService.get(Some(userJoe),5,queueTestPrivate),30 seconds) must throwA[ObjectNotFoundException]
    }
    "allow you to add items to an empty queue" in new TestContext {
      val result = Await.result(context.queueService.itemCreate(userJoe,userJoe,queueTestPublic,QueueItem(Some("sample notes")),movieXmen),30 seconds)
      result.obj.notes must be equalTo Some("sample notes")
      val items = Await.result(context.queueService.itemList(Some(userJoe),userJoe,queueTestPublic),30 seconds)
      items.map(_.v2.id) must be equalTo Seq(movieXmen)
    }
    "retrieve friend queues" in new TestContext {
      val result = Await.result(context.queueService.getFriendQueues(userJoe,userHank,includePublic = false), 30 seconds)
      result.map(_.id) must be equalTo Seq(queueTestFriend)
    }
  }
}
