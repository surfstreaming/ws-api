package tmdb

import com.surf.core.objects.content.movies.Movie
import play.api.test.PlaySpecification
import helpers.TestRuntimeContext
import org.specs2.specification.Scope
import scala.concurrent.{Future, Await}
import scala.concurrent.duration._

/**
 * Test for TmdbServiceModule
 */
object TmdbServiceModuleTest extends PlaySpecification {

  trait TestContext extends Scope {
    val context = new TestRuntimeContext("fixtures/data.json")
    val movieXmen = context.ids.get("movie-xmen").get
  }

  "TmdbServiceModule" should {
    "be able to retrieve movies from graph" in new TestContext {
      val movie = Await.result(context.graph.get[Movie](movieXmen),5 seconds)
      movie.obj.title must be equalTo "X-Men: Days of Future Past"
    }
  }
}
