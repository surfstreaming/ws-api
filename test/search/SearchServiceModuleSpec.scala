package search

import play.api.test.PlaySpecification
import helpers.TestRuntimeContext
import org.specs2.specification.Scope
import models.search._
import scala.concurrent.duration._
import scala.concurrent.Await


object SearchServiceModuleSpec extends PlaySpecification {

  trait TestContext extends Scope {
    val context = new TestRuntimeContext("fixtures/data.json")

    val movieXmen = context.ids.get("movie-xmen").get
    val movieFrozen = context.ids.get("movie-frozen").get
    val movieHobbit = context.ids.get("movie-hobbit").get

    val userJoe = context.ids.get("user-joe").get
    val userSuzy = context.ids.get("user-suzy").get
    val userAnonymous = context.ids.get("user-anonymous").get
    val userAdmin = context.ids.get("user-admin").get
  }


  "SearchService" should {
    /*
    "combine two sets of results together" in new TestContext {
      val result = context.searchService.combineResults(seq1,seq2)
      result must be equalTo Seq(result1,result2,result3)
    }
    */
    "allow you to filter by type Movies" in new TestContext {
      import context.searchObjects._
      val filter1 = SearchFilterInput("NoOp")
      val filters = Seq(filter1)

      val result = Await.result(context.searchService.search(filters,Set(SearchableMovie)),30 seconds)
      val onlyMovies = result.foldLeft(true){ (acc, cur) =>
          if(!acc) acc // if already false, continue to return false
          else cur.objType == "Movie" // otherwise check to make sure it's a movie
      }

      onlyMovies must beTrue
      result.size must be equalTo 3
      result.map(_.id) must containAllOf(Seq(movieXmen,movieFrozen,movieHobbit))
    }

    "allow you to filter by type Users" in new TestContext {
      import context.searchObjects._
      val filter1 = SearchFilterInput("NoOp")
      val filters = Seq(filter1)

      val result = Await.result(context.searchService.search(filters,Set(SearchableUser)),30 seconds)
      val onlyUsers = result.foldLeft(true){ (acc, cur) =>
        if(!acc) acc // if already false, continue to return false
        else cur.objType == "User" // otherwise check to make sure it's a User
      }
      onlyUsers must beTrue
      result.map(_.id) must containAllOf(Seq(userAdmin,userAnonymous,userJoe,userSuzy))
    }

    "allow you to filter by type multiple types" in new TestContext {
      import context.searchObjects._
      val filter1 = SearchFilterInput("NoOp")
      val filters = Seq(filter1)

      val result = Await.result(context.searchService.search(filters,Set(SearchableUser,SearchableMovie)),30 seconds)
      val hasUser = result.foldLeft(true){ (acc, cur) =>
        if(acc) acc // if already true, continue to return true
        else cur.objType == "User" // otherwise check to make sure it's a movie
      }
      val hasMovie = result.foldLeft(true){ (acc, cur) =>
        if(acc) acc // if already true, continue to return true
        else cur.objType == "Movie" // otherwise check to make sure it's a movie
      }
      hasUser must beTrue
      hasMovie must beTrue
    }

    "allow you to filter by name" in new TestContext {
      import context.searchObjects._
      val filter1 = SearchFilterInput("name", value = Some("anon"))
      val filters = Seq(filter1)

      val result = Await.result(context.searchService.search(filters,Set(SearchableUser)),30 seconds)

      result.size must be equalTo 1
      //result.map(_.id) must be equalTo Seq("user-anonymous")
    }
  }
}
