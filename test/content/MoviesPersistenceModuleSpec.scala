package content

import com.surf.core.objects.content.ContentEdgeObjects.GenreEdge
import com.surf.core.objects.content.movies.Genre
import play.api.test.PlaySpecification
import helpers.TestRuntimeContext
import scala.concurrent.Await
import scala.concurrent.duration._
import org.specs2.specification.Scope

/**
 * Test Movies
 */
object MoviesPersistenceModuleSpec extends PlaySpecification {
  trait TestContext extends Scope {
    val context = new TestRuntimeContext("fixtures/data.json")
    val movieXmen = context.ids.get("movie-xmen").get
  }

  "MoviePersistenceModule" should {
    "be able to retrieve movies" in new TestContext {
      val result = Await.result(context.moviesService.get(None,movieXmen,Seq()),30 seconds)
      result.id must be equalTo movieXmen
    }
    "retrieve genres from an existing GraphVertex" in new TestContext {
      val result = Await.result(context.moviesService.get(None,movieXmen,Seq()),30 seconds)
      val genres = Await.result(context.moviesService.getComponent[GenreEdge,Genre](None,result),30 seconds)
      genres.map(_.obj.name) must containTheSameElementsAs( Seq("action","adventure") )
    }
  }

}
