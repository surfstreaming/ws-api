package content

import com.surf.core.objects.content.ContentEdgeObjects.FeaturedContentEdge
import com.surf.graph.{GraphObjectUniqueConstraintException, EdgeHelper}
import helpers.TestRuntimeContext
import org.specs2.specification.Scope
import play.api.test.PlaySpecification

import scala.concurrent.Await
import scala.concurrent.duration._

/**
 * Test Movies Json
 */
object FeaturedSpec extends PlaySpecification {

  trait TestContext extends Scope {
    val context = new TestRuntimeContext("fixtures/data.json")
    val userAdmin = context.ids.get("user-admin").get
    val authToken = context.ids.get("user-joe").get
    val adminToken = userAdmin
    val movieHobbit = context.ids.get("movie-hobbit").get
    val movieXmen = context.ids.get("movie-xmen").get

  }

  "ContentServiceModule" should {
    "let you add featured content" in new TestContext {

      val result = Await.result(context.contentService.addFeatured(userAdmin,movieHobbit),30 seconds)

      result.edge.label must be equalTo implicitly[EdgeHelper[FeaturedContentEdge]].label
      result.v1.objType must be equalTo "Movie"
      result.v2.objType must be equalTo "User"
    }
    "list featured content" in new TestContext {
      val result = Await.result(context.contentService.listFeatured(Some(authToken)),30 seconds)
      result.map(_.vertex.id)  must containAllOf(Seq(movieXmen))
    }
    "throw error if edge exists" in new TestContext {
      val result = Await.result(context.contentService.addFeatured(userAdmin,movieXmen),30 seconds) must throwA[GraphObjectUniqueConstraintException]
    }
  }
}