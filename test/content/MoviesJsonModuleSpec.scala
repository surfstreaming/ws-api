package content

import play.api.test.PlaySpecification
import helpers.TestRuntimeContext
import scala.concurrent.Await
import scala.concurrent.duration._
import org.specs2.specification.Scope
import play.api.libs.json.{JsValue, Json}

/**
 * Test Movies Json
 */
object MoviesJsonModuleSpec extends PlaySpecification {
  trait TestContext extends Scope {
    val context = new TestRuntimeContext("fixtures/data.json")

    val userAdmin = context.ids.get("user-admin").get
    val authToken = context.ids.get("user-joe").get
    val adminToken = userAdmin
    val movieHobbit = context.ids.get("movie-hobbit").get
    val movieXmen = context.ids.get("movie-xmen").get
    val keywordMutant = context.ids.get("keyword-mutant").get
    val genreAdventure = context.ids.get("genre-adventure").get
    val genreAction = context.ids.get("genre-action").get
    val queueTestPrivate = context.ids.get("queue-test-private").get
  }

  "MoviesJsonModule" should {
    "be able to retrieve movies without components" in new TestContext {
      val result = Await.result(context.moviesJson.get(None,movieXmen,Seq()),30 seconds)
      (result \ "id").as[Long] must be equalTo movieXmen
    }
    "be able to retrieve movies with components" in new TestContext {
      val result = Await.result(context.moviesJson.get(None,movieXmen,Seq("genres","keywords")),30 seconds)
      val genres = (result \ "components" \ "genres").as[Seq[JsValue]].map(json => (json \ "id").as[Long])
      val keywords = (result \ "components" \ "keywords").as[Seq[JsValue]].map(json => (json \ "id").as[Long])

      genres must containTheSameElementsAs( Seq(genreAdventure,genreAction))
      keywords must containTheSameElementsAs( Seq(keywordMutant))
    }

    "retrieve queues that a movie is in" in new TestContext {
      val xmen = Await.result(context.moviesService.get(None,movieXmen,Seq()),30 seconds)
      val result = Await.result(context.moviesService.getQueuesCreatedByUser(Some(authToken),xmen),30 seconds)

      //result.map(_.near.id) must containTheSameElementsAs( Seq("queue-test-private","queue-test-friend"))
      result.map(_.v2.id) must containTheSameElementsAs( Seq(queueTestPrivate))
    }

  }

}
