package helpers

import com.thinkaurelius.titan.core.TitanGraph
import scala.collection.JavaConverters._

object TestIds {

  def process(graph : TitanGraph) : Map[String,Long] = {

    graph.query().has("_test",true).vertices().iterator().asScala.map { v =>
      val prop = v.getProperty[String]("_testid")
      val id = v.getId.asInstanceOf[Long]
      (prop,id)
    }.toMap
  }
}
