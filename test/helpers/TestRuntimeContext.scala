package helpers

import com.surf.graph.titan.TitanInMemoryGraphModule
import com.tinkerpop.blueprints.Vertex
import models.auth.TestTokenHelperModuleImpl
import models.{ConstantsModule, AbstractRuntimeContext}
import play.api.Logger
import java.io.FileNotFoundException
import schema.Schema

class TestRuntimeContext(fixtures : String = null) extends AbstractRuntimeContext
  with TestTokenHelperModuleImpl
  with TestMailServiceModuleImpl
  with TestConstantsModuleImpl
  with TitanInMemoryGraphModule
{
  // I am not sure it is best to have this single threaded, but the TinkerPop in memory graph can throw ConcurrentModificationExceptions if accessed concurrently
  //override val graphQueryExecutionContext = scala.concurrent.ExecutionContext.fromExecutor(Executors.newSingleThreadExecutor())
  //override val standardExecutionContext = scala.concurrent.ExecutionContext.Implicits.global

  Schema.load(titanGraph)
  val mgmt = titanGraph.getManagementSystem

  val test = mgmt.makePropertyKey("_test").dataType(classOf[String]).make()
  mgmt.buildIndex("_testIndex",classOf[Vertex]).addKey(test).buildCompositeIndex()
  mgmt.commit()
  if(fixtures != null) {
    Logger.debug("Using test figures from " + fixtures)
    val is = ClassLoader.getSystemClassLoader.getResourceAsStream(fixtures)
    if(is == null) throw new FileNotFoundException("cannot load resource "+ fixtures)
    graph.loadJson(is)
  }

  val ids = TestIds.process(titanGraph)

  implicit def writesVertexId = play.api.libs.json.Writes.LongWrites
  implicit def writesEdgeId = play.api.libs.json.Writes.StringWrites
  implicit def readsVertexId = play.api.libs.json.Reads.LongReads
  implicit def readsEdgeId = play.api.libs.json.Reads.StringReads

}


trait TestConstantsModuleImpl extends ConstantsModule {
  val constants = TestConstants

  object TestConstants extends Constants {
    val adminInterfaceURL = "http://testAdminURL.com/"
    val searchServiceURL = "http://localhost:9001"
    val contentServiceURL = "http://localhost:9002"
    val webUI = "http://localhost:5000"
    val tmdbAPIKey = "93581830f1a1c1d3a861b853bf9f51cc"
    val jwtIssuer = "http://surfbeta.com"
    val mailgunURL = "x"
    val mailgunKey = "x"
    val mailBcc = None
  }
}
