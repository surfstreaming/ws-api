package helpers

import models.mail.MailServiceModule
import play.api.Logger

/**
 * Test implementation of MailServiceModule
 */
trait TestMailServiceModuleImpl extends MailServiceModule {

  val mailService = new TestMaiServiceImpl

  class TestMaiServiceImpl extends MailService {
    def send(to : String, from : String, subject : String, html : String, bcc : Option[String]) : String = {
      Logger.error(s"not actually sending an email to $to because this is only a test. output would have been:\n$html")

      "Ok"
    }
  }
}
