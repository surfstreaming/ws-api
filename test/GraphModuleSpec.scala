import com.surf.graph._
import com.tinkerpop.blueprints.Vertex
import com.tinkerpop.gremlin.scala.GremlinScalaPipeline
import helpers.TestRuntimeContext
import models.user.User
import org.specs2.specification.Scope
import play.api.Logger
import play.api.test.PlaySpecification
import scala.concurrent.Await
import scala.util.Success

import play.api.libs.json._
import play.api.libs.functional.syntax._

import scala.concurrent.duration._


object GraphModuleSpec  extends PlaySpecification {
  case class TestObject(str1 : String, str2 : String, int1 : Int)
  object TestObject {
    implicit val reads : Reads[TestObject] = (
      (JsPath \ 'name).read[String] and
        (JsPath \ 'age).read[String] and
        (JsPath \ 'lovesChocolate).read[Int]
      )(TestObject.apply _)

    implicit val writes : Writes[TestObject] = (
      (JsPath \ 'name).write[String] and
        (JsPath \ 'age).write[String] and
        (JsPath \ 'lovesChocolate).write[Int]
      )(unlift(TestObject.unapply))
  }

  trait TestContext extends Scope {
    val context = new TestRuntimeContext

    implicit object TestObjectHelper extends DefaultVertexHelper[TestObject] {
      val objectType = "Test"
      val uniqueFields = Seq()

      override def validate(u: TestObject, pipe : GremlinScalaPipeline[Vertex,Vertex]) = {
        Success(u)
      }

      def toObject(props : Map[String,Any]) : TestObject = {
        TestObject(
          str1 = props.get("str1").get.asInstanceOf[String],
          str2 = props.get("str2").get.asInstanceOf[String],
          int1 = props.get("int1").get.asInstanceOf[Int]
        )
      }
      def toMap(obj : TestObject) : Map[String,Any] = {
        Map(
          "str1" -> obj.str1,
          "str2" -> obj.str2,
          "int1" -> obj.int1
        )
      }

    }
  }

  trait SampleDataContext extends Scope {
    val context = new TestRuntimeContext("fixtures/sampleGraph.json")
  }
  trait TestFixturesContext extends Scope {
    val context = new TestRuntimeContext("fixtures/data.json")
  }

  "fixtures test" should {
    "be able to query the data graph" in new TestFixturesContext {
      Await.result(context.graph.getByKey[User]("User:username","surf"), 30 seconds).obj.username must be equalTo "surf"
    }

    "test vertex pair" in new TestFixturesContext {
      val query = new GremlinScalaPipeline[Vertex,Vertex]
        .has("id","user-joe")
      /*
      val result = Await.result(
          context.graph.querySegments[User,CreatedQueueEdge,Queue](query,Direction.OUT)
        , 30 seconds)

      result.seq.head.v1.obj.username must be equalTo "joe"
      */
    }

  }

  "GraphModule" should {

    "allow you to create objects" in new TestContext {
      val obj = TestObject("one","two",4)
      val v = Await.result(context.graph.create(obj),30 seconds)

      v.objType must be equalTo "Test"
      v.obj.str1 must be equalTo "one"
      v.obj.int1 must be equalTo 4
    }
    "allow you to retrieve objects" in new TestContext {
      val obj = TestObject("one","two",4)
      val v = Await.result(context.graph.create(obj),30 seconds)
      val obj2 = Await.result(context.graph.get[TestObject](v.id),30 seconds)

      obj2.obj.str1 must be equalTo "one"
    }
    "allow you to retrieve an object by vertex id" in new TestContext {
      val obj = TestObject("one","two",4)
      val v = Await.result(context.graph.create(obj),30 seconds)
      val v2 = Await.result(context.graph.get[SimpleVertex](v.id),30 seconds)

      v2.obj.props.get("str1").get.asInstanceOf[String] must be equalTo "one"
    }
    "allow you to update properties" in new TestContext {
      val obj = TestObject("one","two",4)
      val v = Await.result(context.graph.create(obj),30 seconds)

      val v2 = Await.result(context.graph.updateProperty(v,"str1","NEWVALUE"),30 seconds)
      //v2.obj.str1 must be equalTo "NEWVALUE"
      // TODO test this
    }
    "allow you to convert Object to JSON" in {
      val obj = TestObject("one","two",4)

      Json.obj("data" -> obj)
      success
    }
  }
}
