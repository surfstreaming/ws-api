package users

import play.api.Logger
import play.api.test.PlaySpecification
import helpers.TestRuntimeContext
import org.specs2.specification.Scope
import scala.concurrent.Await
import scala.concurrent.duration._


object UserServiceModuleSpec extends PlaySpecification {

  trait TestContext extends Scope {
    val context = new TestRuntimeContext("fixtures/data.json")

    val userJoe = context.ids.get("user-joe").get
    val userSuzy = context.ids.get("user-suzy").get
  }

  "UserServiceModule" should {

    "allow you to create a friendship" in new TestContext {
      val startFriends = Await.result(context.userService.getFriends(Some(userJoe),userJoe),30 seconds)
      val result = Await.result(context.userService.addFriend(userJoe,userJoe,userSuzy),30 seconds)
      Await.result(context.userService.confirmFriend(userJoe,result.edge.id),30 seconds)
      val relationship = Await.result(context.userService.getRelationship(Some(userJoe),userSuzy),30 seconds)

      relationship.status must be equalTo "Friends"
    }
    "allow you to see the relationship between two users" in new TestContext {
      val currentUser = Await.result(context.userService.getRelationship(Some(userJoe),userJoe),30 seconds)
      currentUser.status must be equalTo "Current User"
      val anon = Await.result(context.userService.getRelationship(None,userSuzy),30 seconds)
      anon.status must be equalTo "Not Authorized"
      val start = Await.result(context.userService.getRelationship(Some(userJoe),userSuzy),30 seconds)
      start.status must be equalTo "No Relationship"
      val result = Await.result(context.userService.addFriend(userJoe,userJoe,userSuzy),30 seconds)
      val pendingA = Await.result(context.userService.getRelationship(Some(userJoe),userSuzy),30 seconds)
      pendingA.status must be equalTo "Pending Request"
      val pendingB = Await.result(context.userService.getRelationship(Some(userSuzy),userJoe),30 seconds)
      pendingB.status must be equalTo "Awaiting Confirmation"
      Await.result(context.userService.confirmFriend(userJoe,result.edge.id),30 seconds)
      val confirmedA = Await.result(context.userService.getRelationship(Some(userJoe),userSuzy),30 seconds)
      confirmedA.status must be equalTo "Friends"
      val confirmedB = Await.result(context.userService.getRelationship(Some(userSuzy),userJoe),30 seconds)
      confirmedA.status must be equalTo "Friends"

    }
  }
}
