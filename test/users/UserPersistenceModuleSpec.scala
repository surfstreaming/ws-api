package users

import com.surf.graph.GraphObjectUniqueConstraintException
import play.api.Logger
import play.api.test.PlaySpecification
import helpers.TestRuntimeContext
import org.specs2.specification.Scope
import scala.concurrent.Await
import scala.concurrent.duration._
import models.user.User
import models.auth.NormalUser
import scala.util.Try


object UserPersistenceModuleSpec extends PlaySpecification {

  trait TestContext extends Scope {
    val context = new TestRuntimeContext("fixtures/data.json")
    val userAdmin = context.ids.get("user-admin").get
  }

  val testUser1 = User("fakeusername","fakeemail@fake.com","fakepassword",NormalUser,"Firstname","Lastname", displayName = "fakeusername")
  val testUser1DupeUsername = User("fakeusername","differentfakeemail@fake.com","fakepassword",NormalUser,"Firstname","Lastname", displayName = "fakeusername")
  val testUser2 = User("anotherusername","anotheremail@fake.com","anotherpassword",NormalUser,"Firstname","Lastname", displayName = "fakeusername")
  val testUser2DupeEmail= User("differentanotherusername","anotheremail@fake.com","anotherpassword",NormalUser,"Firstname","Lastname", displayName = "fakeusername")

  "UserPersistenceModule" should {
    "allow you to create a user" in new TestContext {
      val v = Try(Await.result(context.userPersistence.create(testUser1),30 seconds))
      v.isSuccess must beTrue
      v.get.obj.username must be equalTo "fakeusername"
    }

    "allow you to fetch a user" in new TestContext {
      val x = Await.result(context.userPersistence.getUser(userAdmin),30 seconds)
      x.obj.username must be equalTo "surf"
    }
    "enforce unique usernames and email" in new TestContext {

      val email1 = Try(Await.result(context.userPersistence.create(testUser2),30 seconds))
      val email2 = Try(Await.result(context.userPersistence.create(testUser2DupeEmail),30 seconds))

      val username1 = Try(Await.result(context.userPersistence.create(testUser1),30 seconds))
      val username2 = Try(Await.result(context.userPersistence.create(testUser1DupeUsername),30 seconds))

      username1.isSuccess must beTrue
      username2.isFailure must beTrue
      username2.failed.get must beAnInstanceOf[GraphObjectUniqueConstraintException]

      email1.isSuccess must beTrue
      email2.isFailure must beTrue
      email2.failed.get must beAnInstanceOf[GraphObjectUniqueConstraintException]

    }

    "create a default queue upon user creation" in new TestContext {
      val user = Await.result(context.userPersistence.create(testUser1),30 seconds)
      val queues = Await.result(context.queueService.getPrivateQueues(user.id),30 seconds)

      queues.size must be equalTo 1
      queues.head.obj.name must be equalTo "My Queue"
      queues.head.obj.access must be equalTo "private"
    }
  }
}
