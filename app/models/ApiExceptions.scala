package models

import play.api.libs.json.{JsString, Json, Writes, JsValue}

sealed trait ApiExceptions

abstract class ApiException(message : String) extends Exception(message) {
  val error : String
  val code : Int
  val details : JsValue
}
object ApiException {
  implicit def writes : Writes[ApiException] = new Writes[ApiException]{
    def writes(value: ApiException): JsValue = Json.obj(
      "error" -> value.error,
      "message" -> value.getMessage,
      "code" -> value.code,
      "details" -> value.details
    )
  }
}




case class JsonParseException(message : String,details : JsValue) extends ApiException(message) {
  val error = "JsonParseException"
  val code = 400
}
case class ObjectNotFoundApiException(message : String) extends ApiException(message) {
  val code = 404
  val error = "ObjectNotFoundException"
  val details = JsString(message)
}
case class UnexpectedResultsException(message : String) extends ApiException(message) {
  val code = 406
  val error = "UnexpectedResultsException"
  val details = JsString(message)
}
case class UnauthorizedException(message : String) extends ApiException(message) {
  val code = 401
  val error = "UnauthorizedException"
  val details = JsString(message)
}

case class InvalidStateException(message : String) extends ApiException(message) {
  val code = 400
  val error = "InvalidStateException"
  val details = JsString(message)
}
case class ObjectExistsApiException(message : String) extends ApiException(message) {
  val code = 400
  val error = "ObjectExistsApiException"
  val details = JsString(message)
}