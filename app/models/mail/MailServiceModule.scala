package models.mail

import play.api.Logger
import play.api.libs.ws.{WSAuthScheme, WS}
import com.ning.http.client.Realm.AuthScheme
import play.api.libs.concurrent.Execution.Implicits._
import play.api.libs.json.Json
import play.api.Play.current
import models.{DevelopmentConstantsModuleImpl, ConstantsModule}

/**
 * Module to manage sending email
 */
trait MailServiceModule {

  val mailService : MailService
  trait MailService {
    def send(to : String, from : String, subject : String, html : String, bcc : Option[String]) : String
  }
}

trait MailgunMailServiceModuleImpl extends MailServiceModule {
  this : ConstantsModule =>
  val mailService = new MailgunMailServiceImpl

  class MailgunMailServiceImpl extends MailService {
    def send(to: String, from : String, subject : String, html : String, bcc : Option[String]) = {
      val bccSeq = bcc.map(Seq(_)).getOrElse(Seq())

      val fromDomain = constants.mailgunURL
      val apiKey = constants.mailgunKey
      val data = Map(
        "from" -> Seq(from),
        "to" -> Seq(to),
        "bcc" -> bccSeq,
        "subject" -> Seq(subject),
        "text" -> Seq(html),
        "html" -> Seq(html)
      )

      WS.url(s"https://api.mailgun.net/v2/$fromDomain/messages")
        .withAuth("api",apiKey,WSAuthScheme.BASIC)
        .post(data)
        .map { response =>
          Logger.warn(response.body.toString)
        }

      "Ok"
    }
  }
}


trait DevMailgunMailServiceModuleImpl extends MailgunMailServiceModuleImpl {
  this : DevelopmentConstantsModuleImpl =>
  override val mailService = new DevMailgunMailServiceImpl

  class DevMailgunMailServiceImpl extends MailgunMailServiceImpl {
    override def send(to: String, from : String, subject : String, html : String, bcc : Option[String]) = {
      Logger.info(s"Sending email originally intended for $to to ${constants.emailOverride}\nSubject: $subject\nEmail body:\n$html")
      super.send(constants.emailOverride,from,subject,html,bcc)
    }
  }
}
