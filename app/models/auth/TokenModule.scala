package models.auth

import scala.util.{Success, Try}
import com.nimbusds.jwt.{SignedJWT, JWTClaimsSet, ReadOnlyJWTClaimsSet}
import java.util.Date
import com.nimbusds.jose.{JWSAlgorithm, JWSHeader}
import com.nimbusds.jose.crypto.{RSASSAVerifier, RSASSASigner}
import play.api.Logger
import collection.JavaConversions._
import models.ConstantsModule

trait TokenModule {
  val tokenService : TokenService

  trait TokenService {
    def newTokenWithRefresh(subject : String, customClaims : Map[String,AnyRef], expirationMinutes : Long = 72000L ) : (String, String)
    def encode(subject : String, customClaims : Map[String,AnyRef], expirationMinutes : Long = 72000L ) : String
    def decode(token : String, validate : Boolean = true) : Try[ReadOnlyJWTClaimsSet]
    def process(jwt : ReadOnlyJWTClaimsSet) : Try[(String,Map[String,AnyRef])]
    def extract(token : String) : Try[(String,Map[String,AnyRef])]
    def refresh(originalToken : String, refreshToken : String) : Try[String]
  }
}
trait TokenModuleImpl extends TokenModule {
  this: TokenHelperModule with ConstantsModule =>
  val tokenService = new TokenModuleImpl

  class TokenModuleImpl extends TokenService {
    lazy val privateKey = tokenHelper.makePrivateKey("key.der")
    lazy val publicKey = tokenHelper.makePublicKey("public.der")
    lazy val jwtIssuer = constants.jwtIssuer

    def newTokenWithRefresh(subject : String, customClaims : Map[String,AnyRef], expirationMinutes : Long = 72000L ) : (String, String) = {
      val claims = createClaims(subject,customClaims,expirationMinutes)
      val signedJWT = sign(claims)
      val token = serialize(signedJWT)

      val refresh = encode(subject,Map("refresh"->"true","originalJwtID"->claims.getJWTID),Long.MaxValue)
      (token,refresh)
    }
    def createClaims(subject : String, customClaims : Map[String,AnyRef], expirationMinutes : Long = 72000L ) : JWTClaimsSet = {
      val now = new Date()

      // claim set
      val time = new Date().getTime
      val claimsSet = new JWTClaimsSet()
      claimsSet.setJWTID(time.toString)
      claimsSet.setSubject(subject)
      claimsSet.setIssueTime(now)
      claimsSet.setExpirationTime(new Date(time+1000*60*expirationMinutes))
      claimsSet.setIssuer(jwtIssuer)

      // custom claims
      claimsSet.setCustomClaims(customClaims)
      claimsSet
    }
    def sign(claims : JWTClaimsSet) : SignedJWT = {
      val header = new JWSHeader(JWSAlgorithm.RS256)
      val signedJWT = new SignedJWT(header, claims)
      val signer = new RSASSASigner(privateKey)

      signedJWT.sign(signer)
      signedJWT
    }
    def serialize(signedJWT : SignedJWT) : String = {
      val serializedJWT = signedJWT.serialize()
      SignedJWT.parse(serializedJWT).getParsedString
    }
    def encode(subject : String, customClaims : Map[String,AnyRef], expirationMinutes : Long = 72000L ) : String = {
      val claims = createClaims(subject,customClaims,expirationMinutes)
      val signedJWT = sign(claims)
      serialize(signedJWT)
    }
    def decode(token : String, validate : Boolean = true ) : Try[ReadOnlyJWTClaimsSet] = {
      Try {
        val jwt = SignedJWT.parse(token)
        // Make sure the token is valid using our public key
        val verifier = new RSASSAVerifier(publicKey)

        // throw exception if the token is NOT verified
        if(!jwt.verify(verifier))
          throw new InvalidTokenException("unable to validate token")

        val claims = jwt.getJWTClaimsSet

        // check to make sure the token has not expired
        if(validate && claims.getExpirationTime != null && claims.getExpirationTime.before(new Date())) {
          Logger.debug("token is expired: " +claims.getExpirationTime.toString)
          throw new ExpiredTokenException
        }
        if(validate && !jwtIssuer.equals(claims.getIssuer)) {
          throw new InvalidJWTIssuerException(s"expected issuer to be $jwtIssuer but it was ${claims.getIssuer}")
        }
        claims
      }
    }
    def process(claims : ReadOnlyJWTClaimsSet) : Try[(String,Map[String,AnyRef])] = {
      val customClaims : Map[String,AnyRef] = claims.getCustomClaims.toMap // convert Java map to Scala map
      Success((claims.getSubject,customClaims))
    }

    def extract(token : String) : Try[(String,Map[String,AnyRef])] = {
      decode(token).flatMap(process)
    }

    def refresh(originalToken : String,refreshToken : String) : Try[String] = {
      Try {
        val refresh = decode(refreshToken,validate = false).getOrElse(throw new Exception("Refresh token not valid"))
        // jump through some hoops to prevent a NPE
        val maybeRefreshJWTID = refresh.getStringClaim("originalJwtID")
        val refreshJWTID = if(maybeRefreshJWTID == null) throw new Exception("Refresh token originalJwtID not valid") else maybeRefreshJWTID

        val original = decode(originalToken,validate = false).getOrElse(throw new Exception("Original token not valid"))

        if(refresh.getSubject != original.getSubject) throw new Exception("Refresh token subject not valid")
        if(refreshJWTID != original.getJWTID) throw new Exception("Refresh token does not match the original")

        encode(original.getSubject,original.getCustomClaims.toMap)
      }
    }

  }
}