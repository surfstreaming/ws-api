package models.auth

import com.surf.graph.GraphAPIModule
import play.api.libs.json._
import models.ApiException
import play.api.libs.json.JsString


class AuthenticationException extends ApiException("Incorrect Username or Password") {
  val error = "AuthenticationError"
  val code = 401
  val details = JsString(this.getMessage)
}
class AuthorizationException(message : String) extends ApiException(message) {
  val error = "AuthorizationException"
  val code = 401
  val details = JsString(this.getMessage)
}

trait AuthObjects {
  this: GraphAPIModule =>

  object authObjects {
    case class AuthToken(id : objects.idType, username: String, access: Access)
    case class Authorization(id : objects.idType, username: String, firstName : String, access: Access, token : String, refreshToken : String)
  }

}
/*
case class AuthTokenx(id : Long, username: String, access: Access)

case class Authorizationx(id : Long, username: String, firstName : String, access: Access, token : String, refreshToken : String)
object Authorization{
  implicit val writes : Writes[Authorization] = (
    (JsPath \ "id").write[Long] and
      (JsPath \ "username").write[String] and
      (JsPath \ "firstName").write[String] and
      (JsPath \ "access").write[Access] and
      (JsPath \ "token").write[String] and
      (JsPath \ "refreshToken").write[String]
    )(unlift(Authorization.unapply))
  implicit val reads = Json.reads[Authorization]
}
*/

case class Signup(email: String, username: String, password : String, passwordConfirm : String)
object Signup {
  implicit val reads = Json.reads[Signup]
}
