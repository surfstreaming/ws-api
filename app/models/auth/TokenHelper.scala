package models.auth

import java.security.interfaces.{RSAPublicKey, RSAPrivateKey}
import java.security.KeyFactory
import java.security.spec.{X509EncodedKeySpec, PKCS8EncodedKeySpec}
import play.api.Play
import play.api.Play.current
import java.io.FileNotFoundException

/**
 * Created by dan on 4/11/14.
 * adapted from https://bitbucket.org/connect2id/nimbus-jose-jwt/src/0ede333e8f5610d82ea3a01a03521ad575e19366/src/test/java/com/nimbusds/jwt/EncryptedJWTTest.java?at=master
 */
case class InvalidTokenException(message: String) extends Exception(message)
class ExpiredTokenException extends Exception("token has expired")
case class InvalidJWTIssuerException(message: String) extends Exception(message)

trait TokenHelperModule {
  val tokenHelper : TokenHelper
  trait TokenHelper {
    // http://stackoverflow.com/questions/3441501/java-asymmetric-encryption-preferred-way-to-store-public-private-keys
    def makePrivateKey(location : String) : RSAPrivateKey = {
      val keyFactory = KeyFactory.getInstance("RSA")
      val bytes = getFileBytes(location)

      val spec = new PKCS8EncodedKeySpec(bytes)
      val privateKey = keyFactory.generatePrivate(spec)
      privateKey match {
        case key : RSAPrivateKey => key
        case _ => throw new ClassCastException
      }
    }

    def makePublicKey(location : String) : RSAPublicKey = {
      val keyFactory = KeyFactory.getInstance("RSA")
      val bytes = getFileBytes(location)

      val spec = new X509EncodedKeySpec(bytes)
      val publicKey = keyFactory.generatePublic(spec)
      publicKey match {
        case key : RSAPublicKey => key
        case _ => throw new ClassCastException
      }
    }
    def getFileBytes(location : String) : Array[Byte]
  }
}

trait ProdTokenHelperModuleImpl extends TokenHelperModule {

  val tokenHelper = new ProdTokenHelper

  class ProdTokenHelper extends TokenHelper {
    def getFileBytes(location : String) : Array[Byte] = {
      //http://stackoverflow.com/questions/7598135/how-to-read-a-file-as-a-byte-array-in-scala
      //val in = ClassLoader.getSystemClassLoader.getResourceAsStream("resources/"+location)
      //val in = Play.classloader.getResourceAsStream("resources/"+location)
      Play.application.resourceAsStream("resources/"+location).map { in =>
        val bytes = Stream.continually(in.read).takeWhile(-1 !=).map(_.toByte).toArray
        in.close()

        bytes
      }.getOrElse(throw new FileNotFoundException("failed to load resource "+location))
    }
  }
}
trait TestTokenHelperModuleImpl extends TokenHelperModule {
  val tokenHelper = new TestTokenHelper

  class TestTokenHelper extends TokenHelper {
    def getFileBytes(location : String) : Array[Byte] = {
      //http://stackoverflow.com/questions/7598135/how-to-read-a-file-as-a-byte-array-in-scala
      val in = ClassLoader.getSystemClassLoader.getResourceAsStream("resources/"+location)
      if(in == null) throw new FileNotFoundException("cannot load resource "+location)
      //Play.application.resourceAsStream("resources/"+location).map { in =>
      val bytes = Stream.continually(in.read).takeWhile(-1 !=).map(_.toByte).toArray
      in.close()

      bytes
    }
  }
}