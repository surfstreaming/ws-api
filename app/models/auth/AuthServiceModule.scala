package models.auth

import com.nimbusds.jwt.ReadOnlyJWTClaimsSet
import models.mail.MailServiceModule
import play.api.Logger

import scala.util.{Failure, Success, Try}
import models.user.{User, UserServiceModule}
import org.mindrot.jbcrypt.BCrypt
import com.surf.graph.{GraphAPIModule, ObjectNotFoundException}
import scala.concurrent.Future
import models.{ConstantsModule, ObjectExistsApiException}
import play.api.libs.concurrent.Execution.Implicits.defaultContext

/**
 * Service to manage authentication and authorization
 */
trait AuthServiceModule {
  this: GraphAPIModule with AuthObjects =>

  val authService : AuthService

  trait AuthService {
    import objects._
    import authObjects._
    def authenticate(username : String, password: String) : Future[Authorization]
    def validate(token : String) : Future[ReadOnlyJWTClaimsSet]
    def register(authOpt : Option[AuthToken], email : String) : Future[String]
    def signup(authOpt : Option[AuthToken], user : User) : Future[GraphVertex[User]]
    def logout(token : String, refreshToken : String) : Future[String]
    def changePassword(id : idType, password : String)  : Future[GraphVertex[User]]
    def resetPassword(token : String, password : String) : Future[GraphVertex[User]]
    def generateResetToken(userId : idType, expirationDays : Int = 30) : String
    def encryptPassword(password : String) : String
    def checkPassword(password : String, encrypted : String) : Boolean
    def getUserByToken(token : String) : Try[AuthToken]
    def usernameAvailable(username : String) : Future[Boolean]
    def emailAvailable(email : String) : Future[Boolean]
  }
}
trait AuthServiceModuleImpl extends AuthServiceModule {
  this: TokenModule with UserServiceModule with GraphAPIModule with AuthObjects with MailServiceModule with ConstantsModule =>
  val authService = new AuthServiceImpl

  class AuthServiceImpl extends AuthService {
    import objects._
    import authObjects._
    def authenticate(username : String, password: String) = {
      // If there is an @ in the username, search by email, otherwise by username
      Logger.debug(s"Authentication attempt - $username")

      {
        if(username.contains("@")) userService.getUserByEmail(username.toLowerCase)
        else userService.getUserByUsername(username.toLowerCase)
      }
      .recoverWith { // here we are just mapping to a more user friendly exception
        case e : ObjectNotFoundException => Logger.warn(s"Logging attempt failed - $username does not exist"); Future.failed(new AuthenticationException)
      }
      .map { user =>
        Logger.debug(s"Found - $username - ${user.id}")
        // Return the found user if the input password matches what we expect
        if(checkPassword(password, user.obj.password)){
          Logger.warn(s"Authentication success - $username - getting Authorization")
          val auth = getAuthorizationTokens(user)
          Logger.debug(auth.toString)
          auth
        }
        else {
          Logger.warn(s"Invalid authentication attempt for $username ($password / ${user.obj.password})")
          throw new AuthenticationException
        }
      }
    }
    def validate(token : String) : Future[ReadOnlyJWTClaimsSet] = {
      tokenService.decode(token,validate = true) match {
        case Success(s) => Future.successful(s)
        case Failure(e) => Future.failed(e)
      }
    }
    def register(authOpt : Option[AuthToken], email : String) : Future[String] = {
      val clean = email.toLowerCase
      userService.getUserByEmail(clean)
      .map(_ => throw new ObjectExistsApiException(s"Email is already registered")) // return false if username exists
      .recover {
        case e : ObjectNotFoundException => true // return true if username doesn't exist
      }.map{ available =>
        // generate Token
        val token = tokenService.encode(email,Map(),60*24*7)
        // Send email with link to token
        val subject = "[Surf] Please complete your registration"
        val html = views.html.mail.register.render(constants.webUI,token)
        mailService.send(to = email, from = constants.mailFrom,subject,html.toString(),bcc = constants.mailBcc)
        token
      }
    }
    def signup(authOpt : Option[AuthToken], user : User) : Future[GraphVertex[User]] = {
      val accessNeeded = authOpt.map(_.access).getOrElse(NormalUser)
      // If you are anonymous you should only be allowed to create a NormalUser
      if(user.access != accessNeeded) {
        Future.failed(new AuthorizationException("Not Authorized"))
      } else {
        userService.create(user).map{ user =>
          val subject = "[Surf] Welcome to Surf!"
          val html = views.html.mail.welcome.render(constants.webUI,user.obj)
          mailService.send(user.obj.email,constants.mailFrom,subject,html.toString(),constants.mailBcc)
          user
        }
      }
    }
    def logout(token : String, refreshToken : String) = {
      // TODO we should invalidate the refresh token and consider ignoring the current token
      // perhaps stuff it in the cache?
      Future.successful("Logout Successful")
    }

    def changePassword(id : idType, password : String) = {
      // encrypt the password and then send it to the user service to persist the change
      val encryptedPassword = encryptPassword(password)
      userService.setPassword(id, encryptedPassword)
    }

    def resetPassword(token : String, password : String) = {
      // Decode the input token to confirm that it is valid and extract the claims within
      tokenService.decode(token)
      .map { claims =>
        // The "subject" of the claim is the vertex ID of the user
        val userId = claims.getSubject.asInstanceOf[idType] // TODO unsafe conversion

        changePassword(userId,password)
      }.get
    }

    def generateResetToken(userId : idType, expirationDays : Int = 30) : String = {
      // Since we assigned a random password to the user, we need a token that will allow the user to click from their email to set a password
      val claims = Map("reset"->new java.util.Date().getTime.toString)
      tokenService.encode(userId.toString,claims,60*24*expirationDays) // token expires in 30 days
    }

    def encryptPassword(password : String) : String = {
      BCrypt.hashpw(password, BCrypt.gensalt())
    }
    def checkPassword(password : String, encrypted : String) = {
      // Check if the unencrypted input password matches the encrypted version
      BCrypt.checkpw(password, encrypted)
    }
    def getAuthorizationTokens(user : GraphVertex[User]) : Authorization = {
      val customClaims = Map("access"->user.obj.access.toString,"username"->user.obj.username)
      val (token, refreshToken) = tokenService.newTokenWithRefresh(user.id.toString, customClaims)
      Authorization(user.id, user.obj.username, user.obj.firstName, user.obj.access, token, refreshToken)
    }

    def getUserByToken(token : String) = {
      tokenService.extract(token)
      .map { result =>
        val subject = result._1
        val claims = result._2
        // TODO throw API exceptions instead of generic ones
        val username : String = claims.getOrElse("username", throw new Exception("username not specified in token claims")).toString
        val access = claims.getOrElse("access", throw new Exception("access level not specified in token claims")).toString
        claims.get("username")
        // TODO this is very unsafe
        AuthToken(subject.toLong.asInstanceOf[idType],username,Access.valueOf(access))
      }
    }

    def usernameAvailable(username : String) : Future[Boolean] = {
      val clean = username.toLowerCase
      userService.getUserByUsername(clean)
      .map(_ => throw new ObjectExistsApiException(s"Username is not available")) // return false if username exists
      .recover {
        case e : ObjectNotFoundException => true // return true if username doesn't exist
      }
    }
    def emailAvailable(email : String) : Future[Boolean] = {
      val clean = email.toLowerCase
      userService.getUserByEmail(clean)
        .map(_ => throw new ObjectExistsApiException(s"Email is already registered")) // return false if username exists
        .recover {
        case e : ObjectNotFoundException => true // return true if username doesn't exist
      }
    }
  }
}

