package models.auth

import com.nimbusds.jwt.ReadOnlyJWTClaimsSet
import com.surf.graph.GraphAPIModule
import play.api.Logger
import play.api.libs.functional.syntax._
import play.api.libs.json._
import models.{GraphAPIModuleWithJson, JsonHelper}
import models.user.User
import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import scala.collection.JavaConverters._
import java.util.{Map => JMap}

import scala.util.Try

/**
 * Module to manage json input/output for AuthService
 */
trait AuthJsonModule {
  this: GraphAPIModule with AuthObjects =>
  val authJson : AuthJson

  trait AuthJson {
    import objects._
    import authObjects._
    def authenticate(json : JsValue) : Future[JsValue]
    def validate(token : JsValue) : Future[JsValue]
    def signup(authOpt : Option[AuthToken], json : JsValue) : Future[JsValue]
    def register(authOpt : Option[AuthToken], json : JsValue) : Future[JsValue]
    def logout(id : idType, json : JsValue) : Future[JsValue]
    def changePassword(id : idType, json : JsValue)  : Future[JsValue]
    def resetPassword(json : JsValue) : Future[JsValue]
    def usernameAvailable(username : String) : Future[JsValue]
    def emailAvailable(email : String) : Future[JsValue]
  }
}
trait AuthJsonModuleImpl extends AuthJsonModule {
  this: AuthServiceModule with GraphAPIModuleWithJson with AuthObjects =>
  val authJson = new AuthJsonImpl

  implicit lazy val writesAuthorization : Writes[authObjects.Authorization] = (
      (JsPath \ "id").write[objects.idType] and
      (JsPath \ "username").write[String] and
      (JsPath \ "firstName").write[String] and
      (JsPath \ "access").write[Access] and
      (JsPath \ "token").write[String] and
      (JsPath \ "refreshToken").write[String]
    )(unlift(authObjects.Authorization.unapply))
  implicit lazy val readsAuthorization : Reads[authObjects.Authorization] = (
      (JsPath \ "id").read[objects.idType] and
      (JsPath \ "username").read[String] and
      (JsPath \ "firstName").read[String] and
      (JsPath \ "access").read[Access] and
      (JsPath \ "token").read[String] and
      (JsPath \ "refreshToken").read[String]
    )(authObjects.Authorization.apply _)

  implicit val writesToken : Writes[ReadOnlyJWTClaimsSet] = new Writes[ReadOnlyJWTClaimsSet]{
    def makeStringMap(map : JMap[String,AnyRef]) : Map[String,String] = {
      map.asScala.map(pair => (pair._1,pair._2.toString)).toMap
    }
    def writes(value: ReadOnlyJWTClaimsSet): JsValue = Json.obj(
      "subject" -> value.getSubject,
      "claims" -> Json.toJson(makeStringMap(value.getCustomClaims)),
      "expires" -> value.getExpirationTime
    )
  }

  case class ResetPassword(token : String, password : String)
  object ResetPassword {
    implicit val reads = Json.reads[ResetPassword]
  }

  case class ChangePassword(current : String, password : String, confirm : String)
  object ChangePassword {
    implicit val reads = Json.reads[ChangePassword]
  }

  case class Logout(token : String, refreshToken : String)
  object Logout {
    implicit val reads = Json.reads[Logout]
  }

  case class Login(username: String, password : String)
  object Login{
    implicit val loginReads: Reads[Login] = Json.reads[Login]
    implicit val loginWrites: Writes[Login] = Json.writes[Login]
  }

  class AuthJsonImpl extends AuthJson with JsonHelper {
    import objects.idType
    import authObjects._
    def authenticate(json : JsValue) : Future[JsValue] = WithFutureJson[Login](json){ request =>
      authService.authenticate(request.username,request.password)
        .transform(
          {auth => Logger.debug("Auth success"); Json.toJson(auth)},
          {error => Logger.debug(s"Failure - ${error.getMessage}"); error}
        )
    }
    def validate(json : JsValue) : Future[JsValue] = WithFutureJson[String](json \ "token"){ token =>
        authService.validate(token).map(Json.toJson(_))
    }
    def register(authOpt : Option[AuthToken], json : JsValue) : Future[JsValue] = WithFutureJson[String](json \ "email"){ email =>
        authService.register(authOpt,email).map(x => Json.obj("result"->"ok"))
    }
    def signup(authOpt : Option[AuthToken], json : JsValue) : Future[JsValue] = WithFutureJson[User](json){ user =>
      authService.signup(authOpt,user).map(Json.toJson(_))
    }

    def logout(id : idType, json : JsValue) : Future[JsValue] = WithFutureJson[Logout](json){ request =>
      authService.logout(request.token,request.refreshToken).map(Json.toJson(_))
    }

    def changePassword(id : idType, json : JsValue)  : Future[JsValue] = WithFutureJson[ChangePassword](json){ request =>
        authService.changePassword(id,request.password)
          .map(_ => "Successfully changed password")
          .map(Json.toJson(_))
    }

    def resetPassword(json : JsValue) : Future[JsValue] = WithFutureJson[ResetPassword](json){ request =>
        authService.resetPassword(request.token,request.password)
        .map(_ => "Successfully reset password")
        .map(Json.toJson(_))
    }
    def usernameAvailable(username : String) : Future[JsValue] = {
      authService.usernameAvailable(username)
        .map(_ => Json.obj("result"->"available"))
    }
    def emailAvailable(email : String) : Future[JsValue] = {
      authService.emailAvailable(email)
        .map(_ => Json.obj("result"->"available"))
    }
  }
}
