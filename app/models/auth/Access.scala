package models.auth

import play.api.libs.json._
import scala.util.Try
import play.api.Logger

sealed trait Access extends Ordered[Access] {
  val level : Int
  override def compare(that: Access): Int = {
    this.level.compare(that.level)
  }
}
case object GodMode extends Access {
  val level = 5
}
case object PlatformAdmin extends Access {
  val level = 4
}
case object OrganizationAdmin extends Access {
  val level = 3
}
case object Administrator extends Access {
  val level = 2
}
case object NormalUser extends Access {
  val level = 1
}
case object AnonymousAccess extends Access {
  val level = 0
}

object Access {

  implicit val writes : Writes[Access] = new Writes[Access]{
    def writes(value: Access): JsValue = JsString(value.toString)
  }
  implicit val reads : Reads[Access] = new Reads[Access] {
    def reads(json: JsValue) : JsResult[Access] = {
      Logger.info(Json.prettyPrint(json))
      Try(valueOf(
        json.as[String]
      ))
      .map(JsSuccess(_))
      .getOrElse(JsError("could not read access"))
    }
  }
  def valueOf(value: String): Access = value match {
    case "GodMode" => GodMode
    case "PlatformAdmin" => PlatformAdmin
    case "OrganizationAdmin" => OrganizationAdmin
    case "Administrator" => Administrator
    case "NormalUser"    => NormalUser
    case "AnonymousAccess"    => AnonymousAccess
    case _ => throw new IllegalArgumentException()
  }

}