package models.queues

import com.surf.graph._
import com.tinkerpop.blueprints.{Vertex, Direction}

import play.api.libs.concurrent.Execution.Implicits.defaultContext

import scala.concurrent.Future

/**
 * Queue Favorites Persistence
 */
trait QueueEdgePersistenceModule {
  this: GraphAPIModule =>
  val queueEdgePersistence :  QueueEdgePersistence

  trait QueueEdgePersistence {
    import objects._
    def edgeList[E : EdgeHelper](vertexId : idType) : Future[Seq[GraphVertex[Queue]]]
    def edgeListAnonymous[E : EdgeHelper](vertexId : idType) : Future[Seq[GraphVertex[Queue]]]
    def edgeGet[V : VertexHelper, E : EdgeHelper](vertexId : idType, edgeId : edgeIdType) : Future[Segment[V,E,Queue]]
    def edgeCreate[V : VertexHelper, E : EdgeHelper](vertexId : idType, edge : E, queueId : idType) : Future[Segment[V,E,Queue]]
    def edgeUpdate[V : VertexHelper, E : EdgeHelper](vertexId : idType, edgeId : edgeIdType, edge : E) : Future[Segment[V,E,Queue]]
    def edgeDelete[E : EdgeHelper](vertexId : idType, edgeId : edgeIdType) : Future[String]
    def edgeExists[V : VertexHelper, E : EdgeHelper](vertexId : idType, queueId : idType) : Future[Option[Segment[V,E,Queue]]]
  }
}
trait QueueEdgePersistenceModuleImpl extends QueueEdgePersistenceModule {
  this: GraphAPIModule =>
  val queueEdgePersistence = new Object with QueueEdgePersistenceImpl

  trait QueueEdgePersistenceImpl extends QueueEdgePersistence {
    import objects._
    def edgeList[E : EdgeHelper](vertexId : idType) : Future[Seq[GraphVertex[Queue]]] = {

      graph.queryV[Queue](vertexId)(_.out(implicitly[EdgeHelper[E]].label))
    }
    def edgeListAnonymous[E : EdgeHelper](vertexId : idType) : Future[Seq[GraphVertex[Queue]]] = {
      graph.queryV[Queue]("User:username","surf")(_.out(implicitly[EdgeHelper[E]].label))
    }
    def edgeGet[V : VertexHelper, E : EdgeHelper](vertexId : idType, edgeId : edgeIdType) : Future[Segment[V,E,Queue]] = {
      graph.getSegment[V,E,Queue](edgeId)
    }
    def edgeCreate[V : VertexHelper, E : EdgeHelper](vertexId : idType, edge : E, queueId : idType) : Future[Segment[V,E,Queue]] = {
      val queueFuture = graph.get[Queue](queueId) // retrieve the queue
      val vertexFuture = graph.get[V](vertexId) // retrieve the user
      for {
        queue <- queueFuture
        vertex <- vertexFuture
        edge <- graph.createSegmentUnique(vertex,edge,queue,Direction.OUT)
      } yield edge
    }

    def edgeUpdate[V : VertexHelper, E : EdgeHelper](vertexId : idType, edgeId : edgeIdType, edge : E) : Future[Segment[V,E,Queue]] = {
      throw new UnsupportedOperationException
    }
    def edgeDelete[E : EdgeHelper](vertexId : idType, edgeId : edgeIdType) : Future[String] = {
      graph.getEdge[E](edgeId)
        .flatMap(graph.delete(_))
    }
    def edgeExists[V : VertexHelper, E : EdgeHelper](vertexId : idType, queueId : idType) : Future[Option[Segment[V,E,Queue]]] = {
      graph.getSegmentFromVertices[V,E,Queue](vertexId,queueId,implicitly[EdgeHelper[E]].label)
    }
  }
}
