package models.queues

import com.surf.graph.GraphAPIModule
import com.surf.graph.json.GraphJsonWrites
import play.api.Logger

import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json._
import models.JsonHelper


/**
 * Module to marshal Queue objects to/from json
 */
trait QueueJsonModule {
  this: GraphAPIModule =>

  val queueJson : QueueJson

  trait QueueJson {
    import objects._
    def list(requestorOpt : Option[idType], userId : idType, components : Seq[String] = Seq()) : Future[JsValue]
    def get(requestorOpt : Option[idType], userId : idType, queueId : idType, components : Seq[String] = Seq()) : Future[JsValue]
    def create(requestor : idType, userId : idType, json : JsValue) : Future[JsValue]
    def update(requestor : idType, userId : idType, queueId : idType, json : JsValue) : Future[JsValue]
    def delete(requestor : idType, userId : idType, queueId : idType) : Future[JsValue]

    def itemList(requestorOpt : Option[idType], userId : idType, queueId : idType, limit : Option[Int]) : Future[JsValue]
    def itemGet(requestorOpt : Option[idType], userId : idType, queueId : idType, itemId : idType) : Future[JsValue]
    def itemCreate(requestor : idType, userId : idType, queueId : idType, json : JsValue) : Future[JsValue]
    def itemUpdate(requestor : idType, userId : idType, queueId : idType, itemId : idType, json : JsValue) : Future[JsValue]
    def itemDelete(requestor : idType, userId : idType, queueId : idType, itemId : idType) : Future[JsValue]

    def favoriteList(requestorOpt : Option[idType], userId : idType, components : Seq[String]) : Future[JsValue]
    def favoriteGet(requestorOpt : Option[idType], userId : idType, favoriteId : edgeIdType) : Future[JsValue]
    def favoriteCreate(requestor : idType, userId : idType, json : JsValue) : Future[JsValue]
    def favoriteUpdate(requestor : idType, userId : idType, favoriteId : edgeIdType, json : JsValue) : Future[JsValue]
    def favoriteDelete(requestor : idType, userId : idType, favoriteId : edgeIdType) : Future[JsValue]

    def likesList(requestorOpt : Option[idType], userId : idType, components : Seq[String]) : Future[JsValue]
    def likesGet(requestorOpt : Option[idType], userId : idType, likeId : edgeIdType) : Future[JsValue]
    def likesCreate(requestor : idType, userId : idType, json : JsValue) : Future[JsValue]
    def likesUpdate(requestor : idType, userId : idType, likesId : edgeIdType, json : JsValue) : Future[JsValue]
    def likesDelete(requestor : idType, userId : idType, likesId : edgeIdType) : Future[JsValue]
  }
}
trait QueueJsonModuleImpl extends QueueJsonModule with JsonHelper {
  this: QueueServiceModule with GraphAPIModule with GraphJsonWrites =>
  val queueJson = new QueueJsonImpl

  class QueueJsonImpl extends QueueJson {
    import objects._

    case class NewQueueItemRequest(item : QueueItem, contentId : objects.idType)
    object NewQueueItemRequest {
      implicit val reads = Json.reads[NewQueueItemRequest]
    }

    def list(requestorOpt : Option[idType], userId : idType, components : Seq[String]) : Future[JsValue] = {
      queueService.list(requestorOpt,userId)
      .flatMap{ queues =>
        Future.sequence(queues.map(getComponents(requestorOpt,userId,_,components)))
      }.map(Json.toJson(_))
    }

    def get(requestorOpt : Option[idType], userId : idType, queueId : idType, components : Seq[String]) : Future[JsValue] = {
      queueService.get(requestorOpt,userId,queueId)
        .flatMap(getComponents(requestorOpt,userId,_,components))
    }

    def getComponents(requestorOpt : Option[idType], userId : idType, queue : GraphVertex[Queue], components : Seq[String]) : Future[JsValue] = {
      if(components.size.equals(0)) {
        Future.successful(Json.toJson(queue))
      }
      else {
        val futures = components.map { component =>
          getComponent(requestorOpt,userId,queue,component).map(jsresult => component -> jsresult)
        }

        Future.sequence(futures).map { components =>
          val base = Json.toJson(queue)
          val addition = Json.obj("components" -> Json.toJson(components.toMap[String, JsValue]))
          insertObject(base, addition)
        }
      }
    }

    def getComponent(requestorOpt : Option[idType], userId : idType, queue : GraphVertex[Queue], component : String) : Future[JsValue] = component match {
      case "first_ten" => itemList(requestorOpt,userId, queue.id, Some(10))
      case "items" => itemList(requestorOpt,userId, queue.id, None)
      case "user" => queueService.getUser(queue).map(Json.toJson(_))
      case "splash_images" => queueService.getSplashImages(queue).map(Json.toJson(_))
      case "favorite" => requestorOpt.map { requestor =>
        queueService.edgeExists[UserFavoriteQueueEdge](requestorOpt,requestor,queue.id).map{x => Logger.debug(x.toString); x.map(_.edge.id)}
      }.getOrElse(Future.successful(None)).map(Json.toJson(_))
      case "favorite_count" => queueService.favoriteCount(queue).map(JsNumber(_))
      case "likes" => requestorOpt.map { requestor =>
        queueService.edgeExists[UserLikesQueueEdge](requestorOpt,requestor,queue.id).map(x => x.map(_.edge.id))
      }.getOrElse(Future.successful(None)).map(Json.toJson(_))
      case "like_count" => queueService.likeCount(queue).map(JsNumber(_))
      case "" => Future.successful(JsString("Not implemented"))
    }


    def create(requestor : idType, userId : idType, json : JsValue) : Future[JsValue] = WithFutureJson[Queue](json){ queue =>
      queueService.create(requestor,requestor,queue).map(Json.toJson(_))
    }
    def update(requestor : idType, userId : idType, queueId : idType, json : JsValue) : Future[JsValue] = WithFutureJson[Queue](json){ queue =>
      queueService.update(requestor,requestor,queueId,queue).map(Json.toJson(_))
    }
    def delete(requestor : idType, userId : idType, queueId : idType) : Future[JsValue] = {
      queueService.delete(requestor,requestor,queueId).map(Json.toJson(_))
    }

    def itemList(requestorOpt : Option[idType], userId : idType, queueId : idType, limit : Option[Int]) : Future[JsValue] = {
      queueService.itemList(requestorOpt,userId,queueId).map{ results =>

        Json.toJson(
          limit.map(results.take).getOrElse(results).map( result => Json.obj( "item" -> result.v1, "content" -> result.v2)  )
        )
      }
    }
    def itemGet(requestorOpt : Option[idType], userId : idType, queueId : idType, itemId : idType) : Future[JsValue] = {
      queueService.itemGet(requestorOpt,userId,queueId,itemId).map(Json.toJson(_))
    }
    def itemCreate(requestor : idType, userId : idType, queueId : idType, json : JsValue) : Future[JsValue] = WithFutureJson[NewQueueItemRequest](json){ itemRequest =>
      queueService.itemCreate(requestor,userId,queueId,itemRequest.item,itemRequest.contentId).map(Json.toJson(_))
    }
    def itemUpdate(requestor : idType, userId : idType, queueId : idType, itemId : idType, json : JsValue) : Future[JsValue] = WithFutureJson[QueueItem](json){ item =>
      queueService.itemUpdate(requestor,userId,queueId,itemId,item).map(Json.toJson(_))
    }
    def itemDelete(requestor : idType, userId : idType, queueId : idType, itemId : idType) : Future[JsValue] = {
      queueService.itemDelete(requestor,userId,queueId,itemId).map(Json.toJson(_))
    }
    def favoriteList(requestorOpt : Option[idType], userId : idType, components : Seq[String]) : Future[JsValue] = {
      queueService.edgeList[UserFavoriteQueueEdge](requestorOpt,userId)
      .flatMap{ queues =>
        Future.sequence(queues.map(getComponents(requestorOpt,userId,_,components)))
      }.map(Json.toJson(_))
    }
    def favoriteGet(requestorOpt : Option[idType], userId : idType, favoriteId : edgeIdType) : Future[JsValue] = {
      queueService.edgeGet[UserFavoriteQueueEdge](requestorOpt,userId,favoriteId)
        //.map(Json.toJson(_))
        .map(x => Json.toJson(Json.obj("user" -> x.v1, "queue" -> x.v2)))
    }
    def favoriteCreate(requestor : idType, userId : idType, json : JsValue) : Future[JsValue] = WithFutureJson[idType](json \ "queueId"){ queueId =>
      queueService.edgeCreate(requestor,userId,UserFavoriteQueueEdge(),queueId)
        //.map(Json.toJson(_))
        .map{x => Logger.debug(s"Edge created: ${x.edge.id}"); Json.toJson(x.edge.id)}
    }
    def favoriteUpdate(requestor : idType, userId : idType, favoriteId : edgeIdType, json : JsValue) = WithFutureJson[idType](json \ "queueId"){ queueId =>
      queueService.edgeUpdate[UserFavoriteQueueEdge](requestor,userId,favoriteId,UserFavoriteQueueEdge())
        //.map(Json.toJson(_))
        .map(x => Json.toJson(Json.obj("user" -> x.v1, "queue" -> x.v2)))
    }
    def favoriteDelete(requestor : idType, userId : idType, favoriteId : edgeIdType) : Future[JsValue] = {
      queueService.edgeDelete[UserFavoriteQueueEdge](requestor,userId,favoriteId)
        .map(Json.toJson(_))
    }

    def likesList(requestorOpt : Option[idType], userId : idType, components : Seq[String]) : Future[JsValue] = {
      queueService.edgeList[UserLikesQueueEdge](requestorOpt,userId)
        .flatMap{ queues =>
        Future.sequence(queues.map(getComponents(requestorOpt,userId,_,components)))
      }.map(Json.toJson(_))
    }
    def likesGet(requestorOpt : Option[idType], userId : idType, likesId : edgeIdType) : Future[JsValue] = {
      queueService.edgeGet[UserLikesQueueEdge](requestorOpt,userId,likesId)
        //.map(Json.toJson(_))
        .map(x => Json.toJson(Json.obj("user" -> x.v1, "queue" -> x.v2)))
    }
    def likesCreate(requestor : idType, userId : idType, json : JsValue) : Future[JsValue] = WithFutureJson[idType](json \ "queueId" ){ queueId =>
      queueService.edgeCreate(requestor,userId,UserLikesQueueEdge(),queueId)
        //.map(Json.toJson(_))
        .map(x => Json.toJson(x.edge.id))
    }
    def likesUpdate(requestor : idType, userId : idType, likesId : edgeIdType, json : JsValue) = WithFutureJson[idType](json \ "queueId"){ item =>
      queueService.edgeUpdate[UserLikesQueueEdge](requestor,userId,likesId,UserLikesQueueEdge())
        //.map(Json.toJson(_))
        .map(x => Json.toJson(Json.obj("user" -> x.v1, "queue" -> x.v2)))
    }
    def likesDelete(requestor : idType, userId : idType, likesId : edgeIdType) : Future[JsValue] = {
      queueService.edgeDelete[UserLikesQueueEdge](requestor,userId,likesId)
        .map(Json.toJson(_))
    }
  }
}

