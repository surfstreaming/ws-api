package models.queues

import com.surf.graph.{DefaultVertexHelper, SimpleEdgeHelper}
import play.api.libs.json.Json
import play.api.Logger


case class Queue (
  name: String,
  code: String,
  description: Option[String] = None,
  splash: Option[String] = None,
  access: String
)

object Queue {
  implicit val writes = Json.writes[Queue]
  implicit val reads = Json.reads[Queue]

  implicit object QueueVertexHelper extends DefaultVertexHelper[Queue] {
    val objectType = "Queue"
    val uniqueFields = List("Queue:code")

    def toObject(props : Map[String,Any]) : Queue = {
      Logger.info(props.toString())
      Queue (
        code = props.get("Queue:code").get.toString,
        name = props.get("Queue:name").get.toString,
        description = props.get("Queue:description").map(_.toString),
        splash = props.get("Queue:splash").map(_.toString),
        access = props.get("Queue:access").get.toString
      )
    }

    def toMap(queue : Queue) : Map[String,Any] = {
      Map(
        "Queue:code" -> queue.code,
        "Queue:name" -> queue.name,
        "Queue:description" -> queue.description,
        "Queue:splash" -> queue.splash,
        "Queue:access" -> queue.access
      )
    }
  }
}

case class QueueItem (
 notes : Option[String]
)

object QueueItem {
  implicit val writes = Json.writes[QueueItem]
  implicit val reads = Json.reads[QueueItem]

  implicit object QueueVertexHelper extends DefaultVertexHelper[QueueItem] {
    val objectType = "QueueItem"
    val uniqueFields = Seq()

    def toObject(props : Map[String,Any]) : QueueItem = {
      QueueItem (
        notes = props.get("QueueItem:notes").map(_.asInstanceOf[String])
      )
    }

    def toMap(item : QueueItem) : Map[String,Any] = {
      Map(
        "QueueItem:notes" -> item.notes
      )
    }
  }
}

case class CreatedQueueEdge()
object CreatedQueueEdge {
  implicit object Helper extends SimpleEdgeHelper[CreatedQueueEdge]  {
    val label = "created_queue"
    def toObj(props : Map[String,Any]) = CreatedQueueEdge()
  }
}

case class QueueItemContentEdge()
object QueueItemContentEdge {
  implicit object Helper extends SimpleEdgeHelper[QueueItemContentEdge]  {
    val label = "queue_item_content"
    def toObj(props : Map[String,Any]) = QueueItemContentEdge()
  }
}
case class NextQueueItemEdge()
object NextQueueItemEdge {
  implicit object Helper extends SimpleEdgeHelper[NextQueueItemEdge]  {
    val label = "next_queue_item"
    def toObj(props : Map[String,Any]) = NextQueueItemEdge()
  }
}

case class ItemInQueueEdge()
object ItemInQueueEdge {
  implicit object Helper extends SimpleEdgeHelper[ItemInQueueEdge]  {
    val label = "in_queue"
    def toObj(props : Map[String,Any]) = ItemInQueueEdge()
  }
}

case class UserFavoriteQueueEdge()
object UserFavoriteQueueEdge {
  implicit object Helper extends SimpleEdgeHelper[UserFavoriteQueueEdge]  {
    val label = "favorite_queue"
    def toObj(props : Map[String,Any]) = UserFavoriteQueueEdge()
  }
}

case class UserLikesQueueEdge()
object UserLikesQueueEdge {
  implicit object Helper extends SimpleEdgeHelper[UserLikesQueueEdge]  {
    val label = "likes_queue"
    def toObj(props : Map[String,Any]) = UserLikesQueueEdge()
  }
}

case class DefaultQueueEdge()
object DefaultQueueEdge {
  implicit object Helper extends SimpleEdgeHelper[DefaultQueueEdge]  {
    val label = "default_queue"
    def toObj(props : Map[String,Any]) = DefaultQueueEdge()
  }
}