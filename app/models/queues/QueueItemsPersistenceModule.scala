package models.queues

import com.surf.core.objects.content.GenericContent
import com.surf.graph._

import scala.concurrent.Future
import com.tinkerpop.gremlin.scala.GremlinScalaPipeline
import com.tinkerpop.blueprints.{Direction, Vertex}
import scala.collection.JavaConversions._
import play.api.libs.concurrent.Execution.Implicits.defaultContext

/**
 * Queue Items Persistence
 */
trait QueueItemsPersistenceModule {
  this: GraphAPIModule =>
  val queueItemsPersistence : QueueItemsPersistence

  trait QueueItemsPersistence {
    import objects._
    def itemList(userId : idType, queueId : idType) : Future[Seq[Segment[QueueItem,QueueItemContentEdge,GenericContent]]]
    def itemGet(userId : idType, queueId : idType, itemId : idType) : Future[GraphVertex[QueueItem]]
    def itemCreate(userId : idType, queueId : idType, item : QueueItem, contentId : idType) : Future[GraphVertex[QueueItem]]
    def itemUpdate(userId : idType, queueId : idType, itemId : idType, item : QueueItem) : Future[GraphVertex[QueueItem]]
    def itemDelete(userId : idType, queueId : idType, itemId : idType) : Future[GraphVertex[QueueItem]]
  }
}

trait QueueItemsPersistenceModuleImpl extends QueueItemsPersistenceModule {
  this: GraphAPIModule =>
  val queueItemsPersistence = new Object with QueueItemsPersistenceImpl

  trait QueueItemsPersistenceImpl extends QueueItemsPersistence {
    import objects._
    private val nextQueueItemLabel = implicitly[EdgeHelper[NextQueueItemEdge]].label

    def itemList(userId : idType, queueId : idType) : Future[Seq[Segment[QueueItem,QueueItemContentEdge,GenericContent]]] = {

      graph.querySegments[QueueItem,QueueItemContentEdge,GenericContent](Direction.OUT,queueId){ pipe =>
        pipe.as("x")
          .out(nextQueueItemLabel)
          .loop("x",
            {whileLoop => whileLoop.getObject.getEdges(Direction.OUT,nextQueueItemLabel).size != 0} , // TODO should there be paging?
            emit => true
          )
      }
    }
    def itemGet(userId : idType, queueId : idType, itemId : idType) : Future[GraphVertex[QueueItem]] = {
      graph.get[QueueItem](itemId)
    }
    def itemCreate(userId : idType, queueId : idType, item : QueueItem, contentId : idType) : Future[GraphVertex[QueueItem]] = {
      // find the last item then add the vertex
      val emptyQueue = new GremlinScalaPipeline[Vertex,Vertex]
        .has("id",queueId)
        .filter(x => x.getEdges(Direction.OUT,nextQueueItemLabel).size == 0)

      val lastItem = new GremlinScalaPipeline[Vertex,Vertex]
        .has("id",queueId)
        .as("x")
        .out(nextQueueItemLabel)
        .loop(namedStep = "x",
          until =>  until.getObject.getEdges(Direction.OUT,nextQueueItemLabel).size != 0 , // TODO should there be paging?
          emit => emit.getObject.getEdges(Direction.OUT,nextQueueItemLabel).size == 0
        )


      val contentFuture = graph.get[GenericContent](contentId) // retrieve the content that is being added to a queue
      val lastItemFuture = graph.select[SimpleVertex](queueId)(_.or(emptyQueue,lastItem)) // retrieve the last item of the queue
      val queueFuture = graph.get[Queue](queueId) // retrieve the last item of the queue

      for {
        content <- contentFuture
        lastItem <- lastItemFuture
        newItem <- graph.create(item) // create the item vertex
        nextEdge <- graph.createSegment(lastItem,NextQueueItemEdge(),newItem,Direction.OUT) // link the last item to the next item
        contentEdge <- graph.createSegment(newItem,QueueItemContentEdge(),content,Direction.OUT) // link the new item to the content
        queue <- queueFuture
        queueItemEdge <- graph.createSegment(newItem,ItemInQueueEdge(),queue,Direction.OUT) // shortcut to get from queue item to queue rather than traversing "next item"
      } yield newItem
    }

    def itemOrder(userId : String, queueId : String, itemId : String, prevItemId : String) = {
      // get the itemId in question
      // get the "old" previous item .in("next_queue_item")
      // get the "old" next queue item .out("next_queue_item") (this may be null
      // get the "new" previous item .has("id",prevItemId)
      // get the "new" next item .out("next_queue_item")
      // remove edge between existing "new'
      // remove edges between "old"->(item)->"old"
      // create edge "new"->(item)->"new"
      // create edge "old"->"old"
    }
    def itemUpdate(userId : idType, queueId : idType, itemId : idType, item : QueueItem) : Future[GraphVertex[QueueItem]] = {
      throw new UnsupportedOperationException("not implemented yet")
    }
    def itemDelete(userId : idType, queueId : idType, itemId : idType) : Future[GraphVertex[QueueItem]] = {

      val itemFuture = graph.get[QueueItem](itemId)

      val prevFuture =  graph.select[QueueItem](itemId)(
        _.in(implicitly[EdgeHelper[NextQueueItemEdge]].label)
      )

      val nextOptFuture =  graph.select[QueueItem](itemId)(
        _.out(implicitly[EdgeHelper[NextQueueItemEdge]].label)
      ).map(Some(_))
        .recover{ case e : ObjectNotFoundException => None }


      def fixOrder(prev : GraphVertex[QueueItem], nextOpt : Option[GraphVertex[QueueItem]]) : Future[String] = {
        // if there is a next then we need to repoint the edge to it
        nextOpt.map { next =>
          graph.createSegment(prev,NextQueueItemEdge(),next,Direction.OUT).map(e => "created new segment")
        }.getOrElse(Future.successful("ok"))
      }

      for {
        item <- itemFuture // wait for the item to resolve
        deleted <- graph.delete(item) // delete the item from the graph
        prev <- prevFuture // wait for prev and next futures to resolve
        nextOpt <- nextOptFuture
        result <- fixOrder(prev,nextOpt)
      } yield item
    }
  }
}
