package models.queues

import com.surf.core.objects.content.GenericContent
import com.surf.core.objects.content.movies.ContentImage
import com.surf.graph.{GraphAPIModule, EdgeHelper}
import models.UnauthorizedException
import models.auth.AuthorizationException
import scala.concurrent.Future
import models.user.User
import play.api.Logger

/**
 * Module to create and retrieve queues
 */
trait QueueServiceModule {
  this: GraphAPIModule =>

  val queueService : QueueService

  trait QueueService {
    import objects._
    // queue basics
    def list(requestorOpt : Option[idType], userId : idType, components : Seq[String] = Seq()) : Future[Seq[GraphVertex[Queue]]]
    def get(requestorOpt : Option[idType], userId : idType, queueId : idType) : Future[GraphVertex[Queue]]
    def create(requestor : idType,userId : idType, queue : Queue) : Future[GraphVertex[Queue]]
    def createDefault(queue : Queue) : Future[GraphVertex[Queue]]
    def update(requestor : idType,userId : idType, queueId : idType, queue : Queue) : Future[GraphVertex[Queue]]
    def delete(requestor : idType, userId : idType, queueId : idType) : Future[GraphVertex[Queue]]

    // components
    def getUser(queue : GraphVertex[Queue]) : Future[GraphVertex[User]]
    def getSplashImages(queue : GraphVertex[Queue]) : Future[Seq[GraphVertex[ContentImage]]]
    def favoriteCount(queue : GraphVertex[Queue]) : Future[Long]
    def likeCount(queue : GraphVertex[Queue]) : Future[Long]

    // queue item basics
    def itemList(requestorOpt : Option[idType], userId : idType, queueId : idType) : Future[Seq[Segment[QueueItem,QueueItemContentEdge,GenericContent]]]
    def itemGet(requestorOpt : Option[idType], userId : idType, queueId : idType, itemId : idType) : Future[GraphVertex[QueueItem]]
    def itemCreate(requestor : idType,userId : idType, queueId : idType, item : QueueItem, contentId : idType) : Future[GraphVertex[QueueItem]]
    def itemUpdate(requestor : idType,userId : idType, queueId : idType, itemId : idType, item : QueueItem) : Future[GraphVertex[QueueItem]]
    def itemDelete(requestor : idType, userId : idType, queueId : idType, itemId : idType) : Future[GraphVertex[QueueItem]]


    /**
     * FAVORITES / LIKES
     */
    def edgeList[E : EdgeHelper](requestorOpt : Option[idType], userId : idType) : Future[Seq[GraphVertex[Queue]]]
    def edgeExists[E : EdgeHelper](requestorOpt : Option[idType], userId : idType, queueId : idType) : Future[Option[Segment[User,E,Queue]]]
    def edgeGet[E : EdgeHelper](requestorOpt : Option[idType], userId : idType, edgeId : edgeIdType) : Future[Segment[User,E,Queue]]
    def edgeCreate[E : EdgeHelper](requestor : idType, userId : idType, edge : E, queueId : idType) : Future[Segment[User,E,Queue]]
    def edgeUpdate[E : EdgeHelper](requestor : idType, userId : idType, edgeId : edgeIdType, edge : E) : Future[Segment[User,E,Queue]]
    def edgeDelete[E : EdgeHelper](requestor : idType, userId : idType, edgeId : edgeIdType) : Future[String]

    def getQueues(requestorOpt : Option[idType], userId : idType) : Future[Seq[GraphVertex[Queue]]]
    def getAllQueues(userId : idType) : Future[Seq[GraphVertex[Queue]]]
    def getPrivateQueues(userId : idType) : Future[Seq[GraphVertex[Queue]]]
    def getFriendQueues(userId : idType, friendId : idType, includePublic : Boolean = true) : Future[Seq[GraphVertex[Queue]]]
    def getPublicQueues(userId : idType) : Future[Seq[GraphVertex[Queue]]]
  }
}

trait QueueServiceModuleImpl extends QueueServiceModule {
  this: QueuePersistenceModule with GraphAPIModule =>
  val queueService = new QueueServiceImpl

  class QueueServiceImpl extends QueueService {
    import objects._
    // queue basics
    def list(requestorOpt : Option[idType], userId : idType, components : Seq[String]) : Future[Seq[GraphVertex[Queue]]] = {
      requestorOpt.map { requestor => // Execute if user is Authenticated
        if(requestor.equals(userId)){ // Authorized user retrieving their own Queues
          queuePersistence.getAllQueues(userId)
        } else { // Authorized user is retrieving a different User's Queues
          queuePersistence.getFriendQueues(requestor,userId,includePublic = true)
        }
      } getOrElse { // Execute if user is Anonymous
        queuePersistence.getPublicQueues(userId)
      }
    }
    def get(requestorOpt : Option[idType], userId : idType, queueId : idType) : Future[GraphVertex[Queue]] = {
      requestorOpt.map { requestor => // Execute if user is Authenticated
        if(requestor.equals(userId)){ // Authorized user retrieving their own Queues
          queuePersistence.get(queueId,userId)
        } else {
          // Authorized user is retrieving a different User's Queues
          queuePersistence.getFriendQueue(requestor,userId,queueId,includePublic = true)
        }
      } getOrElse { // Execute if user is Anonymous
        queuePersistence.getPublicQueue(queueId,userId)
      }
    }
    def create(requestorUserId : idType, userId : idType, queue : Queue) : Future[GraphVertex[Queue]] = {
      if(!requestorUserId.equals(userId)) throw new AuthorizationException("User not authorized to create queue")
      // Now create the queue
      queuePersistence.create(userId,queue)
    }
    def createDefault(queue : Queue) : Future[GraphVertex[Queue]] = {
      queuePersistence.createDefault(queue)
    }
    def update(requestor : idType,userId : idType, queueId : idType, queue : Queue) : Future[GraphVertex[Queue]] = {
      if(!requestor.equals(userId)) throw new AuthorizationException("User not authorized to update queue")
      // Now update the queue
      queuePersistence.update(userId,queueId,queue)
    }
    def delete(requestor : idType, userId : idType, queueId : idType) : Future[GraphVertex[Queue]] = {
      if(!requestor.equals(userId)) throw new AuthorizationException("User not authorized to delete queue")
      // Now delete the queue
      queuePersistence.delete(userId,queueId)
    }

    // components
    def getUser(queue : GraphVertex[Queue]) : Future[GraphVertex[User]] = queuePersistence.getUser(queue)
    def getSplashImages(queue : GraphVertex[Queue]) : Future[Seq[GraphVertex[ContentImage]]] = queuePersistence.getSplashImages(queue)
    def favoriteCount(queue : GraphVertex[Queue]) : Future[Long] = queuePersistence.favoriteCount(queue)
    def likeCount(queue : GraphVertex[Queue]) : Future[Long] = queuePersistence.likeCount(queue)

    /*
    * queue item basics
    */
    def itemList(requestorOpt : Option[idType], userId : idType, queueId : idType) : Future[Seq[Segment[QueueItem,QueueItemContentEdge,GenericContent]]] = {
      queuePersistence.itemList(userId,queueId)
    }
    def itemGet(requestorOpt : Option[idType], userId : idType, queueId : idType, itemId : idType) : Future[GraphVertex[QueueItem]] = {
      queuePersistence.itemGet(userId,queueId,itemId)
    }
    def itemCreate(requestor : idType,userId : idType, queueId : idType, item : QueueItem, contentId : idType) : Future[GraphVertex[QueueItem]] = {
      if(!requestor.equals(userId)) throw new AuthorizationException("User not authorized to create queue item")
      queuePersistence.itemCreate(userId,queueId,item,contentId)
    }
    def itemUpdate(requestor : idType,userId : idType, queueId : idType, itemId : idType, item : QueueItem) : Future[GraphVertex[QueueItem]] = {
      if(!requestor.equals(userId)) throw new AuthorizationException("User not authorized to update queue item")
      queuePersistence.itemUpdate(userId,queueId,itemId,item)
    }
    def itemDelete(requestor : idType, userId : idType, queueId : idType, itemId : idType) : Future[GraphVertex[QueueItem]] = {
      if(!requestor.equals(userId)) throw new AuthorizationException("User not authorized to delete queue item")
      queuePersistence.itemDelete(userId, queueId, itemId)
    }

    /**
     * FAVORITES
     */
    def edgeList[E : EdgeHelper](requestorOpt : Option[idType], userId : idType) : Future[Seq[GraphVertex[Queue]]] = {
      requestorOpt.map { requestor =>
        if(requestor != userId) throw new UnauthorizedException("Permission denied")
        queuePersistence.edgeList[E](userId)
      }.getOrElse {
        queuePersistence.edgeListAnonymous[E](userId)
      }
    }
    def edgeExists[E : EdgeHelper](requestorOpt : Option[idType], userId : idType, queueId : idType) : Future[Option[Segment[User,E,Queue]]] = {
      if(requestorOpt.isEmpty || requestorOpt.get != userId) throw new UnauthorizedException("Permission denied")
      queuePersistence.edgeExists[User,E](userId,queueId)
    }
    def edgeGet[E : EdgeHelper](requestorOpt : Option[idType], userId : idType, edgeId : edgeIdType) : Future[Segment[User,E,Queue]] = {
      if(requestorOpt.isEmpty || requestorOpt.get != userId) throw new UnauthorizedException("Permission denied")
      queuePersistence.edgeGet[User,E](userId,edgeId)
    }
    def edgeCreate[E : EdgeHelper](requestor : idType, userId : idType, edge : E, queueId : idType) : Future[Segment[User,E,Queue]] = {
      if(requestor != userId) throw new UnauthorizedException("Permission denied")
      queuePersistence.edgeCreate[User,E](userId,edge,queueId)
    }
    def edgeUpdate[E : EdgeHelper](requestor : idType, userId : idType, edgeId : edgeIdType, edge : E) : Future[Segment[User,E,Queue]] = {
      if(requestor != userId) throw new UnauthorizedException("Permission denied")
      queuePersistence.edgeUpdate[User,E](userId,edgeId,edge)
    }
    def edgeDelete[E : EdgeHelper](requestor : idType, userId : idType, edgeId : edgeIdType) : Future[String] = {
      if(requestor != userId) throw new UnauthorizedException("Permission denied")
      queuePersistence.edgeDelete[E](userId,edgeId)
    }

    /*
    old implementations start here
     */

    def getQueues(requestorOpt : Option[idType], userId : idType) : Future[Seq[GraphVertex[Queue]]] = {
      // authOpt contains the currently logged in user or None if Anonymous
      requestorOpt.map { requestor =>
        Logger.info(s"Getting queues for $userId on behalf of $requestor")
        if (requestor.equals(userId)) getAllQueues(userId) // If auth.id and requested user are the same, then they can access all queues
        else getFriendQueues(userId,requestor) // If user is authorized then only return queues then have access to
      }.getOrElse(getPublicQueues(userId)) // If the user is Anon, then only return Public queues
    }
    def getAllQueues(userId : idType) : Future[Seq[GraphVertex[Queue]]] = {
      queuePersistence.getAllQueues(userId)
    }
    def getPrivateQueues(userId : idType) : Future[Seq[GraphVertex[Queue]]] = {
      queuePersistence.getPrivateQueues(userId)
    }
    def getFriendQueues(userId : idType, friendId : idType, includePublic : Boolean) : Future[Seq[GraphVertex[Queue]]] = {
      queuePersistence.getFriendQueues(userId,friendId,includePublic)
    }
    def getPublicQueues(userId : idType) : Future[Seq[GraphVertex[Queue]]] = {
      queuePersistence.getPublicQueues(userId)
    }
  }
}