package models.queues

import com.surf.graph._
import com.tinkerpop.blueprints.Direction
import models.user.User

import play.api.libs.concurrent.Execution.Implicits.defaultContext

import scala.concurrent.Future

/**
 * Queue Favorites Persistence
 */
trait QueueFavoritesPersistenceModule {
  this: GraphAPIModule =>
  val queueFavoritesPersistence :  QueueFavoritesPersistence

  trait QueueFavoritesPersistence {
    import objects._
    def favoriteList(requestorOpt : Option[idType], userId : idType) : Future[Seq[GraphVertex[Queue]]]
    def favoriteGet(requestorOpt : Option[idType], userId : idType, favoriteId : edgeIdType) : Future[Segment[User,UserFavoriteQueueEdge,Queue]]
    def favoriteCreate(requestor : idType, userId : idType, queueId : idType) : Future[Segment[User,UserFavoriteQueueEdge,Queue]]
    def favoriteUpdate(requestor : idType, userId : idType, favoriteId : edgeIdType, queueId : idType) : Future[Segment[User,UserFavoriteQueueEdge,Queue]]
    def favoriteDelete(requestor : idType, userId : idType, favoriteId : edgeIdType) : Future[String]
    def favoriteExists(requestorOpt : Option[idType], userId : idType, queueId : idType) : Future[Option[Segment[User,UserFavoriteQueueEdge,Queue]]]
  }
}
trait QueueFavoritesPersistenceModuleImpl extends QueueFavoritesPersistenceModule {
  this: GraphAPIModule =>
  val queueFavoritesPersistence = new Object with QueueFavoritesPersistenceImpl

  trait QueueFavoritesPersistenceImpl extends QueueFavoritesPersistence {
    import objects._
    def favoriteList(requestorOpt : Option[idType], userId : idType) : Future[Seq[GraphVertex[Queue]]] = {
      requestorOpt.map { requestor =>
        graph.queryV[Queue](userId)(_.out(implicitly[EdgeHelper[UserFavoriteQueueEdge]].label))
      }.getOrElse {
        graph.queryV[Queue]("User:username","surf")(_.out(implicitly[EdgeHelper[DefaultQueueEdge]].label))
      }
    }
    def favoriteGet(requestorOpt : Option[idType], userId : idType, favoriteId : edgeIdType) : Future[Segment[User,UserFavoriteQueueEdge,Queue]] = {
      graph.getSegment[User,UserFavoriteQueueEdge,Queue](favoriteId)
    }
    def favoriteCreate(requestor : idType, userId : idType, queueId : idType) : Future[Segment[User,UserFavoriteQueueEdge,Queue]] = {
      val queueFuture = graph.get[Queue](queueId) // retrieve the queue
      val userFuture = graph.get[User](userId) // retrieve the user
      for {
        queue <- queueFuture
        user <- userFuture
        edge <- graph.createSegmentUnique(user,UserFavoriteQueueEdge(),queue,Direction.OUT)
      } yield edge
    }
    def favoriteUpdate(requestor : idType, userId : idType, favoriteId : edgeIdType, queueId : idType) : Future[Segment[User,UserFavoriteQueueEdge,Queue]] = {
      throw new UnsupportedOperationException
    }
    def favoriteDelete(requestor : idType, userId : idType, favoriteId : edgeIdType) : Future[String] = {
      graph.getEdge[UserFavoriteQueueEdge](favoriteId)
        .flatMap(graph.delete(_))
    }
    def favoriteExists(requestorOpt : Option[idType], userId : idType, queueId : idType) : Future[Option[Segment[User,UserFavoriteQueueEdge,Queue]]] = {
      graph.getSegmentFromVertices[User,UserFavoriteQueueEdge,Queue](userId,queueId,implicitly[EdgeHelper[UserFavoriteQueueEdge]].label)
    }
  }
}
