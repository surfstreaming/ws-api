package models.queues

import com.surf.core.objects.content.ContentEdgeObjects.BackdropsEdge
import com.surf.core.objects.content.movies.ContentImage
import com.surf.graph._
import com.tinkerpop.blueprints.{Direction, Vertex}
import com.tinkerpop.gremlin.Tokens.T
import com.tinkerpop.gremlin.scala.GremlinScalaPipeline
import models.{InvalidStateException, UnauthorizedException}
import models.user.{FriendsWithEdge, User}
import play.api.Logger

import scala.concurrent.Future

import scala.collection.JavaConversions._

import play.api.libs.concurrent.Execution.Implicits.defaultContext

trait QueueBasicsPersistenceModule {
  this: GraphAPIModule =>

  val queueBasicsPersistence : QueueBasicsPersistence
  trait QueueBasicsPersistence {
    import objects._
    def get(id : idType, userId : idType) : Future[GraphVertex[Queue]]
    def getFriendQueue(userId : idType, friendId : idType, queueId : idType, includePublic : Boolean) : Future[GraphVertex[Queue]]
    def getPublicQueue(id : idType, userId : idType) : Future[GraphVertex[Queue]]
    def create(userId : idType, queue : Queue) : Future[GraphVertex[Queue]]
    def update(userId : idType, queueId : idType, queue : Queue) : Future[GraphVertex[Queue]]
    def delete(userId : idType, queueId : idType) : Future[GraphVertex[Queue]]

    // components
    def getUser(queue : GraphVertex[Queue]) : Future[GraphVertex[User]]
    def getSplashImages(queue : GraphVertex[Queue]) : Future[Seq[GraphVertex[ContentImage]]]
    def favoriteCount(queue : GraphVertex[Queue]) : Future[Long]
    def likeCount(queue : GraphVertex[Queue]) : Future[Long]


    def create(user : GraphVertex[User], queue : Queue) : Future[GraphVertex[Queue]]
    def createDefault(queue : Queue) : Future[GraphVertex[Queue]]
    def getAllQueues(userId : idType) : Future[Seq[GraphVertex[Queue]]]
    def getPrivateQueues(userId : idType) : Future[Seq[GraphVertex[Queue]]]
    def getFriendQueues(userId : idType, friendId : idType, includePublic : Boolean = true) : Future[Seq[GraphVertex[Queue]]]
    def getPublicQueues(userId : idType) : Future[Seq[GraphVertex[Queue]]]
  }
}

trait QueueBasicsPersistenceModuleImpl extends QueueBasicsPersistenceModule {
  this: GraphAPIModule =>
  val queueBasicsPersistence = new Object with QueueBasicsPersistenceImpl

  trait QueueBasicsPersistenceImpl extends QueueBasicsPersistence {
    import objects._
    private val createdQueueLabel = implicitly[EdgeHelper[CreatedQueueEdge]].label
    private val friendsWithLabel = implicitly[EdgeHelper[FriendsWithEdge]].label
    private val nextQueueItemLabel = implicitly[EdgeHelper[NextQueueItemEdge]].label

    def get(id : idType, userId : idType) : Future[GraphVertex[Queue]] = {
      Logger.info(s"Getting queue with id $id and userId $userId")

      graph.select[Queue](userId)(_.out(createdQueueLabel).has("id",id))
        .recoverWith {
        case e : ObjectNotFoundException =>
          Future.failed(new UnauthorizedException("Queue does not exist or you do not have permission to access it"))
      }
    }
    def getFriendQueue(userId : idType, friendId : idType, queueId : idType, includePublic : Boolean) : Future[GraphVertex[Queue]] = {

      val public = new GremlinScalaPipeline[Vertex,Vertex]
        .has("Queue:access","public")
      val friend = new GremlinScalaPipeline[Vertex,Vertex]
        .has("Queue:access","friends")
        .in(createdQueueLabel) // get the user that created the queue
        .has("id",friendId) // get the user that created the queue
        .in(friendsWithLabel) // get the user's friends
        .has("id",userId) // filter on only the user making the request

      graph.select[Queue](queueId)(_.as("queue").or(public,friend))
    }
    def getPublicQueue(id : idType, userId : idType) : Future[GraphVertex[Queue]] = {
      get(id, userId).map { queue =>
        if (queue.obj.access.equals("public")) queue
        else throw new UnauthorizedException("You do not have permission to access this resource")
      }
    }
    def create(userId : idType, queue : Queue) : Future[GraphVertex[Queue]] = {
      for {
        user <- graph.get[User](userId)
        queueV <- graph.create(queue) // create the queue
        edge <-  graph.createSegment(user,CreatedQueueEdge(),queueV,Direction.OUT) // add edge (User)->[created_queue]->(Queue)
      } yield queueV
    }
    def createDefault(queue : Queue) : Future[GraphVertex[Queue]] = {
      for {
        user <- graph.getByKey[User]("User:username","surf")
        queue <- create(user.id,queue)
        edge <- graph.createSegmentUnique(user,DefaultQueueEdge(),queue)
      } yield queue
    }
    def update(userId : idType, queueId : idType, queue : Queue) : Future[GraphVertex[Queue]] = {
      get(queueId,userId).flatMap { queueVertex =>
        graph.update(queueVertex.copy(obj = queue))
      }
    }
    def delete(userId : idType, queueId : idType) : Future[GraphVertex[Queue]] = {
      graph.get[Queue](queueId)
        .flatMap{ queue =>
        graph.delete(queue).map(x => queue)
      }
    }


    // components
    def getUser(queue : GraphVertex[Queue]) : Future[GraphVertex[User]] = {
      graph.getEdges[CreatedQueueEdge,User](queue.id,Direction.IN).map { users =>
        if(users.size == 0) throw new Exception("no users")
        else users.head.vertex
      }
    }

    def getSplashImages(queue : GraphVertex[Queue]) : Future[Seq[GraphVertex[ContentImage]]] = {

      graph.queryV[ContentImage](queue.id){ pipe =>
        pipe.has("id", queue.id).as("x")
          .out(nextQueueItemLabel)
          .loop("x",
        {whileLoop => whileLoop.getObject.getEdges(Direction.OUT,nextQueueItemLabel).size != 0} , // TODO should there be paging?
        emit => true
        )
          .out(implicitly[EdgeHelper[QueueItemContentEdge]].label)
          .out(implicitly[EdgeHelper[BackdropsEdge]].label)
      }
    }

    def favoriteCount(queue : GraphVertex[Queue]) : Future[Long] = {
      // TODO this should return a Stream and reduce that instead of list.size - may take up too much memory
      graph.genericQueryV(queue.id){ pipe =>
        pipe.in(implicitly[EdgeHelper[UserFavoriteQueueEdge]].label)
      }.map(_.size)
    }
    def likeCount(queue : GraphVertex[Queue]) : Future[Long] = {
      graph.genericQueryV(queue.id){ pipe =>
        pipe.in(implicitly[EdgeHelper[UserLikesQueueEdge]].label)
      }.map(_.size)
    }

    def create(user : GraphVertex[User], queue : Queue) : Future[GraphVertex[Queue]] = {
      if(!Seq("private","public","friends").contains(queue.access)) throw new InvalidStateException(s"${queue.access} is not valid for queue.access - must be either private, public, or friends")
      for {
        queueV <- graph.create(queue) // create the queue
        edge <-  graph.createSegment(user,CreatedQueueEdge(),queueV,Direction.OUT) // add edge (User)->[created_queue]->(Queue)
      } yield queueV
    }

    def getQueues(userId : idType, access : Seq[String]) : Future[Seq[GraphVertex[Queue]]] = {
      val javaList : java.util.List[String] = access
      val pipe = new GremlinScalaPipeline[Vertex,Vertex]

      graph.queryV[Queue](userId) { pipe =>
        pipe
          .has("type","User")
          .as("user")
          .out(implicitly[EdgeHelper[CreatedQueueEdge]].label)
          .has("Queue:access",T.in,javaList)
      }
    }
    def getPrivateQueues(userId : idType) : Future[Seq[GraphVertex[Queue]]] = {
      getQueues(userId,Seq("private"))
    }
    def getAllQueues(userId : idType) : Future[Seq[GraphVertex[Queue]]] = {
      getQueues(userId,Seq("private","friends","public"))
    }
    def getFriendQueues(userId : idType, friendId : idType, includePublic : Boolean = true) : Future[Seq[GraphVertex[Queue]]] = {
      //val access : java.util.List[String] = if (includePublic) Seq("friends","public") else Seq("friends")

      val public = new GremlinScalaPipeline[Vertex,Vertex]
        .has("Queue:access","public") // filter only public queues

      val friend = new GremlinScalaPipeline[Vertex,Vertex]
        .has("Queue:access","friends") // filter on only friend queues
        .in(createdQueueLabel) // go back to the creator
        .in(friendsWithLabel) // get the user's friends
        .has("id",friendId) // filter on only the user making the request

      val orQuery = if(includePublic) Seq(public,friend) else Seq(friend)

      graph.queryV[Queue](userId) { pipe =>
        pipe.has("id",userId) // get the user that created the queue
          .out(createdQueueLabel) // hop to the queue
          .as("queue")
          .or(orQuery: _*)
      }

    }
    def getPublicQueues(userId : idType) : Future[Seq[GraphVertex[Queue]]] = {
      getQueues(userId,Seq("public"))
    }
  }
}