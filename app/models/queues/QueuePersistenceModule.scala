package models.queues

import com.surf.graph.GraphAPIModule
import play.api.Logger

/**
 * Module to create and retrieve queues
 */
trait QueuePersistenceModule
  extends QueueBasicsPersistenceModule
  with QueueEdgePersistenceModule
  with QueueItemsPersistenceModule
{
  this: GraphAPIModule =>
  val queuePersistence : QueuePersistence

  trait QueuePersistence
    extends QueueBasicsPersistence
    with QueueEdgePersistence
    with QueueItemsPersistence
}

trait QueuePersistenceModuleImpl
  extends QueuePersistenceModule
  with QueueBasicsPersistenceModuleImpl
  with QueueEdgePersistenceModuleImpl
  with QueueItemsPersistenceModuleImpl
{
  this: GraphAPIModule =>

  val queuePersistence = new QueuePersistenceImpl

  class QueuePersistenceImpl
    extends QueuePersistence
    with QueueBasicsPersistenceImpl
    with QueueEdgePersistenceImpl
    with QueueItemsPersistenceImpl
}