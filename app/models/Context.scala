package models

import java.io.FileNotFoundException

import com.surf.graph._
import com.surf.graph.json.GraphJsonWrites
import com.surf.graph.titan.{TitanGraphIds, TitanInMemoryGraphModule, TitanCassandraGraphModule}
import models.mail.{DevMailgunMailServiceModuleImpl, MailgunMailServiceModuleImpl, MailServiceModule}
import play.api.{Play, Logger, Mode}
import models.content._
import models.user._
import models.auth._
import models.search._
import models.content.movies._
import models.queues._
import schema.Schema

/**
 * Sets up dependency injection using concrete implementations using Cake Pattern
 */
trait DynamicRuntimeContext
{
  val context = play.api.Play.current.mode match {
    case Mode.Dev => DevelopmentRuntimeContext
    case Mode.Prod => ProductionRuntimeContext
    case _ => DevelopmentRuntimeContext
  }
}

trait GraphAPIModuleWithJson extends GraphAPIModule with GraphJsonWrites

/*
  This is a complete list of modules needed for the entire application
 */
trait RuntimeContext
  extends GraphAPIModuleWithJson
  with AuthObjects
  with AuthServiceModule
  with AuthJsonModule
  with ConstantsModule
  with ContentJsonModule
  with ContentPersistenceModule
  with ContentServiceModule
  with MailServiceModule
  with MoviesJsonModule
  with MoviesServiceModule
  with MoviesPersistenceModule
  with QueueServiceModule
  with QueuePersistenceModule
  with QueueJsonModule
  with SearchObjects
  with SearchServiceModule
  with SearchPersistenceModule
  with SearchJsonModule
  with TokenModule
  with TokenHelperModule
  with UserJsonModule
  with UserPersistenceModule
  with UserServiceModule

/*
  This contains default implementations for each module
 */
trait AbstractRuntimeContext extends RuntimeContext
  with AuthServiceModuleImpl
  with AuthJsonModuleImpl
  with ContentJsonModuleImpl
  with ContentPersistenceModuleImpl
  with ContentServiceModuleImpl
  with GraphModuleImpl
  with GraphJsonWrites
  with MoviesJsonModuleImpl
  with MoviesServiceModuleImpl
  with MoviesPersistenceModuleImpl
  with QueueServiceModuleImpl
  with QueuePersistenceModuleImpl
  with QueueJsonModuleImpl
  with SearchServiceModuleImpl
  with SearchPersistenceModuleImpl
  with SearchJsonModuleImpl
  with TokenModuleImpl
  with UserJsonModuleImpl
  with UserPersistenceModuleImpl
  with UserServiceModuleImpl {
    this : RawGraph =>
}


// this contains the base for Prod + Dev contexts (and shouldn't be in Test)
trait DefaultRuntimeContext extends AbstractRuntimeContext
  with ProdTokenHelperModuleImpl
  with TitanGraphIds
{
  this : RawGraph  =>
}
/*
  Production context
 */
object ProductionRuntimeContext
  extends DefaultRuntimeContext
  with ProductionConstantsModuleImpl
  with MailgunMailServiceModuleImpl
  with TitanCassandraGraphModule
{
  Logger.info("Production context: Using TitanCassandraGraphModule")
  // TODO these should be set properly (and likely need to be lazy or else it will throw an NPE)
  //override val graphQueryExecutionContext = Akka.system.dispatchers.lookup("graph-context")
  //override val standardExecutionContext = play.api.libs.concurrent.Execution.Implicits.defaultContext

  implicit def writesVertexId = play.api.libs.json.Writes.LongWrites
  implicit def writesEdgeId = play.api.libs.json.Writes.StringWrites
  implicit def readsVertexId = play.api.libs.json.Reads.LongReads
  implicit def readsEdgeId = play.api.libs.json.Reads.StringReads

  val storageHostname = scala.util.Properties.envOrElse("TITAN_HOSTNAME", "10.240.68.81")
  val indexHostname = scala.util.Properties.envOrElse("TITAN_INDEXHOST", "10.240.68.81")

}
/*
  Development context
 */
object DevelopmentRuntimeContext
  extends DefaultRuntimeContext
  with DevelopmentConstantsModuleImpl
  with DevMailgunMailServiceModuleImpl
  with TitanInMemoryGraphModule
{
  Logger.info("Development context: Using In Memory graph")
  //override val graphQueryExecutionContext = Akka.system.dispatchers.lookup("graph-context")
  //override val standardExecutionContext = play.api.libs.concurrent.Execution.Implicits.defaultContext
  Schema.load(titanGraph)


  import play.api.Play.current

  val graphFileLocation = "resources/dev_graph/development.json"
  val is = Play.classloader.getResourceAsStream(graphFileLocation)
  if(is == null) throw new FileNotFoundException("cannot load resource "+ graphFileLocation)
  graph.loadJson(is)

  implicit def writesVertexId = play.api.libs.json.Writes.LongWrites
  implicit def writesEdgeId = play.api.libs.json.Writes.StringWrites
  implicit def readsVertexId = play.api.libs.json.Reads.LongReads
  implicit def readsEdgeId = play.api.libs.json.Reads.StringReads

}
