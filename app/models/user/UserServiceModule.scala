package models.user

import com.surf.graph.GraphAPIModule
import com.tinkerpop.blueprints.Direction
import models.GraphAPIModuleWithJson

import org.mindrot.jbcrypt.BCrypt
import models.auth._
import play.api.libs.json.Json
import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits.defaultContext

/**
 * UserServiceModule
 */
trait UserServiceModule {
  this: UserPersistenceModule with GraphAPIModuleWithJson =>

  val userService : UserService
  case class Relationship(id : Option[objects.edgeIdType], status : String)
  object Relationship {
    implicit val writes = Json.writes[Relationship]
  }

  trait UserService {
    import objects._
    def create(user : User) : Future[GraphVertex[User]]
    def update(requestor : idType, userId : idType, user : User)  : Future[GraphVertex[User]]
    def getUser(id : idType) : Future[GraphVertex[User]]
    def getUserByEmail(email : String) : Future[GraphVertex[User]]
    def getUserByUsername(username : String) : Future[GraphVertex[User]]
    def setPassword(id : idType, password : String) : Future[GraphVertex[User]]
    def getFriends(requestorOpt : Option[idType], userId : idType) : Future[Seq[GraphVertex[User]]]
    def getPendingFriendships(requestorOpt : Option[idType], userId : idType) : Future[Seq[EdgeTuple[PendingFriendEdge,User]]]
    def getFriendsRequested(requestorOpt : Option[idType], userId : idType) : Future[Seq[EdgeTuple[PendingFriendEdge,User]]]
    def getFriendsRequesting(requestorOpt : Option[idType], userId : idType) : Future[Seq[EdgeTuple[PendingFriendEdge,User]]]
    def getRelationship(requestorOpt : Option[idType], userId : idType) : Future[Relationship]
    def addFriend(auth : idType, userId : idType, friendId : idType) : Future[Segment[User,PendingFriendEdge,User]]
    def confirmFriend(auth : idType, friendshipID : edgeIdType) : Future[Relationship]
    def removeFriend(auth : idType, userId : idType, friendshipID : edgeIdType) : Future[Relationship]
  }
}
/**
 * UserServiceModule
 * implementation
 */
trait UserServiceModuleImpl extends UserServiceModule {
  this: UserPersistenceModule with TokenModule with GraphAPIModuleWithJson =>

  val userService = new UserServiceImpl
  class UserServiceImpl extends UserService {
    import objects._

    def createSeedUser(password : String) : Future[GraphVertex[User]] = {
      val encryptedPassword = BCrypt.hashpw(password, BCrypt.gensalt())
      val user = User(username = "admin",email = "contact@surfbeta.com",password = encryptedPassword,access = GodMode,firstName = "The",lastName = "Dude", displayNameType = DisplayNameType.firstLastname, displayName = "The Dude")
      userPersistence.createSeedUser(user)
    }

    def create(user : User) = {
      val encryptedPassword = BCrypt.hashpw(user.password, BCrypt.gensalt())
      val encrUser = user.copy(password = encryptedPassword, username = user.username.toLowerCase, email = user.email.toLowerCase)
      userPersistence.create(encrUser)
    }
    def update(requestor : idType, userId : idType, user : User)  : Future[GraphVertex[User]] = {
      if(requestor != userId) throw new AuthorizationException("operation not permitted")
      userPersistence.update(userId,user)
    }
    def getUser(id : idType) = {
      userPersistence.getUser(id)
    }
    def getUserByEmail(email : String) = {
      userPersistence.getUserByEmail(email.toLowerCase)
    }
    def getUserByUsername(username : String) = {
      userPersistence.getUserByUsername(username.toLowerCase)
    }

    def setPassword(id : idType, password : String) = {
      userPersistence.setPassword(id, password)
    }
    def getFriends(requestorOpt : Option[idType], userId : idType) : Future[Seq[GraphVertex[User]]] = {
      userPersistence.getFriends(requestorOpt,userId)
    }
    def getPendingFriendships(requestorOpt : Option[idType], userId : idType) : Future[Seq[EdgeTuple[PendingFriendEdge,User]]] = {
      userPersistence.getPendingFriends(requestorOpt,userId,Direction.BOTH)
    }
    def getFriendsRequested(requestorOpt : Option[idType], userId : idType) : Future[Seq[EdgeTuple[PendingFriendEdge,User]]] = {
      userPersistence.getPendingFriends(requestorOpt,userId,Direction.OUT)
    }
    def getFriendsRequesting(requestorOpt : Option[idType], userId : idType) : Future[Seq[EdgeTuple[PendingFriendEdge,User]]] = {
      userPersistence.getPendingFriends(requestorOpt,userId,Direction.IN)
    }
    def getRelationship(requestorOpt : Option[idType], userId : idType) = {
      userPersistence.getRelationship(requestorOpt,userId).map(x => Relationship(x._1,x._2))
    }
    def addFriend(requestor : idType, userId : idType, friendId : idType) : Future[Segment[User,PendingFriendEdge,User]] = {
      if(!requestor.equals(userId)) throw new AuthorizationException("operation not permitted")
      userPersistence.addFriend(userId,friendId)
    }
    def confirmFriend(requestor : idType, friendshipID : edgeIdType) : Future[Relationship] = {
      //if(!requestor.equals(userId)) throw new AuthorizationException("operation not permitted")
      // TODO make sure this is authorized
      userPersistence.confirmFriend(friendshipID).map(x => Relationship(x._1,x._2))
    }
    def removeFriend(requestor : idType, userId : idType, friendshipID : edgeIdType) : Future[Relationship] = {
      if(!requestor.equals(userId)) throw new AuthorizationException("operation not permitted")
      userPersistence.removeFriend(userId,friendshipID).map(x => Relationship(x._1,x._2))
    }
  }

}