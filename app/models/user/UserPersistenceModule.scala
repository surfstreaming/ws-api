package models.user

import com.surf.graph._
import com.tinkerpop.blueprints.{Edge, Direction, Vertex}
import com.tinkerpop.gremlin.scala.GremlinScalaPipeline
import play.api.Logger
import scala.concurrent.Future
import models.queues.{QueueServiceModule, Queue}
import java.util.Date
import play.api.libs.concurrent.Execution.Implicits.defaultContext

/**
 * UserPersistenceModule
 */
trait UserPersistenceModule {
  this: GraphAPIModule =>
  val userPersistence: UserPersistence
  type RelationshipTuple = (Option[objects.edgeIdType], String)

  trait UserPersistence {
    import objects._

    def createSeedUser(user : User) : Future[GraphVertex[User]]
    def create(user : User) : Future[GraphVertex[User]]
    def update(userId : idType, user : User) : Future[GraphVertex[User]]
    def getUser(id : idType) : Future[GraphVertex[User]]
    def getUserByEmail(email : String) : Future[GraphVertex[User]]
    def getUserByUsername(username : String) : Future[GraphVertex[User]]
    def setPassword(id : idType, password : String) :Future[GraphVertex[User]]
    def getFriends(requestorOpt : Option[idType], userId : idType) : Future[Seq[GraphVertex[User]]]
    def getPendingFriends(requestorOpt : Option[idType], userId : idType, direction : Direction) : Future[Seq[EdgeTuple[PendingFriendEdge,User]]]
    def getRelationship(requestorOpt : Option[idType], userId : idType) : Future[RelationshipTuple]
    def addFriend(userId : idType, friendId : idType) : Future[Segment[User,PendingFriendEdge,User]]
    def confirmFriend(friendshipEdgeId : edgeIdType) : Future[RelationshipTuple]
    def removeFriend(userId : idType, friendshipID : edgeIdType) : Future[RelationshipTuple]
  }
}
/**
 * UserPersistenceModule
 * implementation
 */
trait UserPersistenceModuleImpl extends UserPersistenceModule {
  this: GraphAPIModule with QueueServiceModule =>
  val userPersistence = new UserPersistenceImpl

  class UserPersistenceImpl extends UserPersistence {
    import objects._
    val friendsWithLabel = implicitly[EdgeHelper[FriendsWithEdge]].label

    def createSeedUser(user : User) : Future[GraphVertex[User]] = {
      graph.create(user)
    }

    def create(user : User) : Future[GraphVertex[User]] = {
      val ts = new Date().getTime.toString
      val defaultQueue = Queue(code = s"${user.username}-$ts", name = "My Queue", access = "private")
      Logger.info(s"Creating user: ${user.toString}")
      for {
        userV <- graph.create(user) // create the user
        queue <- queueService.create(userV.id, userV.id, defaultQueue) // create the default queue
      } yield userV
    }

    def update(userId : idType, user : User) = {
      graph.get[User](userId).flatMap { userVertex =>
        graph.update(userVertex.copy(obj = user))
      }
    }
    def getUser(id : idType) = {
      graph.get[User](id)
    }
    def getUserByEmail(email : String) = {
      graph.getByKey[User]("User:email",email)
    }
    def getUserByUsername(username : String) = {
      graph.getByKey[User]("User:username",username)
    }
    def getLists(id : idType) = {
      //graph.v(id).out("created-list").map(x => ScalaVertex.wrap(x)).toList()
      throw new Exception("not implemented")
    }

    def setPassword(id : idType, password : String) = {
      getUser(id).flatMap { user =>
        graph.updateProperty(user,"User:password",password)
      }
    }
    def getFriends(requestorOpt : Option[idType], userId : idType) : Future[Seq[GraphVertex[User]]] = {
      requestorOpt.map { requestor =>
        // if auth user is the user requesting friends
        if(requestor.equals(userId)) {
          graph.getEdges[FriendsWithEdge,User](userId,Direction.OUT).map(_.map(_.vertex))
        } else {
          // only return friends if they are themselves friends
          graph.queryV[User](requestor)(
            _.out(friendsWithLabel)
            .has("id",userId)
            .out(friendsWithLabel))
        }
      }.getOrElse(Future.successful(Seq())) // if user is Anonymous, don't return friends
    }

    def getPendingFriends(requestorOpt : Option[idType], userId : idType, direction : Direction) : Future[Seq[EdgeTuple[PendingFriendEdge,User]]] = {
      graph.getEdges[PendingFriendEdge,User](userId,direction)
    }

    def getRelationship(requestorOpt : Option[idType], userId : idType) : Future[RelationshipTuple] = {
      requestorOpt.map { requestor =>
        if(requestor.equals(userId)) Future.successful(( None, "Current User"))
        else {
          val pendingLabel = implicitly[EdgeHelper[PendingFriendEdge]].label
          val friendLabel = implicitly[EdgeHelper[FriendsWithEdge]].label

          val inEdges = new GremlinScalaPipeline[Vertex,Edge]
            .inE(pendingLabel,friendLabel).as("edges")
            .outV.has("id",userId)
            .back("edges")

          val outEdges = new GremlinScalaPipeline[Vertex,Edge]
            .outE(pendingLabel,friendLabel).as("edges")
            .inV.has("id",userId)
            .back("edges")


          graph.genericQueryV(requestor){ pipe =>
            pipe.copySplit(inEdges,outEdges)
              .fairMerge
              .asInstanceOf[GremlinScalaPipeline[Vertex,Edge]]
              .map { edge =>
              val label = edge.getLabel
              val id = edge.getId.asInstanceOf[edgeIdType]
              val o = edge.getVertex(Direction.OUT)
              //
              if(o.getId.asInstanceOf[idType].equals(requestor) && label.equals(pendingLabel))
                ( Some(id), "Pending Request" )
              else if(o.getId.asInstanceOf[idType].equals(userId) && label.equals(pendingLabel))
                ( Some(id),  "Awaiting Confirmation")
              else if(label.equals(friendLabel))
                ( Some(id),  "Friends")
              else ( Some(id),  "Unknown")
            }
          }.map{ results =>
            if(results.size == 0 ) ( None, "No Relationship")
            else results.head
          }
        }
      }.getOrElse(Future.successful( ( None, "Not Authorized")))
    }

    def addFriend(userId : idType, friendId : idType) : Future[Segment[User,PendingFriendEdge,User]] = {
      for {
        user <- graph.get[User](userId)
        friend <- graph.get[User](friendId)
        edge <- graph.createSegment(user,PendingFriendEdge(),friend)
      } yield edge
    }
    def confirmFriend(friendshipEdgeId : edgeIdType) : Future[RelationshipTuple] = {
      for {
        friendship <- graph.getSegment[User,SimpleEdge,User](friendshipEdgeId)
        friendA <- graph.createSegment(friendship.v1,FriendsWithEdge(),friendship.v2)
        friendB <- graph.createSegment(friendship.v2,FriendsWithEdge(),friendship.v1)
        removeEdge <- graph.delete(friendship.edge)
      } yield (Some(friendA.edge.id),"Friends")
    }
    def removeFriend(userId : idType, friendshipID : edgeIdType) : Future[RelationshipTuple] = {

      for {
        edge <- graph.getEdge[PendingFriendEdge](friendshipID)
        result <- graph.delete(edge)
      } yield (None,  "No Relationship")

    }

  }
}