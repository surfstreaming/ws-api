package models.user

import com.surf.graph.{SimpleEdgeHelper, DefaultVertexHelper}
import models.auth.Access
import models.user.DisplayNameType.DisplayNameType
import models.user.AvatarType.AvatarType
import play.api.libs.json._

import scala.util.Try


case class User(
   username : String,
   email: String,
   password: String,
   access: Access,
   firstName : String,
   lastName : String,
   displayNameType : DisplayNameType = DisplayNameType.firstL,
   displayName : String,
   avatarType : AvatarType = AvatarType.defaults,
   avatarId : String = "/assets/images/avatars/default1.png",
   onboarded : Boolean = false
 )
object User {

  implicit val writes = Json.writes[User]
  implicit val reads = Json.reads[User]
  implicit object UserVertexHelper extends DefaultVertexHelper[User]{
    val objectType = "User"
    val uniqueFields = List("User:email","User:username")
    def toObject(props : Map[String,Any]) : User = {
      User(
        username = props.get("User:username").get.asInstanceOf[String],
        email = props.get("User:email").get.asInstanceOf[String],
        password = props.get("User:password").get.asInstanceOf[String],
        access = Access.valueOf(props.get("User:access").get.asInstanceOf[String]),
        firstName = props.get("User:firstName").get.asInstanceOf[String],
        lastName = props.get("User:lastName").get.asInstanceOf[String],
        displayNameType = DisplayNameType.valueOf(props.get("User:displayNameType").get.asInstanceOf[String]),
        displayName = props.get("User:displayName").get.asInstanceOf[String],
        avatarType = AvatarType.valueOf(props.get("User:avatarType").get.asInstanceOf[String]),
        avatarId = props.get("User:avatarId").get.asInstanceOf[String],
        onboarded = props.getOrElse("User:onboarded",false).asInstanceOf[Boolean]
      )
    }
    def toMap(obj : User) : Map[String,Any] = {
      Map(
        "User:username" -> obj.username,
        "User:email" -> obj.email,
        "User:password" -> obj.password,
        "User:access" -> obj.access.toString,
        "User:firstName" -> obj.firstName,
        "User:lastName" -> obj.lastName,
        "User:displayNameType" -> obj.displayNameType.toString,
        "User:displayName" -> obj.displayName,
        "User:avatarType" -> obj.avatarType.toString,
        "User:avatarId" -> obj.avatarId,
        "User:onboarded" -> obj.onboarded
      )
    }
  }
}
object DisplayNameType extends Enumeration {
  type DisplayNameType = Value

  val firstLastname, fLastname, firstname, firstL, username = Value

  def valueOf(name : String) : DisplayNameType = {
    DisplayNameType.withName(name)
  }
  implicit val writes : Writes[DisplayNameType] = new Writes[DisplayNameType]{
    def writes(value: DisplayNameType): JsValue = JsString(value.toString)
  }
  implicit val reads : Reads[DisplayNameType] = new Reads[DisplayNameType]{
    def reads(json: JsValue) : JsResult[DisplayNameType] = {
      Try(valueOf(json.as[String]))
        .map(JsSuccess(_))
        .getOrElse(JsError("could not read access"))
    }
  }
}
object AvatarType extends Enumeration {
  type AvatarType = Value
  val twitter, facebook, instagram, gravatar, defaults = Value
  def valueOf(name : String) : AvatarType = {
    AvatarType.withName(name)
  }

  implicit val writes : Writes[AvatarType] = new Writes[AvatarType]{
    def writes(value: AvatarType): JsValue = JsString(value.toString)
  }
  implicit val reads : Reads[AvatarType] = new Reads[AvatarType]{
    def reads(json: JsValue) : JsResult[AvatarType] = {
      Try(valueOf(json.as[String]))
        .map(JsSuccess(_))
        .getOrElse(JsError("could not read access"))
    }
  }
}

case class PendingFriendEdge()
object PendingFriendEdge {
  implicit val writes : Writes[PendingFriendEdge] = new Writes[PendingFriendEdge]{
    def writes(value: PendingFriendEdge): JsValue = JsString("pending_friendship")
  }
  implicit object Helper extends SimpleEdgeHelper[PendingFriendEdge] {
    val label = "pending_friendship"
    def toObj(props : Map[String,Any]) = PendingFriendEdge()
  }
}
case class FriendsWithEdge()
object FriendsWithEdge {
  implicit val writes : Writes[FriendsWithEdge] = new Writes[FriendsWithEdge]{
    def writes(value: FriendsWithEdge): JsValue = JsString("friend")
  }
  implicit object Helper extends SimpleEdgeHelper[FriendsWithEdge] {
    val label = "friends_with"
    def toObj(props : Map[String,Any]) = FriendsWithEdge()
  }
}