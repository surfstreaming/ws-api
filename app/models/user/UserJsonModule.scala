package models.user

import com.surf.graph.GraphAPIModule
import com.surf.graph.json.GraphJsonWrites
import play.api.libs.json._
import models.JsonHelper
import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import models.queues.QueueServiceModule
import play.api.libs.json.JsSuccess
import play.api.libs.json.JsObject

/**
 * UserJsonModule
 */
trait UserJsonModule {
  this: GraphAPIModule =>

  val userJson : UserJson

  trait UserJson {
    import objects._
    def getUserById(requestorOpt : Option[idType], userId : idType, components : Seq[String]) : Future[JsValue]
    def getUserByUsername(requestorOpt : Option[idType], username : String, components : Seq[String]) : Future[JsValue]
    def getQueues(requestorOpt : Option[idType], userId : idType, components : Seq[String]) : Future[JsValue]
    def getFriends(requestorOpt : Option[idType], userId : idType, components : Seq[String]) : Future[JsValue]
    def getRelationship(requestorOpt : Option[idType], userId : idType) : Future[JsValue]
    def addFriend(requestor : idType, userId : idType, request : JsValue) : Future[JsValue]
    def confirmFriend(requestor : idType, friendshipID : edgeIdType) : Future[JsValue]
    def removeFriend(requestor : idType, userId : idType, friendshipID : edgeIdType) : Future[JsValue]
    def updateUser(requestor : idType, userId : idType, json : JsValue) : Future[JsValue]
  }
}
trait UserJsonModuleImpl extends UserJsonModule {
  this: UserServiceModule with QueueServiceModule with GraphJsonWrites with GraphAPIModule =>

  val userJson = new UserJsonModuleImpl

  class UserJsonModuleImpl extends UserJson with JsonHelper{
    import objects._

    def getUserById(requestorOpt : Option[idType], userId : idType, components : Seq[String]) : Future[JsValue] = {
      userService.getUser(userId)
        .flatMap(getComponents(requestorOpt,_,components))
    }
    def getUserByUsername(requestorOpt : Option[idType], username : String, components : Seq[String]) : Future[JsValue] = {
      userService.getUserByUsername(username)
        .flatMap(getComponents(requestorOpt,_,components))
    }
    def getComponents(requestorOpt : Option[idType], user: GraphVertex[User], components :  Seq[String]) : Future[JsValue] = {
      val futures = components.map { component =>
        getComponent(requestorOpt,user,component).map(jsresult => component -> jsresult)
      }

      Future.sequence(futures).map { components =>
        val mainJson = Json.toJson(user)
        val componentsJson = Json.obj("components" -> Json.toJson(components.toMap[String, JsValue]))


        val trans = __.json.update(
          __.read[JsObject].map {
            o => o ++ componentsJson
          }
        )
        mainJson.transform(trans) match {
          case JsSuccess(e, _) => e
          case JsError(_) => throw new Exception("could not transform Json")
        }
      }
    }

    def getComponent(requestorOpt : Option[idType], user: GraphVertex[User], component : String) : Future[JsValue] = component match {
      case "friends" => getFriends(requestorOpt,user.id,Seq())
      case "queues" => getQueues(requestorOpt,user.id,Seq())
      case "relationship" => getRelationship(requestorOpt,user.id)
      case "pending_friends" => getPendingFriends(requestorOpt,user.id)
      case "friends_requested" => getFriendsRequested(requestorOpt,user.id)
      case "friends_requesting" => getFriendsRequesting(requestorOpt,user.id)
    }

    def getQueues(requestorOpt : Option[idType], userId : idType, components : Seq[String]) : Future[JsValue] = {
      queueService.getQueues(requestorOpt,userId).map(Json.toJson(_))
    }

    def getFriends(requestorOpt : Option[idType], userId : idType, components : Seq[String]) : Future[JsValue] = {
      userService.getFriends(requestorOpt,userId).map(Json.toJson(_))
    }
    def getPendingFriends(requestorOpt : Option[idType], userId : idType) : Future[JsValue] = {
      userService.getPendingFriendships(requestorOpt,userId).map(Json.toJson(_))
    }
    def getFriendsRequested(requestorOpt : Option[idType], userId : idType) : Future[JsValue] = {
      userService.getFriendsRequested(requestorOpt,userId).map(Json.toJson(_))
    }
    def getFriendsRequesting(requestorOpt : Option[idType], userId : idType) : Future[JsValue] = {
      userService.getFriendsRequesting(requestorOpt,userId).map(Json.toJson(_))
    }
    def getRelationship(requestorOpt : Option[idType], userId : idType) : Future[JsValue] = {
      userService.getRelationship(requestorOpt,userId).map(x => Json.toJson(relationshipJson(x)))
    }
    def addFriend(requestor : idType, userId : idType, json : JsValue) : Future[JsValue] = WithFutureJson[idType](json \ "friendUserId"){ friendUserId =>
      userService.addFriend(requestor,userId,friendUserId).map(Json.toJson(_))
    }
    def confirmFriend(requestor : idType, friendshipID : edgeIdType) : Future[JsValue] = {
      userService.confirmFriend(requestor,friendshipID).map(Json.toJson(_))
    }
    def removeFriend(requestor : idType, userId : idType, friendshipID : edgeIdType) : Future[JsValue] = {
      userService.removeFriend(requestor,userId,friendshipID).map(Json.toJson(_))
    }

    def updateUser(requestor : idType, userId : idType, json : JsValue) : Future[JsValue] = WithFutureJson[User](json){ req =>
      userService.update(requestor,userId,req).map(Json.toJson(_))
    }

    def relationshipJson(rel : Relationship) : JsValue = Json.obj("id"-> rel.id, "status" -> rel.status)
  }
}