package models

/**
 * A trait that contains constants used throughout the app
 */
trait ConstantsModule {
  val constants : Constants

  trait Constants {
    val adminInterfaceURL : String
    val searchServiceURL : String
    val contentServiceURL : String
    val webUI : String
    val tmdbAPIKey : String
    val jwtIssuer : String
    val mailgunURL : String
    val mailgunKey : String
    val mailFrom = "Dan at Surf <contact@surfbeta.com>"
    val mailBcc : Option[String]
  }
}

trait ProductionConstantsModuleImpl extends ConstantsModule {
  val constants = ProductionApplicationConstants

  object ProductionApplicationConstants extends Constants {
    val adminInterfaceURL = "http://admin.surfbeta.com/#"
    val searchServiceURL = scala.util.Properties.envOrElse("SEARCH_SERVICE","http://localhost:9001")
    val contentServiceURL = scala.util.Properties.envOrElse("CONTENT_SERVICE","http://localhost:9002")
    val webUI = "http://surfbeta.com"
    val tmdbAPIKey = "93581830f1a1c1d3a861b853bf9f51cc"
    val jwtIssuer = "http://api.surfbeta.com"
    val mailgunURL = "surfbeta.com"
    val mailgunKey = "key-952f4ba210bf4d1ae652ce2fa511655b"
    val mailBcc = Some("dan@surfbeta.com") // change to None to turn off
    val rexsterHost = "172.17.42.1"
    val rexsterPort = "10000"
    val rexsterGraph = "graph"
  }
}

trait DevelopmentConstantsModuleImpl extends ConstantsModule {
  val constants = DevelopmentConstants

  object DevelopmentConstants extends Constants {
    val adminInterfaceURL = "http://localhost:5500"
    val searchServiceURL = "http://localhost:9001"
    val contentServiceURL = "http://localhost:9002"
    val webUI = "http://localhost:5000"
    val tmdbAPIKey = "93581830f1a1c1d3a861b853bf9f51cc"
    val jwtIssuer = "http://api.surfbeta.com"
    val mailgunURL = "sandbox21b60f0f0a594abea548c72ffe642a8d.mailgun.org"
    val mailgunKey = "key-952f4ba210bf4d1ae652ce2fa511655b"
    val mailBcc = None
    val graphFileLocation = "conf/resources/dev_graph/"

    val emailOverride = "dan@surfbeta.com" // any emails that are sent through mailgun will be sent here instead
  }
}

