package models.search

import com.surf.graph.GraphAPIModule
import models.ConstantsModule
import scala.concurrent.Future

import play.api.libs.concurrent.Execution.Implicits._
/**
 * Module that facilitates searching for content
 */
trait SearchServiceModule {
  this: GraphAPIModule with SearchObjects =>
  val searchService : SearchService

  trait SearchService {
    import searchObjects._
    val searchObjectsDefault : Set[Searchable] = Set(SearchableTV, SearchableMovie)
    def search(input : Seq[SearchFilterInput], objects : Set[Searchable] = searchObjectsDefault) : Future[Seq[SearchResult]]
  }
}

trait SearchServiceModuleImpl extends SearchServiceModule  {
  this: GraphAPIModule with ConstantsModule with SearchPersistenceModuleImpl with SearchObjects =>
  val searchService = new SearchServiceImpl

  class SearchServiceImpl extends SearchService {
    import searchObjects._

    def search(input : Seq[SearchFilterInput], objects : Set[Searchable]) : Future[Seq[SearchResult]] =  {
      searchPersistence.search(input,objects) // execute the search and map to objects
    }
  }
}