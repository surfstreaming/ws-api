package models.search

import com.surf.graph.{GraphAPIModule, SimpleVertex}
import com.tinkerpop.gremlin.scala.GremlinScalaPipeline
import com.tinkerpop.blueprints.Vertex
import play.api.Logger
import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits.defaultContext

trait SearchPersistenceModule {
  this: GraphAPIModule with SearchObjects =>
  val searchPersistence : SearchPersistence
  trait SearchPersistence {
    import searchObjects._
    //def search(filters : Seq[GremlinScalaPipeline[Vertex,Vertex]]) : Future[Seq[SearchResult]]
    def search(queries : Seq[SearchFilterInput], objects : Set[Searchable]) : Future[Seq[SearchResult]]
  }
}

trait SearchPersistenceModuleImpl extends SearchPersistenceModule {
  this: GraphAPIModule with SearchObjects =>
  val searchPersistence = new SearchPersistenceImpl
  class SearchPersistenceImpl extends SearchPersistence {
    import searchObjects._
    /*
      Search function will create a pipe for each Searchable type (Movie,TV, etc)
      After the pipes are complete they will be merged together and mapped to SearchResults
     */
    def search(queries : Seq[SearchFilterInput], objects : Set[Searchable]) : Future[Seq[SearchResult]] = {
      val filters = objects.toSeq.map { searchable =>
        Logger.error(s"Starting query for type ${searchable.vertexType}")
        graph.queryV[SimpleVertex]("type",searchable.vertexType){ pipe =>
          val filters = searchable.factory(queries)
          filters(pipe)
        }
      }

      val future = Future.sequence(filters).map { resultSets => resultSets.flatMap( resultSet => resultSet) }

      future.map { results => // execute this block when the future completes
          results.map { result =>
            val vertexId = result.id // String
            val props = result.obj.props // Map[String,Any]
            val objType = props.get("type").get.toString // TODO this is unsafe

            Searchable.valueOf(objType) // retrieve the correct Searchable object based on the vertex object type
              .process(vertexId,props) // convert id,props to a SearchResult
          }
      }
    }
  }
}
