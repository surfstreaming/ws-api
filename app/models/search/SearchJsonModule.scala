package models.search

import play.api.libs.json._
import play.api.libs.functional.syntax._
import models.{GraphAPIModuleWithJson, JsonHelper}
import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits._

/**
 * Module that facilitates searching for content
 */
trait SearchJsonModule {
  val searchJson : SearchJson

  trait SearchJson {
    def autocomplete(query : String) : Future[JsValue]
    def search(query : String) : Future[JsValue]
    def search(filters : JsValue) : Future[JsValue]
  }
}

trait SearchJsonModuleImpl extends SearchJsonModule with JsonHelper {
  this: SearchServiceModule with GraphAPIModuleWithJson with SearchObjects =>
  val searchJson = new SearchJsonImpl
  import searchObjects._

  implicit lazy val jsonSearchResult : Format[SearchResult] = (
      (JsPath \ "id").format[objects.idType] and
      (JsPath \ "objType").format[String] and
      (JsPath \ "name").format[String] and
      (JsPath \ "popularity").formatNullable[Double] and
      (JsPath \ "thumbnail").formatNullable[String]
    )(SearchResult.apply, unlift(SearchResult.unapply))

  class SearchJsonImpl extends SearchJson {
    def autocomplete(query : String) = {
      search(query)
    }
    def search(query : String) = {
      val filters = Seq(SearchFilterInput("name",Some(query)))
      searchService.search(filters).map(result => Json.toJson(result))
    }

    def search(json : JsValue) = WithFutureJson[Seq[SearchFilterInput]](json){ filters =>
      searchService.search(filters).map(result => Json.toJson(result))
    }
  }
}