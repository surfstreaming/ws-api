package models.search
/*
import com.tinkerpop.gremlin.Tokens.T
import com.tinkerpop.gremlin.scala.GremlinScalaPipeline
import com.tinkerpop.blueprints.Vertex
import scala.collection.JavaConversions._
import com.thinkaurelius.titan.core.attribute.Text.CONTAINS


trait Searchable {
  def factory(filter : SearchFilterInput) : SearchFilter = filter match {
    case SearchFilterInput("type", Some(query), None) => SearchFilterTypes(Seq(query))
    case SearchFilterInput("type", None, Some(query)) => SearchFilterTypes(query)
    case SearchFilterInput("name", Some(query), None) => SearchFilterName(query)
    case _ => throw new Exception("unrecognized search input: "+filter.toString)
  }
}


trait SearchFilter extends Ordered[SearchFilter] {
  type pipeFunction = GremlinScalaPipeline[Vertex,Vertex] => GremlinScalaPipeline[Vertex,Vertex]
  val order : Int
  def compare(that : SearchFilter) = {
    if(this.order < that.order) -1
    else if(this.order > that.order) 1
    else 0
  }

  def filter : pipeFunction

}

case class SearchFilterTypes(types : Seq[String]) extends SearchFilter {
  val order = 0

  def filter = { pipe : GremlinScalaPipeline[Vertex,Vertex] =>
    val javaList : java.util.List[String] = types // blueprints wants a java Collection not a scala List
    pipe.has("type",T.in,javaList)
  }
}
case class SearchFilterName(query : String) extends SearchFilter {
  val order = Int.MaxValue

  def filter = { pipe : GremlinScalaPipeline[Vertex,Vertex] =>
    pipe.has("name",CONTAINS,query)
  }
}

*/