package models.search

import com.surf.graph.GraphAPIModule
import play.api.Logger
import play.api.libs.json._
import com.tinkerpop.gremlin.scala.GremlinScalaPipeline
import com.tinkerpop.blueprints.Vertex
import com.thinkaurelius.titan.core.attribute.Text.CONTAINS_REGEX

case class SearchFilterInput (
 id: String,
 value: Option[String] = None,
 values: Option[Seq[String]] = None
)

object SearchFilterInput {
  implicit val writes = Json.writes[SearchFilterInput]
  implicit val reads = Json.reads[SearchFilterInput]
}

trait SearchObjects {
  this: GraphAPIModule =>
  import objects.idType
  object searchObjects {
    type pipeFunction = GremlinScalaPipeline[Vertex,Vertex] => GremlinScalaPipeline[Vertex,Vertex]
    sealed trait Searchable {
      val vertexType : String
      val searchField : String
      def process(id : idType, props : Map[String,Any]) : SearchResult
      def factory(filters : Seq[SearchFilterInput]) : pipeFunction = {
        filters
          .map(factory) // map the SearchFilterInput to a Pipe
          // TODO we should sort the filters by a ranking function
          .reduce( (acc,cur) => acc.compose(cur)) // add the current Pipe to the accumulator Pipe
      }
      def factory(filter : SearchFilterInput) : pipeFunction = filter match {
        case SearchFilterInput("NoOp", _, _) => filterNoOp
        case SearchFilterInput("name", Some(query), None) => filterByName(query)
        case _ => throw new Exception("unrecognized search input: "+filter.toString)
      }

      def filterNoOp = { pipe : GremlinScalaPipeline[Vertex,Vertex] => pipe }
      def filterByName(name : String) = { pipe : GremlinScalaPipeline[Vertex,Vertex] =>
        pipe.has(searchField,CONTAINS_REGEX,".*"+name+".*")
      }
    }
    object Searchable {
      def valueOf(input : String) = input match {
        case "Movie" => SearchableMovie
        case "TVSeries" => SearchableTV
        case "Queue" => SearchableQueue
        case "User" => SearchableUser
        case "Person" => SearchablePerson
        case "Company" => SearchableCompany
      }
    }
    case object SearchableMovie extends Searchable {
      val vertexType = "Movie"
      val searchField = "Movie:title"
      def process(id : idType, props : Map[String,Any]) : SearchResult = {
        val name = props.get("Movie:title").get.toString
        val popularity = None
        val thumbnail = props.get("Movie:posterPath").map(_.toString)
        SearchResult( id, vertexType, name, popularity, thumbnail )
      }
    }
    case object SearchableTV extends Searchable {
      val vertexType = "TVSeries"
      val searchField = "TVSeries:title"
      def process(id : idType, props : Map[String,Any]) : SearchResult = {
        val name = props.get("TVSeries:title").get.toString
        val popularity = None
        val thumbnail = props.get("TVSeries:posterPath").map(_.toString)
        SearchResult( id, vertexType, name, popularity, thumbnail )
      }
    }
    case object SearchableQueue extends Searchable {
      val vertexType = "Queue"
      val searchField = "Queue.name"

      def process(id : idType, props : Map[String,Any]) : SearchResult = {
        val name = props.get("Queue:name").get.toString
        val popularity = None
        val thumbnail = props.get("Queue:posterPath").map(_.toString)
        SearchResult( id, vertexType, name, popularity, thumbnail )
      }
    }
    case object SearchableUser extends Searchable {
      val vertexType = "User"
      val searchField = "User:username"
      def process(id : idType, props : Map[String,Any]) : SearchResult = {
        val name = props.get("User:username").get.toString
        val popularity = None
        val thumbnail = props.get("User:thumbnail").map(_.toString)
        SearchResult( id, vertexType, name, popularity, thumbnail )
      }
    }
    case object SearchablePerson extends Searchable {
      val vertexType = "Person"
      val searchField = "Person:name"
      def process(id : idType, props : Map[String,Any]) : SearchResult = {
        val name = props.get("Person:name").get.toString
        val popularity = None
        val thumbnail = props.get("Person:posterPath").map(_.toString)
        SearchResult( id, vertexType, name, popularity, thumbnail )
      }
    }
    case object SearchableCompany extends Searchable {
      val vertexType = "Company"
      val searchField = "Company:name"
      def process(id : idType, props : Map[String,Any]) : SearchResult = {
        val name = props.get("Company:name").get.toString
        val popularity = None
        val thumbnail = props.get("Company:posterPath").map(_.toString)
        SearchResult( id, vertexType, name, popularity, thumbnail )
      }
    }

    case class SearchResult(
     id: idType,
     objType : String,
     name : String,
     popularity : Option[Double],
     thumbnail : Option[String]
     )
  }

}

case class TmdbSearchResult(
   id: Int,
   title: String,
   popularity : Option[Double],
   thumbnail : Option[String]
)
/*
object TmdbSearchResult {
  implicit val writes = Json.writes[TmdbSearchResult]
  implicit val reads = Json.reads[TmdbSearchResult]
}
*/

case class TmdbSearchContentResult(
  backdrop_path : String,
  id : Int,
  original_name : Int,
  first_air_date : Option[String],
  poster_path : String,
  popularity : Double,
  name : String,
  vote_average : Double,
  vote_count : Int
)

object TmdbSearchContentResult {
  implicit val writes = Json.writes[TmdbSearchContentResult]
  implicit val reads = Json.reads[TmdbSearchContentResult]
}


case class TmdbSearchResults(page: Int, results : Seq[TmdbSearchContentResult], total_Pages : Int, totalResults: Int)
object TmdbSearchResults {
  implicit val writes = Json.writes[TmdbSearchResults]
  implicit val reads = Json.reads[TmdbSearchResults]
}


