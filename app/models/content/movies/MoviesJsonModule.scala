package models.content.movies

import com.surf.core.objects.content.ContentEdgeObjects._
import com.surf.core.objects.content.movies._
import com.surf.graph.GraphAPIModule
import models.GraphAPIModuleWithJson
import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json._
import models.queues.{QueueItem, Queue}
import models.user.User

/**
 * Module to handle converting movies to/from JSON
 */
trait MoviesJsonModule {
  this: GraphAPIModule =>

  val moviesJson : MoviesJson

  trait MoviesJson {
    import objects.idType
    def get(auth : Option[idType], id : idType, components : Seq[String]) : Future[JsValue]
  }
}

trait MoviesJsonModuleImpl extends MoviesJsonModule {
  this : MoviesServiceModule with GraphAPIModuleWithJson =>
  val moviesJson = new MoviesJsonImpl

  class MoviesJsonImpl extends MoviesJson {
    import objects._
    def get(requestorOpt : Option[idType], id : idType, components : Seq[String]) : Future[JsValue] = {
      moviesService.get(requestorOpt,id,components)
      .flatMap { result =>
        val futures = components.map { component =>
          getComponent(requestorOpt,result,component).map(jsresult => component -> jsresult)
        }

        Future.sequence(futures).map{ components =>
          val moviesJson = Json.toJson(result)
          val componentsJson = Json.obj("components" -> Json.toJson(components.toMap[String,JsValue]))


          val trans = __.json.update(
            __.read[JsObject].map{ o => o ++ componentsJson }
          )

          moviesJson.transform(trans) match {
            case JsSuccess(e,_) => e
            case JsError(_) => throw new Exception("could not transform Json")
          }
        }
      }
    }

    def getComponent(authOpt : Option[idType], movie : GraphVertex[Movie], component : String) : Future[JsValue] = component match {
      case "genres" => moviesService.getComponent[GenreEdge,Genre](authOpt,movie).map(Json.toJson(_))
      case "cast" => moviesService.getComponent[CastEdge,Cast](authOpt,movie).map(Json.toJson(_))
      case "guest_stars" => moviesService.getComponent[GuestStarsEdge,Cast](authOpt,movie).map(Json.toJson(_))
      case "posters" => moviesService.getComponent[PostersEdge,ContentImage](authOpt,movie).map(Json.toJson(_))
      case "backdrops" => moviesService.getComponent[BackdropsEdge,ContentImage](authOpt,movie).map(Json.toJson(_))
      case "keywords" => moviesService.getComponent[KeywordsEdge,Keyword](authOpt,movie).map(Json.toJson(_))
      case "releases" => moviesService.getComponent[ReleasesEdge,Release](authOpt,movie).map(Json.toJson(_))
      case "alternative_titles" => moviesService.getComponent[AltTitleEdge,AlternativeTitle](authOpt,movie).map(Json.toJson(_))
      case "videos" => moviesService.getComponent[VideosEdge,ExternalVideo](authOpt,movie).map(Json.toJson(_))
      case "translations" => moviesService.getComponent[TranslationsEdge,Language](authOpt,movie).map(Json.toJson(_))
      case "collection" => moviesService.getComponent[CollectionEdge,Collection](authOpt,movie).map(Json.toJson(_))
      case "crew" => moviesService.getComponent[CrewEdge,Crew](authOpt,movie).map(Json.toJson(_))
      case "public_queues" => moviesService.getPublicQueues(authOpt,movie,includeRequestorQueues = false).map(writeQueueJson)
      case "private_queues" => moviesService.getQueuesCreatedByUser(authOpt,movie).map(writeQueueItemJson)
      case "friend_queues" => moviesService.getFriendQueues(authOpt,movie).map(writeQueueJson)
    }
    def writeQueueJson(queues : Seq[Segment[Queue,_,User]]) : JsValue = {
      Json.toJson(queues.map { queue =>
        Json.obj("queue"->Json.toJson(queue.v1),"user"-> Json.toJson(queue.v2))
      })
    }
    def writeQueueItemJson(queues : Seq[Segment[QueueItem,_,Queue]]) : JsValue = {
      Json.toJson(queues.map { queue =>
        Json.obj("item"->Json.toJson(queue.v1),"queue"->Json.toJson(queue.v2))
      })
    }
  }
}



