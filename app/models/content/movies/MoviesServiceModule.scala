package models.content.movies

import com.surf.core.objects.content.movies.Movie
import com.surf.graph._
import models.UnauthorizedException
import scala.concurrent.Future
import models.queues.{ItemInQueueEdge, QueueItem, CreatedQueueEdge, Queue}
import models.user.User
import play.api.libs.concurrent.Execution.Implicits.defaultContext

/**
 * Module to handle getting movies from the graph
 */
trait MoviesServiceModule {
  this: GraphAPIModule =>

  val moviesService : MovieService

  trait MovieService {
    import objects._
    def get(requestorOpt : Option[idType], id : idType, components : Seq[String]) : Future[GraphVertex[Movie]]
    def getComponent[E : EdgeHelper, T : VertexHelper](authOpt : Option[idType], movie : GraphVertex[Movie]) : Future[Seq[GraphVertex[T]]]
    def getPublicQueues(requestorOpt : Option[idType], movie : GraphVertex[Movie], includeRequestorQueues : Boolean) : Future[Seq[Segment[Queue,CreatedQueueEdge,User]]]
    def getQueuesCreatedByUser(requestorOpt : Option[idType],movie : GraphVertex[Movie]) : Future[Seq[Segment[QueueItem,ItemInQueueEdge,Queue]]]
    def getFriendQueues(requestorOpt : Option[idType], movie : GraphVertex[Movie]) : Future[Seq[Segment[Queue,CreatedQueueEdge,User]]]
  }
}

trait MoviesServiceModuleImpl extends MoviesServiceModule {
  this: MoviesPersistenceModule with GraphAPIModule =>
  val moviesService = new MovieServiceImpl

  class MovieServiceImpl extends MovieService {
    import objects._
    def get(requestorOpt : Option[idType], id : idType, components : Seq[String]) : Future[GraphVertex[Movie]] = {
      moviesPersistence.get(id,components)
    }
    def getPublicQueues(requestorOpt : Option[idType], movie : GraphVertex[Movie], includeRequestorQueues : Boolean) : Future[Seq[Segment[Queue,CreatedQueueEdge,User]]] = {
      moviesPersistence.getPublicQueues(requestorOpt,movie,includeRequestorQueues)
    }
    def getQueuesCreatedByUser(requestorOpt : Option[idType],movie : GraphVertex[Movie]) : Future[Seq[Segment[QueueItem,ItemInQueueEdge,Queue]]] = {
      requestorOpt.map ( requestor =>
        moviesPersistence.getQueuesCreatedByUser(requestor, movie)
      ).getOrElse(throw new UnauthorizedException("unauthorized request by anonymous user"))
    }
    def getFriendQueues(requestorOpt : Option[idType], movie : GraphVertex[Movie]) : Future[Seq[Segment[Queue,CreatedQueueEdge,User]]] = {
      requestorOpt.map ( requestor =>
        moviesPersistence.getFriendQueues(requestor, movie)
      ).getOrElse(throw new UnauthorizedException("unauthorized request by anonymous user"))
    }
    def getComponent[E : EdgeHelper, T : VertexHelper](requestorOpt : Option[idType], movie : GraphVertex[Movie]) : Future[Seq[GraphVertex[T]]] =  {
      moviesPersistence.getComponent[E,T](movie).map(_.map(_.vertex))
    }
  }
}