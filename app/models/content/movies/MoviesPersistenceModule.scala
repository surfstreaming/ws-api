package models.content.movies

import com.surf.core.objects.content.movies.Movie
import com.surf.graph._
import scala.concurrent.Future
import models.user.{FriendsWithEdge, User}
import models.queues._
import com.tinkerpop.gremlin.scala.GremlinScalaPipeline
import com.tinkerpop.blueprints.{Direction, Vertex}
import com.tinkerpop.gremlin.Tokens.T
import scala.collection.JavaConversions._


/**
 *
 */
trait MoviesPersistenceModule {
  this: GraphAPIModule =>
  val moviesPersistence : MoviesPersistence

  trait MoviesPersistence {
    import objects._
    def get(id : idType, components : Seq[String]) : Future[GraphVertex[Movie]]
    def getComponent[E : EdgeHelper, T : VertexHelper](movie : GraphVertex[Movie], direction : Direction = Direction.OUT) : Future[Seq[EdgeTuple[E,T]]]
    // TODO this should return a different type of user that doesn't show password
    def getPublicQueues(requestorOpt : Option[idType], movie : GraphVertex[Movie], includeRequestorQueues : Boolean) : Future[Seq[Segment[Queue,CreatedQueueEdge,User]]]
    def getQueuesCreatedByUser(userId : idType, movie : GraphVertex[Movie]) : Future[Seq[Segment[QueueItem,ItemInQueueEdge,Queue]]]
    def getFriendQueues(userId : idType, movie : GraphVertex[Movie]) : Future[Seq[Segment[Queue,CreatedQueueEdge,User]]]
  }
}

trait MoviesPersistenceModuleImpl extends MoviesPersistenceModule {
  this: GraphAPIModule =>
  val moviesPersistence = new MoviesPersistenceImpl

  class MoviesPersistenceImpl extends MoviesPersistence {
    import objects._
    val createdQueueLabel = implicitly[EdgeHelper[CreatedQueueEdge]].label
    val friendsWithLabel = implicitly[EdgeHelper[FriendsWithEdge]].label

    def get(id : idType, components : Seq[String]) : Future[GraphVertex[Movie]] = {
      graph.get[Movie](id)
    }
    def getComponent[E : EdgeHelper, T : VertexHelper](movie : GraphVertex[Movie], direction : Direction) : Future[Seq[EdgeTuple[E,T]]] = {
      graph.getEdges[E,T](movie.id,direction)
    }
    def getPublicQueues(requestorOpt : Option[idType], movie : GraphVertex[Movie], includeRequestorQueues : Boolean) : Future[Seq[Segment[Queue,CreatedQueueEdge,User]]] = {

      val query = new GremlinScalaPipeline[Vertex,Vertex]
        .has("id",movie.id) // start at the content

      //val query = queuesWithContentPipe(movie.id).has("Queue:access","public")
      graph.querySegments[Queue,CreatedQueueEdge,User](Direction.IN,movie.id){ pipe =>
        pipe.in(implicitly[EdgeHelper[QueueItemContentEdge]].label) // go to the queue item
          .out(implicitly[EdgeHelper[ItemInQueueEdge]].label) // shortcut to the queue
          .has("Queue:access","public") // only include public queues

        if(requestorOpt.nonEmpty && !includeRequestorQueues){
          val userId = requestorOpt.get
          val filter = new GremlinScalaPipeline[Vertex,Vertex] // starts at the queue vertex
            .in(implicitly[EdgeHelper[CreatedQueueEdge]].label) // go to the user
            .has("id",T.neq,userId)
          pipe.or(filter)
        }
        pipe
      }
    }
    def getQueuesCreatedByUser(userId : idType, movie : GraphVertex[Movie]) : Future[Seq[Segment[QueueItem,ItemInQueueEdge,Queue]]] = {
      val filter = new GremlinScalaPipeline[Vertex,Vertex] // this starts with the queue item
        .out(implicitly[EdgeHelper[ItemInQueueEdge]].label) // go out to the queue
        .in(implicitly[EdgeHelper[CreatedQueueEdge]].label) // go to the user
        .has("id",userId)

      graph.querySegments[QueueItem,ItemInQueueEdge,Queue](Direction.OUT,movie.id){ pipe =>
        pipe.in(implicitly[EdgeHelper[QueueItemContentEdge]].label)
          .or(filter)
      }
    }
    def getFriendQueues(userId : idType, movie : GraphVertex[Movie]) : Future[Seq[Segment[Queue,CreatedQueueEdge,User]]] = {
      val access : java.util.List[String] = Seq("public","friends")

      graph.querySegments[Queue,CreatedQueueEdge,User](Direction.IN,movie.id){ pipe =>
        queuesWithContentPipe(pipe)
          .has("Queue:access",T.in,access)
          .as("queues")
          .in(createdQueueLabel)
          .out(friendsWithLabel)
          .has("id",userId)
          .back("queues")
          .map(_.asInstanceOf[Vertex])
      }
    }

    def queuesWithContentPipe(pipe : GremlinScalaPipeline[Vertex,Vertex]) : GremlinScalaPipeline[Vertex,Vertex] = {
      // pass in a pipe containing movie vertices
      pipe
        .in("queue_item_content")
        .as("x")
        .in("next_queue_item")
        .loop(
          "x",
          whileFun => whileFun.getObject.getEdges(Direction.IN,"next_queue_item").iterator().hasNext, // loop until we reach the start of the queue (which is the queue object)
          emit => ! emit.getObject.getEdges(Direction.IN,"next_queue_item").iterator().hasNext // only emit if its the queue object
        )
    }
  }
}