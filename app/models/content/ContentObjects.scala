package models.content

import play.api.libs.json.{Json, JsValue, Writes}
import com.surf.graph.{VertexHelper, DefaultVertexHelper}
import java.util.Date

case class Thumbnail(height: Int, width: Int, url : String, descr : String)
object Thumbnail {
  implicit val writes : Writes[Thumbnail] = Json.writes[Thumbnail]
}
sealed trait Content
object Content{
  implicit def writes : Writes[Content] = new Writes[Content]{
    def writes(value: Content): JsValue = value match {
      case o : BasicVideo => BasicVideo.writesJson.writes(o)
    }
  }
}
case class BasicVideo(
  accountId : Option[String],
  identifier : String,
  title : String,
  description  : String,
  longDescription  : String,
  itemState : String,
  startDate : Option[Date],
  endDate : Option[Date],
  tags : Array[String],
  length : Long,
  geoFiltered : Option[Boolean],
  geoFilteredCountries : Option[Array[String]],
  geoFilterExclude : Option[Boolean],
  videoStillURL : Option[String],
  thumbnailURL : Option[String],
  lastUpdated : Date
) extends Content

object  BasicVideo {
  val writesJson : Writes[BasicVideo] = Json.writes[BasicVideo]
  implicit object BasicVideoHelper extends DefaultVertexHelper[BasicVideo] {
    val objectType = "Content"
    val uniqueFields = Seq()

    def toMap(obj : BasicVideo) : Map[String,Any] = throw new Exception("convert this class to use GraphVertex not GraphObjects")
    def toObject(props : Map[String,Any]) : BasicVideo = throw new Exception("not implemented")

  }
}
