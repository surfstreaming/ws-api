package models.content

import com.surf.core.objects.content.ContentEdgeObjects.FeaturedContentEdge
import com.surf.core.objects.content.movies.Movie
import com.surf.graph.GraphAPIModule
import models.user.User

import scala.concurrent.Future

trait ContentServiceModule {
  this : ContentPersistenceModule with GraphAPIModule =>
  val contentService : ContentService

  trait ContentService {
    import objects._

    def listFeatured(requestorOpt : Option[idType]) : Future[Seq[EdgeTuple[FeaturedContentEdge,Movie]]]
    def addFeatured(userId : idType, contentId : idType) : Future[Segment[Movie,FeaturedContentEdge,User]]
    def updateFeatured(featuredId : edgeIdType, userId : idType, contentId : idType) : Future[String]
    def deleteFeatured(featuredId : edgeIdType) : Future[String]
  }
}

trait ContentServiceModuleImpl extends ContentServiceModule{
  this : ContentPersistenceModule with GraphAPIModule =>
  val contentService = new ContentServiceImpl

  class ContentServiceImpl extends ContentService {
    import objects._

    def listFeatured(requestorOpt : Option[idType]) : Future[Seq[EdgeTuple[FeaturedContentEdge,Movie]]] = {
      requestorOpt.map(contentPersistence.listFeatured(_)) // if authenticated, then get featured for the user
        .getOrElse(contentPersistence.listFeatured) // if anonymous then get generic featured content
    }
    def addFeatured(userId : idType, contentId : idType) : Future[Segment[Movie,FeaturedContentEdge,User]] = {
      contentPersistence.addFeatured(userId,contentId)
    }
    def updateFeatured(featuredId : edgeIdType, userId : idType, contentId : idType) : Future[String] = {
      contentPersistence.updateFeatured(featuredId,userId,contentId)
    }
    def deleteFeatured(featuredId : edgeIdType) : Future[String] = {
      contentPersistence.deleteFeatured(featuredId)
    }
  }
}