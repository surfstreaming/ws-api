package models.content

import com.surf.graph._
import com.tinkerpop.blueprints.{Vertex, Direction}


trait ContentPersistenceModule
  extends FeaturedPersistenceModule
{
  this: GraphAPIModule =>
  val contentPersistence : ContentPersistence

  trait ContentPersistence extends FeaturedPersistence {

  }
}
trait ContentPersistenceModuleImpl
  extends ContentPersistenceModule
  with FeaturedPersistenceModuleImpl
{
  this : GraphAPIModule =>
  val contentPersistence = new Object with ContentPersistenceImpl

  trait ContentPersistenceImpl extends ContentPersistence with FeaturedPersistenceImpl{

  }
}