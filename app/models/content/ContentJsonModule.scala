package models.content

import com.surf.graph.GraphAPIModule
import play.api.libs.json.{Json, JsValue}
import models.{GraphAPIModuleWithJson, JsonHelper}
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import scala.concurrent.Future

/**
 * Module to convert Content objects to<->from JSON
 */
trait ContentJsonModule {
  this: GraphAPIModule =>
  val contentJson : ContentJson

  trait ContentJson {
    import objects._
    def listFeatured(requestorOpt : Option[idType]) : Future[JsValue]
    def addFeatured(json : JsValue) : Future[JsValue]
    def updateFeatured(featuredId : edgeIdType, request : JsValue) : Future[JsValue]
    def deleteFeatured(featuredId : edgeIdType) : Future[JsValue]
  }
}
trait ContentJsonModuleImpl extends ContentJsonModule {
  this: ContentServiceModule with GraphAPIModuleWithJson  =>

  val contentJson = new ContentJsonImpl

  case class FeaturedRequest(userId : objects.idType, contentId : objects.idType)
  object FeaturedRequest {
    implicit val reads = Json.reads[FeaturedRequest]
  }

  class ContentJsonImpl extends ContentJson with JsonHelper  {
    import objects._

    def listFeatured(requestorOpt : Option[idType]) = {
      contentService.listFeatured(requestorOpt).map(Json.toJson(_))
    }
    def addFeatured(json : JsValue) : Future[JsValue] = WithFutureJson[FeaturedRequest](json){ req =>
      contentService.addFeatured(req.userId,req.contentId).map(Json.toJson(_))
    }
    def updateFeatured(featuredId : edgeIdType, json : JsValue) : Future[JsValue] = WithFutureJson[FeaturedRequest](json){ req =>
      contentService.updateFeatured(featuredId,req.userId,req.contentId).map(Json.toJson(_))
    }
    def deleteFeatured(featuredId : edgeIdType) : Future[JsValue] = {
      contentService.deleteFeatured(featuredId).map(Json.toJson(_))
    }
  }
}