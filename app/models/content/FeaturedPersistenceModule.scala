package models.content

import com.surf.core.objects.content.ContentEdgeObjects.FeaturedContentEdge
import com.surf.core.objects.content.movies.Movie
import com.tinkerpop.blueprints.{Direction, Vertex}
import com.surf.graph._
import models.user.User

import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits.defaultContext

/**
 * Module for featured content CRUD
 */
trait FeaturedPersistenceModule {
  this: GraphAPIModule =>

  trait FeaturedPersistence {
    import objects._
    def listFeatured : Future[Seq[EdgeTuple[FeaturedContentEdge,Movie]]]
    def listFeatured(requestor : idType) : Future[Seq[EdgeTuple[FeaturedContentEdge,Movie]]]
    def addFeatured(userId : idType, contentId : idType) : Future[Segment[Movie,FeaturedContentEdge,User]]
    def updateFeatured(featuredId : edgeIdType, userId : idType, contentId : idType) : Future[String]
    def deleteFeatured(featuredId : edgeIdType) : Future[String]
  }
}
trait FeaturedPersistenceModuleImpl extends FeaturedPersistenceModule {
  this: GraphAPIModule =>

  trait FeaturedPersistenceImpl extends FeaturedPersistence {
    import objects._
    def listFeatured : Future[Seq[EdgeTuple[FeaturedContentEdge,Movie]]] = {

      graph.getByKey[User]("User:username","surf")
        .flatMap(user => graph.getEdges[FeaturedContentEdge,Movie](user.id,Direction.IN))
        .recover {
        case e : ObjectNotFoundException => Seq()
      }
    }
    def listFeatured(requestor : idType) : Future[Seq[EdgeTuple[FeaturedContentEdge,Movie]]] = {
      listFeatured
    }
    def addFeatured(userId : idType, contentId : idType) : Future[Segment[Movie,FeaturedContentEdge,User]] = {
      val userFuture = graph.get[User](userId)
      val contentFuture = graph.get[Movie](contentId)

      for {
        user <- userFuture
        content <- contentFuture
        edge <- graph.createSegmentUnique(content,FeaturedContentEdge(),user)
      } yield edge
    }
    def updateFeatured(featuredId : edgeIdType, userId : idType, contentId : idType) : Future[String] = {
      throw new UnsupportedOperationException
    }
    def deleteFeatured(featuredId : edgeIdType) : Future[String] = {
      graph.getEdge[FeaturedContentEdge](featuredId).flatMap(graph.delete(_))
    }
  }
}