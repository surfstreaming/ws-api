package models

import play.api.Logger
import play.api.libs.json._
import scala.util.{Failure, Success, Try}
import scala.util.Failure
import scala.util.Success
import play.api.libs.json._
import play.api.libs.functional.syntax._
import play.api.data.validation.ValidationError
import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits._

/**
 * Json Helpers
 */



case class CrudRequest(action : String, objType : String, objClass : String, json : JsValue)
object CrudRequest {
  //implicit val writes : Writes[JsonRequest] = Json.writes[JsonRequest]
  implicit val reads : Reads[CrudRequest] = (
      (JsPath \ "action").read[String] and
      (JsPath \ "type").read[String] and
      (JsPath \ "class").read[String] and
      (JsPath \ "object").read[JsValue]
    )(CrudRequest.apply _)
}
trait JsonHelper {
  implicit def writesTry[E](implicit objWrites: Writes[E]) = new Writes[Try[E]]{
    def writes(value: Try[E]): JsValue = value match {
      case Success(_) => Json.toJson(value.get)
      case Failure(e : ApiException) => Json.toJson(e)
      case Failure(e) => Json.obj("error"->e.getMessage)
    }
  }
  implicit def writesJsResult[E](implicit objWrites: Writes[E]) = new Writes[JsResult[E]]{
    def writes(value: JsResult[E]): JsValue = value match {
      case JsSuccess(j,_) => Json.toJson(j)
      case e : JsError => Json.toJson(new JsonParseException("error parsing json",errToJson(e.errors)))
    }
  }
  def WithJson[A](json : JsValue)(f: A => JsValue)(implicit readsA : Reads[A]) : JsValue = {
    json.validate[A] match {
      case JsSuccess(j,_) => f(j)
      case e : JsError => Json.toJson(new JsonParseException("error parsing json",errToJson(e.errors)))
    }
  }
  def WithFutureJson[A](json : JsValue)(f: A => Future[JsValue])(implicit readsA : Reads[A]) : Future[JsValue] = {
    json.validate[A] match {
      case JsSuccess(j,_) => f(j)
      case e : JsError => Future.failed(new JsonParseException("error parsing json",errToJson(e.errors)))
    }
  }

  def insertObject(base : JsValue, addition : JsObject) : JsValue = {
    val trans = __.json.update(
      __.read[JsObject].map{ o => o ++ addition }
    )
    base.transform(trans) match {
      case JsSuccess(e,_) => e
      case JsError(_) => throw new Exception("could not transform Json")
    }
  }

  /** Helper that maps JsError errors to a JSON object */
  def errToJson(errors: Seq[(JsPath, Seq[ValidationError])]): JsValue = {
    val jsonErrors: Seq[(String, JsValue)] = errors map {
      case (path, errs) => path.toJsonString -> Json.toJson(errs.map(_.message))
    }
    JsObject(jsonErrors)
  }
}
