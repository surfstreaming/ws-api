package models.tmdb

import com.surf.core.objects.content.movies._
import play.api.libs.json.{JsPath, Reads, Json}
import play.api.libs.functional.syntax._
/**
 * Objects used for API requests
 * API DOCUMENTATION: http://docs.themoviedb.apiary.io/
 */

case class TmdbCollectionPart (
  backdrop_path : Option[String],
  id : Int,
  poster_path : Option[String],
  release_date : String,
  title : String
)
object TmdbCollectionPart {
  implicit val reads = Json.reads[TmdbCollectionPart]
}


case class TmdbMovieAlternativeTitles(
   titles: Seq[AlternativeTitle]
 )
object TmdbMovieAlternativeTitles {
  implicit val reads = Json.reads[TmdbMovieAlternativeTitles]
}


case class TmdbCredits(
  cast : Seq[Cast],
  crew : Seq[Crew],
  guest_stars : Option[Seq[Cast]]
)
object TmdbCredits {
  implicit val reads = Json.reads[TmdbCredits]
}


case class TmdbImages (
  backdrops : Option[Seq[ContentImage]],
  posters : Option[Seq[ContentImage]]
)
object TmdbImages {
  implicit val reads = Json.reads[TmdbImages]
}


case class TmdbKeywords (
  keywords : Seq[Keyword]
)
object TmdbKeywords {
  implicit val reads = Json.reads[TmdbKeywords]
}


case class TmdbReleases (
  countries: Seq[Release]
)
object TmdbReleases {
  implicit val reads = Json.reads[TmdbReleases]
}


case class TmdbVideos(
 results : Seq[ExternalVideo]
)
object TmdbVideos {
  implicit val reads = Json.reads[TmdbVideos]
}


case class TmdbTranslations(
 translations : Seq[Language]
)
object TmdbTranslations {
  implicit val reads = Json.reads[TmdbTranslations]
}


case class TmdbMovieExtras (
  genres : Seq[Genre],
  production_companies : Seq[Company],
  production_countries: Seq[Country],
  spoken_languages : Seq[Language],
  credits : TmdbCredits,
  images : TmdbImages,
  keywords : TmdbKeywords,
  releases : TmdbReleases,
  alternative_titles : TmdbMovieAlternativeTitles,
  videos : TmdbVideos,
  translations : TmdbTranslations,
  imdb_id : String,
  belongs_to_collection : Option[Collection]
 )
object TmdbMovieExtras {
  implicit val reads = Json.reads[TmdbMovieExtras]
}
case class MovieLinks (
   genres : Seq[Genre],
   productionCompanies : Seq[Company],
   productionCountries: Seq[Country],
   spokenLanguages : Seq[Language],
   cast : Seq[Cast],
   crew : Seq[Crew],
   guestStars : Option[Seq[Cast]],
   posters : Option[Seq[ContentImage]],
   backdrops : Option[Seq[ContentImage]],
   keywords : Seq[Keyword],
   releases : Seq[Release],
   alternativeTitles : Seq[AlternativeTitle],
   videos : Seq[ExternalVideo],
   translations : Seq[Language],
   imdb_id : String,
   collection : Option[Collection]
 )
object MovieLinks {
  implicit val reads : Reads[MovieLinks] = (
    (JsPath \ "genres").read[Seq[Genre]] and
    (JsPath \ "production_companies").read[Seq[Company]] and
    (JsPath \ "production_countries").read[Seq[Country]] and
    (JsPath \ "spoken_languages").read[Seq[Language]] and
    (JsPath \ "credits" \ "cast").read[Seq[Cast]] and
    (JsPath \ "credits" \ "crew").read[Seq[Crew]] and
    (JsPath \ "credits" \ "guest_stars").readNullable[Seq[Cast]] and
    (JsPath \ "images" \ "posters").readNullable[Seq[ContentImage]] and
    (JsPath \ "images" \ "backdrops").readNullable[Seq[ContentImage]] and
    (JsPath \ "keywords" \ "keywords").read[Seq[Keyword]] and
    (JsPath \ "releases" \ "countries").read[Seq[Release]] and
    (JsPath \ "alternative_titles" \ "titles").read[Seq[AlternativeTitle]] and
    (JsPath \ "videos" \ "results").read[Seq[ExternalVideo]] and
    (JsPath \ "translations" \ "translations").read[Seq[Language]] and
    (JsPath \ "imdb_id").read[String] and
    (JsPath \ "belongs_to_collection").readNullable[Collection]
  )(MovieLinks.apply _)
}