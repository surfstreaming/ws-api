package models

import play.api.{Logger, GlobalSettings, Application}

object Global extends GlobalSettings {

  override def onStart(app: Application) {

    //implicit val application = app
    // initialize the runtime context
    new Object with DynamicRuntimeContext

    Logger.info("Application has started in mode: " + app.mode )

  }

  override def onStop(app: Application) {
    Logger.info("Application shutdown...")
  }

}


