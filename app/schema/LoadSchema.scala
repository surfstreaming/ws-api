package schema

import com.surf.graph.titan.TitanRawGraph
import com.tinkerpop.blueprints.Vertex
import com.thinkaurelius.titan.core.schema.{Parameter,Mapping}
import com.thinkaurelius.titan.core.{TitanGraph, Multiplicity}


object Schema {

  def load(graph : TitanGraph) = {
    val mgmt = graph.getManagementSystem

    val username = mgmt.makePropertyKey("User:username").dataType(classOf[String]).make()
    mgmt.makePropertyKey("User:onboarded").dataType(classOf[java.lang.Boolean]).make()
    mgmt.buildIndex("usernameIndex",classOf[Vertex]).addKey(username).unique().buildCompositeIndex()
    //mgmt.buildIndex("usernameSearch",classOf[Vertex]).addKey(username,Parameter.of("mapping",Mapping.TEXT)).buildMixedIndex("search")
    val displayName = mgmt.makePropertyKey("User:displayName").dataType(classOf[String]).make()
    //mgmt.buildIndex("displayNameSearch",classOf[Vertex]).addKey(displayName,Parameter.of("mapping",Mapping.TEXT)).buildMixedIndex("search")
    val email = mgmt.makePropertyKey("User:email").dataType(classOf[String]).make()
    mgmt.buildIndex("emailIndex",classOf[Vertex]).addKey(email).unique().buildCompositeIndex()
    val typeProp = mgmt.makePropertyKey("type").dataType(classOf[String]).make()
    mgmt.buildIndex("typeIndex",classOf[Vertex]).addKey(typeProp).buildCompositeIndex()
    val theClass = mgmt.makePropertyKey("class").dataType(classOf[String]).make()
    mgmt.buildIndex("classIndex",classOf[Vertex]).addKey(theClass).buildCompositeIndex()

    val movie = mgmt.makePropertyKey("Movie:tmdbId").dataType(classOf[java.lang.Integer]).make()
    mgmt.buildIndex("movieTmdbId",classOf[Vertex]).addKey(movie).unique().buildCompositeIndex()
    mgmt.makePropertyKey("Movie:adult").dataType(classOf[java.lang.Boolean]).make()
    mgmt.makePropertyKey("Movie:backdropPath").dataType(classOf[String]).make()
    mgmt.makePropertyKey("Movie:homepage").dataType(classOf[String]).make()
    mgmt.makePropertyKey("Movie:originalTitle").dataType(classOf[String]).make()
    mgmt.makePropertyKey("Movie:posterPath").dataType(classOf[String]).make()
    mgmt.makePropertyKey("Movie:releaseDate").dataType(classOf[String]).make()
    mgmt.makePropertyKey("Movie:tagline").dataType(classOf[String]).make()

    val movieTitle = mgmt.makePropertyKey("Movie:title").dataType(classOf[String]).make()
    //mgmt.buildIndex("movieTitle",classOf[Vertex]).addKey(movieTitle,Parameter.of("mapping",Mapping.TEXT)).buildMixedIndex("search")

    val movieOverview = mgmt.makePropertyKey("Movie:overview").dataType(classOf[String]).make()
    //mgmt.buildIndex("movieOverview",classOf[Vertex]).addKey(movieOverview,Parameter.of("mapping",Mapping.TEXT)).buildMixedIndex("search")
    val movieRevenue = mgmt.makePropertyKey("Movie:revenue").dataType(classOf[java.lang.Integer]).make()
    //mgmt.buildIndex("movieRevenue",classOf[Vertex]).addKey(movieRevenue).buildMixedIndex("search")
    val movieRuntime = mgmt.makePropertyKey("Movie:runtime").dataType(classOf[java.lang.Integer]).make()
    //mgmt.buildIndex("movieRuntime",classOf[Vertex]).addKey(movieRuntime).buildMixedIndex("search")
    val movieStatus = mgmt.makePropertyKey("Movie:status").dataType(classOf[String]).make()
    //mgmt.buildIndex("movieStatus",classOf[Vertex]).addKey(movieStatus).buildMixedIndex("search")
    val movieBudget = mgmt.makePropertyKey("Movie:budget").dataType(classOf[java.lang.Integer]).make()
    //mgmt.buildIndex("movieBudget",classOf[Vertex]).addKey(movieBudget).buildMixedIndex("search")

    val genre = mgmt.makePropertyKey("Genre:tmdbId").dataType(classOf[java.lang.Integer]).make()
    mgmt.buildIndex("genretmdbIdIndex",classOf[Vertex]).addKey(genre).unique().buildCompositeIndex()
    mgmt.makePropertyKey("Genre:name").dataType(classOf[String]).make()
    val company = mgmt.makePropertyKey("Company:tmdbId").dataType(classOf[java.lang.Integer]).make()
    mgmt.buildIndex("companytmdbIdIndex",classOf[Vertex]).addKey(company).unique().buildCompositeIndex()
    mgmt.makePropertyKey("Company:name").dataType(classOf[String]).make()
    val country = mgmt.makePropertyKey("Country:code").dataType(classOf[String]).make()
    mgmt.buildIndex("countryCodeIndex",classOf[Vertex]).addKey(country).unique().buildCompositeIndex()
    mgmt.makePropertyKey("Country:name").dataType(classOf[String]).make()
    val language = mgmt.makePropertyKey("Language:code").dataType(classOf[String]).make()
    mgmt.buildIndex("languageCodeIndex",classOf[Vertex]).addKey(language).unique().buildCompositeIndex()
    mgmt.makePropertyKey("Language:name").dataType(classOf[String]).make()
    mgmt.makePropertyKey("Language:englishName").dataType(classOf[String]).make()

    val cast = mgmt.makePropertyKey("Cast:creditId").dataType(classOf[String]).make()
    mgmt.buildIndex("castCreditId",classOf[Vertex]).addKey(cast).unique().buildCompositeIndex()
    mgmt.makePropertyKey("Cast:character").dataType(classOf[String]).make()
    mgmt.makePropertyKey("Cast:tmdbPersonId").dataType(classOf[java.lang.Integer]).make()
    mgmt.makePropertyKey("Cast:name").dataType(classOf[String]).make()
    mgmt.makePropertyKey("Cast:profilePath").dataType(classOf[String]).make()
    mgmt.makePropertyKey("Cast:order").dataType(classOf[java.lang.Integer]).make()

    val crew = mgmt.makePropertyKey("Crew:creditId").dataType(classOf[String]).make()
    mgmt.buildIndex("crewCreditIdIndex",classOf[Vertex]).addKey(crew).unique().buildCompositeIndex()
    mgmt.makePropertyKey("Crew:tmdbPersonId").dataType(classOf[java.lang.Integer]).make()
    mgmt.makePropertyKey("Crew:name").dataType(classOf[String]).make()
    mgmt.makePropertyKey("Crew:department").dataType(classOf[String]).make()
    mgmt.makePropertyKey("Crew:job").dataType(classOf[String]).make()
    mgmt.makePropertyKey("Crew:profilePath").dataType(classOf[String]).make()

    mgmt.makePropertyKey("ContentImage:filePath").dataType(classOf[String]).make()
    mgmt.makePropertyKey("ContentImage:height").dataType(classOf[java.lang.Double]).make()
    mgmt.makePropertyKey("ContentImage:language").dataType(classOf[String]).make()
    mgmt.makePropertyKey("ContentImage:voteCount").dataType(classOf[java.lang.Integer]).make()
    mgmt.makePropertyKey("ContentImage:width").dataType(classOf[java.lang.Double]).make()
    val keyword = mgmt.makePropertyKey("Keyword:tmdbId").dataType(classOf[java.lang.Integer]).make()
    mgmt.buildIndex("keywordTmdbIdIndex",classOf[Vertex]).addKey(keyword).unique().buildCompositeIndex()
    mgmt.makePropertyKey("Keyword:name").dataType(classOf[String]).make()

    mgmt.makePropertyKey("Release:countryCode").dataType(classOf[String]).make()
    mgmt.makePropertyKey("Release:certification").dataType(classOf[String]).make()
    mgmt.makePropertyKey("Release:releaseDate").dataType(classOf[String]).make()

    mgmt.makePropertyKey("AlternativeTitle:countryCode").dataType(classOf[String]).make()
    mgmt.makePropertyKey("AlternativeTitle:title").dataType(classOf[String]).make()

    mgmt.makePropertyKey("ExternalVideo:videoId").dataType(classOf[String]).make()
    mgmt.makePropertyKey("ExternalVideo:countryCode").dataType(classOf[String]).make()
    mgmt.makePropertyKey("ExternalVideo:key").dataType(classOf[String]).make()
    mgmt.makePropertyKey("ExternalVideo:name").dataType(classOf[String]).make()
    mgmt.makePropertyKey("ExternalVideo:site").dataType(classOf[String]).make()
    mgmt.makePropertyKey("ExternalVideo:size").dataType(classOf[java.lang.Integer]).make()
    mgmt.makePropertyKey("ExternalVideo:videoType").dataType(classOf[String]).make()
    val collection = mgmt.makePropertyKey("Collection:tmdbId").dataType(classOf[java.lang.Integer]).make()
    mgmt.buildIndex("collectionIndex",classOf[Vertex]).addKey(collection).unique().buildCompositeIndex()
    mgmt.makePropertyKey("Collection:name").dataType(classOf[String]).make()
    mgmt.makePropertyKey("Collection:backdropPath").dataType(classOf[String]).make()
    mgmt.makePropertyKey("Collection:posterPath").dataType(classOf[String]).make()
    mgmt.makePropertyKey("MetadataProviders:name").dataType(classOf[String]).make()
    val metadata = mgmt.makePropertyKey("MetadataProviders:providerId").dataType(classOf[String]).make()
    mgmt.buildIndex("metadataProviderIndex",classOf[Vertex]).addKey(metadata).unique().buildCompositeIndex()
    mgmt.makePropertyKey("MetadataProviders:created").dataType(classOf[String]).make()
    mgmt.makePropertyKey("MetadataProviders:lastUpdated").dataType(classOf[String]).make()

    mgmt.makeEdgeLabel("genres").multiplicity(Multiplicity.MULTI).make()
    mgmt.makeEdgeLabel("production_companies").multiplicity(Multiplicity.MULTI).make()
    mgmt.makeEdgeLabel("production_countries").multiplicity(Multiplicity.MULTI).make()
    mgmt.makeEdgeLabel("keywords").multiplicity(Multiplicity.MULTI).make()
    mgmt.makeEdgeLabel("spoken_languages").multiplicity(Multiplicity.MULTI).make()
    mgmt.makeEdgeLabel("translations").multiplicity(Multiplicity.MULTI).make()
    mgmt.makeEdgeLabel("guest_stars").multiplicity(Multiplicity.ONE2MANY).make()
    mgmt.makeEdgeLabel("posters").multiplicity(Multiplicity.ONE2MANY).make()
    mgmt.makeEdgeLabel("backdrops").multiplicity(Multiplicity.ONE2MANY).make()
    mgmt.makeEdgeLabel("releases").multiplicity(Multiplicity.ONE2MANY).make()
    mgmt.makeEdgeLabel("alternative_titles").multiplicity(Multiplicity.ONE2MANY).make()
    mgmt.makeEdgeLabel("videos").multiplicity(Multiplicity.ONE2MANY).make()
    mgmt.makeEdgeLabel("belongs_to_collection").multiplicity(Multiplicity.ONE2MANY).make()
    mgmt.makeEdgeLabel("cast").multiplicity(Multiplicity.ONE2MANY).make()
    mgmt.makeEdgeLabel("crew").multiplicity(Multiplicity.ONE2MANY).make()

    mgmt.makeEdgeLabel("FeaturedContent").multiplicity(Multiplicity.MULTI).make()
    mgmt.makeEdgeLabel("default_queue").multiplicity(Multiplicity.MULTI).make()

    mgmt.makeEdgeLabel("favorite_queue").multiplicity(Multiplicity.MULTI).make()
    mgmt.makeEdgeLabel("likes_queue").multiplicity(Multiplicity.MULTI).make()
    mgmt.makeEdgeLabel("in_queue").multiplicity(Multiplicity.MANY2ONE).make()
    mgmt.makeEdgeLabel("next_queue_item").multiplicity(Multiplicity.SIMPLE).make()
    mgmt.makeEdgeLabel("queue_item_content").multiplicity(Multiplicity.MANY2ONE).make()
    mgmt.makeEdgeLabel("created_queue").multiplicity(Multiplicity.ONE2MANY).make()

    mgmt.commit()
  }
}
