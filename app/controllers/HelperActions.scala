package controllers

import play.api.Logger
import play.api.mvc._
import play.api.http.HeaderNames
import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits._
import play.api.libs.json._
import scala.reflect.ClassTag
import scala.concurrent.Future.{successful => resolve}

import play.api.mvc.Results._
import models._
import play.api.libs.iteratee.Done
import models.auth.{ExpiredTokenException, Access }
import play.api.libs.json.JsSuccess
import scala.util.Failure
import models.auth.InvalidTokenException
import play.api.data.validation.ValidationError
import scala.util.Success
import play.api.libs.json.JsObject

/**
 * Helper actions to handle authentication/authorization
 */
trait HelperActions extends HeaderNames with DynamicRuntimeContext {
  import context.authObjects._

  def WithCors(action: EssentialAction): EssentialAction = EssentialAction {
    requestHeader =>
      val origin = requestHeader.headers.get(ORIGIN).getOrElse("*")
      action(requestHeader).map {
        res => res.withHeaders(ACCESS_CONTROL_ALLOW_ORIGIN -> origin)
      }
  }

  def HasToken(action: String => EssentialAction): EssentialAction = WithCors {
    EssentialAction { requestHeader =>
      val maybeToken = requestHeader.headers.get("Authorization")

      maybeToken.map { token =>
        action(token)(requestHeader)
      }.getOrElse {
        Done(Unauthorized("401 No Security Token\n"))
      }
    }
  }

  def AuthOrAnon(action : Option[AuthToken] => EssentialAction) : EssentialAction = WithCors {
    EssentialAction { request =>
      val maybeToken = request.headers.get("Authorization")
      maybeToken.map { token =>
          val result = context.authService.getUserByToken(token)
            .map(auth => action(Some(auth))(request))

          result match {
            case Success(x) => x
            case Failure(e) => Done[Array[Byte], Result](handleInvalidToken(e))
          }
      }.getOrElse {
        action(None)(request)
      }
    }
  }
  def Authenticated(access: Access)(action: AuthToken => EssentialAction) : EssentialAction = HasToken { token =>
    EssentialAction { requestHeader =>
      // TODO CHECK THE CACHE FIRST
      val result = context.authService.getUserByToken(token)
      .map { auth =>
          if(auth.access >= access) action(auth)(requestHeader)
          else  Done[Array[Byte], Result](Forbidden)
      }
      //.getOrElse(Done[Array[Byte], Result](Forbidden("server error")))

      result match {
        case Success(x) => x
        case Failure(e) => Done[Array[Byte], Result](handleInvalidToken(e))
      }
    }
  }


  def handleInvalidToken(e : Throwable) = e match {
    case e: InvalidTokenException => Forbidden("invalid token")

    case e: ExpiredTokenException => Unauthorized("expired token")
    // TODO remove this at some point:
    case e: Exception => Logger.error("exception from handleInvalidToken",e); Unauthorized("authorization error")
    case _ => Unauthorized("authorization error")
  }

  @deprecated("use jsonSuccess + apiException","2014-10-01")
  def handleJsonResponse(json : JsValue) = {
    // If there is an "error" object in the json, then we need to determine which HTTP code to write
    (json \ "error").asOpt[String]
      .map { error =>
        (json \ "code").asOpt[Int].map {
            case 400 => BadRequest(json)
            case 401 => Unauthorized(json)
            case 404 => NotFound(json)
            case _ => BadRequest(json)
        }.getOrElse(InternalServerError("Unable to process response")) // if there was no code, this probably is not a subclass of ApiException
    }.getOrElse(Ok(json)) // get the error response, but if there was no error than just return 200 OK with the original JSON
  }

  def jsonSuccess(json : JsValue) : Result = Ok(json)
  def apiException = PartialFunction[Throwable,Result] {
    case x : ApiException => recoverApiException(x);
    case e =>  Logger.error("Encountered Error",e); InternalServerError("Error Processing Response")
  }
  def recoverApiException(e : ApiException) : Result = e.code match {
    case 400 => BadRequest(Json.toJson(e))
    case 401 => Unauthorized(Json.toJson(e))
    case 404 => NotFound(Json.toJson(e))
    case _ => BadRequest(Json.toJson(e))
  }

  @deprecated("don't put this in the controller, create a module and use JsonHelper.WithJson","2014-05-14")
  def WithJson[T](json: JsValue)(action: T => Future[Result])
                 (implicit reads: Reads[T], classTag: ClassTag[T]): Future[Result] =
    json.validate[T] match {
      case JsSuccess(value, _) => action(value)
      case JsError(err) => resolve(BadRequest(Json.obj("error" -> "error", "errors" -> errToJson(err))))
  }
  @deprecated("this can be removed after HelperActions.WithJson is removed","2014-05-14")
  /** Helper that maps JsError errors to a JSON object */
  private def errToJson(errors: Seq[(JsPath, Seq[ValidationError])]): JsValue = {
    val jsonErrors: Seq[(String, JsValue)] = errors map {
      case (path, errs) => path.toJsonString -> Json.toJson(errs.map(_.message))
    }
    JsObject(jsonErrors)
  }


  //def JsonFuture(block : Request[AnyContent] => Future[Any]) : Future[Result] = block.apply()

}
