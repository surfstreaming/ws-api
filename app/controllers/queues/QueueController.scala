package controllers.queues

import play.api.libs.json.Json
import play.api.mvc.{Action, Controller}
import controllers.HelperActions
import models.auth.NormalUser
import models.DynamicRuntimeContext
import play.api.libs.concurrent.Execution.Implicits.defaultContext

import scala.concurrent.Future

/**
 * Controller for Queue CRUD
 */
object QueueController extends Controller with HelperActions with DynamicRuntimeContext {
  def list(userId : Long, components : String) = WithCors {
    AuthOrAnon { auth =>
      Action.async { request =>
        context.queueJson.list(auth.map(_.id),userId,components.split(",")).map(jsonSuccess).recover(apiException)
      }
    }
  }
  def get(userId : Long, queueId : Long, components : String) = WithCors {
    AuthOrAnon { auth =>
      Action.async { request =>
        context.queueJson.get(auth.map(_.id),userId,queueId,components.split(",")).map(jsonSuccess).recover(apiException)
      }
    }
  }
  def post(userId : Long) = WithCors {
    Authenticated(NormalUser) { auth =>
      Action.async(parse.json){ request =>
         context.queueJson.create(auth.id,userId,request.body).map(jsonSuccess).recover(apiException)
      }
    }
  }
  def put(userId : Long, queueId : Long) = WithCors {
    Authenticated(NormalUser) { auth =>
      Action.async(parse.json){ request =>
        context.queueJson.update(auth.id,userId,queueId,request.body).map(jsonSuccess).recover(apiException)
      }
    }
  }
  def delete(userId : Long, queueId : Long) = WithCors {
    Authenticated(NormalUser) { auth =>
      Action.async(parse.json){ request =>
        context.queueJson.delete(auth.id,userId,queueId).map(jsonSuccess).recover(apiException)
      }
    }
  }

  def copy(userId : Long, queueId : Long) = WithCors {
    Authenticated(NormalUser) { auth =>
      Action.async(parse.json){ request =>
        Future.successful(Ok(Json.obj("status"->"not implemented")))
      }
    }
  }

}
