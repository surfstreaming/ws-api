package controllers.queues

import play.api.mvc.{Action, Controller}
import controllers.HelperActions
import models.DynamicRuntimeContext
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import models.auth.NormalUser

/**
 * Controller for Queue Item CRUD
 */
object QueueItemController extends Controller with HelperActions with DynamicRuntimeContext {
  def list(userId : Long, queueId : Long) = WithCors {
    AuthOrAnon { auth =>
      Action.async { request =>
        context.queueJson.itemList(auth.map(_.id),userId,queueId,None).map(jsonSuccess).recover(apiException)
      }
    }
  }
  def get(userId : Long, queueId : Long, itemId : Long) = WithCors {
    AuthOrAnon { auth =>
      Action.async { request =>
        context.queueJson.itemGet(auth.map(_.id),userId,queueId,itemId).map(jsonSuccess).recover(apiException)
      }
    }
  }
  def post(userId : Long, queueId : Long) = WithCors {
    Authenticated(NormalUser) { auth =>
      Action.async(parse.json){ request =>
        context.queueJson.itemCreate(auth.id,userId,queueId,request.body).map(jsonSuccess).recover(apiException)
      }
    }
  }
  def put(userId : Long, queueId : Long, itemId : Long) = WithCors {
    Authenticated(NormalUser) { auth =>
      Action.async(parse.json){ request =>
        context.queueJson.itemUpdate(auth.id,userId,queueId,itemId,request.body).map(jsonSuccess).recover(apiException)
      }
    }
  }
  def delete(userId : Long, queueId : Long, itemId : Long) = WithCors {
    Authenticated(NormalUser) { auth =>
      Action.async(parse.json){ request =>
        context.queueJson.itemDelete(auth.id,userId,queueId,itemId).map(jsonSuccess).recover(apiException)
      }
    }
  }
}
