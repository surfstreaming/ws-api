package controllers.queues

import controllers.HelperActions
import models.DynamicRuntimeContext
import models.auth.NormalUser
import play.api.Logger
import play.api.mvc.{Action, Controller}
import play.api.libs.concurrent.Execution.Implicits.defaultContext

/**
 * Controller for mananging a user's favorite queues
 */
object QueueLikesController extends Controller with HelperActions with DynamicRuntimeContext {
  import context.objects._
  def list(userId : Long,components : String) = WithCors {
    AuthOrAnon { auth =>
      Action.async { request =>
        context.queueJson.likesList(auth.map(_.id),userId,components.split(",")).map(jsonSuccess).recover(apiException)
      }
    }
  }
  def get(userId : Long, likesId : String) = WithCors {
    AuthOrAnon { auth =>
      Action.async { request =>
        context.queueJson.likesGet(auth.map(_.id),userId,likesId).map(jsonSuccess).recover(apiException)
      }
    }
  }
  def post(userId : Long) = WithCors {
    Authenticated(NormalUser) { auth =>
      Action.async(parse.json){ request =>
        context.queueJson.likesCreate(auth.id,userId,request.body).map(jsonSuccess).recover(apiException)
      }
    }
  }

  def delete(userId : Long, likesId : String) = WithCors {
    Authenticated(NormalUser) { auth =>
      Action.async(parse.json){ request =>
        context.queueJson.likesDelete(auth.id,userId,likesId).map(jsonSuccess).recover(apiException)
      }
    }
  }
}
