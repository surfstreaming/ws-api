package controllers.users

import com.tinkerpop.blueprints.{Graph, Vertex}
import com.tinkerpop.gremlin.groovy.Gremlin
import com.tinkerpop.gremlin.java.GremlinPipeline
import com.tinkerpop.pipes.util.Pipeline
import com.tinkerpop.pipes.util.iterators.SingleIterator
import controllers.HelperActions
import models.auth.NormalUser
import models.user.User
import play.api.mvc.{Action, Controller}
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import scala.concurrent.Future
import play.api.libs.json.JsString


object UserController extends Controller with HelperActions {


  def username(username : String, components : String) = AuthOrAnon { auth =>
    Action.async {
      context.userJson.getUserByUsername(auth.map(_.id),username,components.split(",")).map(jsonSuccess).recover(apiException)
    }
  }

  def get(user : Long, components : String) = AuthOrAnon { auth =>
    Action.async {
      context.userJson.getUserById(auth.map(_.id),user,components.split(",")).map(jsonSuccess).recover(apiException)
    }
  }

  def put(user : Long) = Authenticated(NormalUser) { auth =>
    Action.async(parse.json) { request =>
      context.userJson.updateUser(auth.id,user,request.body).map(jsonSuccess).recover(apiException)
    }
  }

  def delete(userId : Long) = Authenticated(NormalUser) { auth =>
    Action.async(parse.json) { request =>
      throw new UnsupportedOperationException
    }
  }

  def getQueues(userId : Long, components : String) = AuthOrAnon { auth =>
    Action.async {
      context.userJson.getQueues(auth.map(_.id),userId,components.split(",")).map(jsonSuccess).recover(apiException)
    }
  }

  def getQueue(userId : Long, queueId : Long, components : String) = AuthOrAnon { auth =>
    Action.async {
      // TODO implement
      Future(JsString("not implemented")).map(jsonSuccess).recover(apiException)
    }
  }
}
