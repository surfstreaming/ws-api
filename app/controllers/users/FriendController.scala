package controllers.users

import controllers.HelperActions
import models.auth.NormalUser
import play.api.mvc.{Action, Controller}
import play.api.libs.concurrent.Execution.Implicits.defaultContext


object FriendController extends Controller with HelperActions {
  import context.objects._
  def list(user : Long, components : String) =    AuthOrAnon { auth =>
    Action.async {
      context.userJson.getFriends(auth.map(_.id),user,components.split(",")).map(jsonSuccess).recover(apiException)
    }
  }
  def add(user: Long) = Authenticated(NormalUser) { auth =>
    Action.async(parse.json) { request =>
      context.userJson.addFriend(auth.id,user,request.body).map(jsonSuccess).recover(apiException)
    }
  }

  def confirm(user: Long, friendshipID : String) = Authenticated(NormalUser) { auth =>
    Action.async {
      context.userJson.confirmFriend(auth.id,friendshipID).map(jsonSuccess).recover(apiException)
    }
  }
  def delete(user: Long, friend : String) = Authenticated(NormalUser) { auth =>
    Action.async {
      context.userJson.removeFriend(auth.id,user,friend).map(jsonSuccess).recover(apiException)
    }
  }
}
