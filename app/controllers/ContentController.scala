package controllers

import models.DynamicRuntimeContext

object ContentController extends ServiceController with DynamicRuntimeContext {
  val host = context.constants.contentServiceURL
}
