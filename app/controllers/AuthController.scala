package controllers

import play.api.mvc._
import play.api.libs.json._
import models._
import scala.util.Success
import scala.util.Failure
import models.auth.NormalUser
import models.user.User

// JSON library
import play.api.libs.json.Reads._ // Custom validation helpers
import play.api.libs.functional.syntax._ // Combinator syntax

import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits._

/**
 * Controller to handle authentication and token checking
 */

object AuthController extends Controller with HelperActions with DynamicRuntimeContext {

  def login = WithCors {
    Action.async(parse.json) { parsedRequest =>
      // TODO store refresh token in DB so it can be revoked
      context.authJson.authenticate(parsedRequest.body)
        .map(jsonSuccess).recover(apiException)
    }
  }

  def logout = Authenticated(NormalUser) { auth =>
    Action.async(parse.json) { request =>
      context.authJson.logout(auth.id,request.body)
        .map(jsonSuccess).recover(apiException)
    }
  }
  def register = AuthOrAnon { auth =>
    Action.async(parse.json) { request =>
      context.authJson.register(auth,request.body)
        .map(jsonSuccess).recover(apiException)
    }
  }

  def signup = AuthOrAnon { auth =>
    Action.async(parse.json) { request =>
      context.authJson.signup(auth,request.body)
        .map(jsonSuccess).recover(apiException)
    }
  }

  def validate = WithCors {
    Action.async(parse.json) { request =>
      context.authJson.validate(request.body)
        .map(jsonSuccess).recover(apiException)
    }
  }

  def current(components : String) = Authenticated(NormalUser) { auth =>
    Action.async { request =>
      context.userJson.getUserById(Some(auth.id),auth.id,components.split(","))
        .map(jsonSuccess).recover(apiException)
    }
  }

  def reset = WithCors {
    Action.async(parse.json) { request =>
      context.authJson.resetPassword(request.body)
        .map(jsonSuccess).recover(apiException)
    }
  }

  def changePassword = Authenticated(NormalUser) { auth =>
    Action.async(parse.json) { request =>
      context.authJson.changePassword(auth.id,request.body)
        .map(jsonSuccess).recover(apiException)
    }
  }

  def usernameAvailable(username : String) = AuthOrAnon { auth =>
    Action.async { request =>
      context.authJson.usernameAvailable(username)
        .map(jsonSuccess).recover(apiException)
    }
  }

  def emailAvailable(email : String) = AuthOrAnon { auth =>
    Action.async { request =>
      context.authJson.emailAvailable(email)
        .map(jsonSuccess).recover(apiException)
    }
  }

}
