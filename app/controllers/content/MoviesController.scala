package controllers.content

import controllers.HelperActions
import play.api.mvc.{Action, Controller}
import models.DynamicRuntimeContext
import play.api.libs.concurrent.Execution.Implicits.defaultContext

/**
 * Controller to handle functions related to Movies
 */
object MoviesController extends Controller with HelperActions with DynamicRuntimeContext {

  def getMovie(id : Long, components : String) = WithCors {
    AuthOrAnon { auth =>
      Action.async {
        context.moviesJson.get(auth.map(_.id),id,components.split(",")).map(jsonSuccess).recover(apiException)
      }
    }
  }
}
