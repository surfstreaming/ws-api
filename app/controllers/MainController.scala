package controllers

import play.api.mvc.{Action, Controller}

/**
 * Created by dan on 5/23/14.
 */
object MainController extends Controller {

  def index = Action {
    Ok("Queue API")
  }

  def health = Action {
    Ok("ok")
  }

  def options(url : String ) = Action {
    request =>
      val origin = request.headers.get(ORIGIN).getOrElse("*")

      Ok("").withHeaders(
        ACCESS_CONTROL_ALLOW_ORIGIN -> origin,
        ACCESS_CONTROL_ALLOW_METHODS -> "GET,POST,PUT,DELETE,OPTIONS",
        ACCESS_CONTROL_MAX_AGE -> "3600",
        ACCESS_CONTROL_ALLOW_HEADERS ->  s"$ORIGIN, X-Requested-With, $CONTENT_TYPE, $ACCEPT, $AUTHORIZATION, Authorization",
        ACCESS_CONTROL_ALLOW_CREDENTIALS -> "true")
  }
}
