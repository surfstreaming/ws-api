package controllers

import play.api.libs.ws.WS
import play.api.mvc.{Action, Controller}

import play.api.libs.concurrent.Execution.Implicits._
import models.DynamicRuntimeContext

import play.api.Play.current


/**
  * Created by dan on 4/10/14.
  */
object SearchController extends Controller with HelperActions with DynamicRuntimeContext {
  val searchHost = context.constants.searchServiceURL
  def get(path : String) = WithCors {
    Action.async { implicit request =>
      val q = request.queryString.toList.flatMap{ x =>
        x._2.map(z => (x._1,z))
      }

      WS.url(searchHost+"/"+path).withQueryString(q : _*).get().map(x => Ok(x.json))
    }
  }
  def post(path : String) = WithCors {
    Action.async(parse.json) { implicit request =>
      WS.url(searchHost+"/"+path).post(request.body).map(x => Ok(x.json))
    }
  }
}
