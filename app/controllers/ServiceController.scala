package controllers

import play.api.libs.ws.WS
import play.api.mvc.{Result, ResponseHeader, Action, Controller}
import play.api.Play.current
import play.api.libs.concurrent.Execution.Implicits._

/**
 * A generic controller that redirects requests to an external microservice
 */
trait ServiceController extends Controller with HelperActions {
  val host : String
  def get(path : String) = AuthOrAnon { authOpt =>
    Action.async { implicit request =>

      val q = request.queryString.toList.flatMap{ x =>
        x._2.map(z => (x._1,z))
      }

      val query = authOpt.map { auth =>
          q ++ Seq(("requestor",auth.id.toString))
      }.getOrElse(q)

      WS.url(host+"/"+path).withQueryString(query : _*).getStream().map {
        case (response, body) =>
          Result(ResponseHeader(response.status, response.headers.mapValues(_.head)), body)
      }
    }
  }
  def post(path : String) = AuthOrAnon { auth =>
    Action.async(parse.json) { implicit request =>
      WS.url(host+"/"+path).post(request.body).map(x => Ok(x.json))
    }
  }
}
