package controllers

import models.auth.PlatformAdmin
import play.api.mvc.{Action, Controller}
import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits._
import models.DynamicRuntimeContext
import play.api.libs.json.Json

/**
 * Controller to display queues and content on the homepage
 */
object FeaturedController extends Controller with HelperActions with DynamicRuntimeContext {
  import context.objects._
  def list = AuthOrAnon { auth =>
    Action.async {
      context.contentJson.listFeatured(auth.map(_.id)).map(jsonSuccess).recover(apiException)
    }
  }

  def add = Authenticated(PlatformAdmin) { auth =>
    Action.async(parse.json) { request =>
      context.contentJson.addFeatured(request.body).map(jsonSuccess).recover(apiException)
    }
  }
  def update(id : String) = Authenticated(PlatformAdmin) { auth =>
    Action.async(parse.json) { request =>
      context.contentJson.updateFeatured(id,request.body).map(jsonSuccess).recover(apiException)
    }
  }
  def delete(id : String) = Authenticated(PlatformAdmin) { auth =>
    Action.async {
      context.contentJson.deleteFeatured(id).map(jsonSuccess).recover(apiException)
    }
  }

  def listQueues(components : String) = AuthOrAnon { auth =>
    Action.async { request =>
      context.queueJson.favoriteList(None,0,components.split(",")).map(jsonSuccess).recover(apiException)
    }
  }

}
