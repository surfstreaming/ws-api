# BUILD-USING:        activator dist && docker build -t ws-api .
# RUN-USING:          docker run --rm -t -i --name ws-api ws-api
# DEPLOY-USING        docker tag ws-api surf/ws-api && docker push surf/ws-api
FROM         surf/base-jvm

# add the application and bundle
ADD target/universal/ws-api-1.0-SNAPSHOT.zip /
RUN unzip /ws-api-1.0-SNAPSHOT
RUN ln -s ws-api-1.0-SNAPSHOT /app

EXPOSE 9000

CMD           ["/app/bin/ws-api"]

