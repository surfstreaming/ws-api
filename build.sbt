name := """ws-api"""

version := "1.0-SNAPSHOT"

logLevel := Level.Info

scalaVersion := "2.11.1"

lazy val root = (project in file(".")).enablePlugins(PlayScala)


libraryDependencies ++= Seq(
  ws,
  "com.surf.graph" %% "surf-graph-titan" % "0.1-SNAPSHOT",
  "com.surf.graph" %% "surf-graph-json" % "0.1-SNAPSHOT",
  "com.surf.ws" %% "ws-core" % "1.0-SNAPSHOT",
  "org.mindrot" % "jbcrypt" % "0.3m",
  "com.nimbusds" % "nimbus-jose-jwt" % "2.22.1"
  // Add your own project dependencies in the form:
  // "group" % "artifact" % "version"
)